clearvars();
global step

boxcar(:,1) = [zeros(2,1); repmat(0.3,3,1); zeros(11,1)];
boxcar(:,2) = [zeros(2,1); repmat(0.6,3,1); zeros(11,1)];
boxcar(:,3) = [zeros(2,1); repmat(1,3,1); zeros(11,1)];

for i = 1:size(boxcar,2)
    startingvector = [0 1 1 1]; %[dz df dv dq]
    tspan = [0:0.001:15];
    p = 0.34;
    v0 = 0.02;
    k1 = 4.3*28.265*3*0.0331*p;
    k2 = 0.47*110*0.0331*p;
    k3 = 1-0.47;
    step = boxcar(:,i);

    [t,y] = ode45(@tim,tspan,startingvector);
    hrf(:,i) = 100/p*v0*(k1*(1-y(:,4))+k2*(1-y(:,4)./y(:,3)+k3*(1-y(:,3))));
end
hemo1 = hrf(:,1);
hemo2 = hrf(:,2);
hemo3 = hrf(:,3);

[peak1_amp, peak1_time] = max(hemo1);
[peak2_amp, peak2_time] = max(hemo2);
[peak3_amp, peak3_time] = max(hemo3);

plot(tspan, hrf/10, 'LineWidth', 2)
ylabel('Response magnitude (a.u.)', 'FontSize', 12);
xlabel('Time (s)', 'FontSize', 12);
legend(['Duration 1; a: ' num2str(peak1_amp/10) ', t: ' num2str(peak1_time)],...
    ['Duration 2; a: ' num2str(peak2_amp/10) ', t: ' num2str(peak2_time)],...
    ['Duration 3; a: ' num2str(peak3_amp/10) ', t: ' num2str(peak3_time)], 'FontSize', 12);
title('Expected effects of stimulus duration on balloon-modelled BOLD response', 'FontSize', 14);