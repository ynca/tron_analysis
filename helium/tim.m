function dF = tim(t,x)
global step
y = step;
p = 0.34;
v0 = 0.02;
k1 = 4.3*28.265*3*0.0331*p;
k2 = 0.47*110*0.0331*p;
k3 = 1-0.47;
beta = 0.65; %per s   % signal decay
gamma = 0.41; %per s   % autoregulation
tau = 0.98; %s % transit time
alpha = 0.32; % vessel stiffness exponent

z = x(1);
f = x(2);
v = x(3);
q = x(4);

%dz = y(floor(t)+1) - beta*z - gamma*(f-1);
dz = y(floor(t)+1) - beta*z - gamma*(f-1);
%dz = y - beta*z - gamma*(f-1);
df = z;
dv = 1/tau*(f-v^(1/alpha));
dq = 1/tau*(f/p*(1-(1-p)^(1/f))-q/v*v^(1/alpha));

dF = [dz df dv dq]';

