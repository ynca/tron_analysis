% load('testdata.mat');
% ydata = ProtoMatrix1;

clearvars();
global yparams
global hrfe

%options = optimset('MaxIter',10000);
volsperevent = 41; % Length of each timecourse
customvolsperevent = 34; % Length after forcing baseline at 3s

cd('/Volumes/Passport/TRoN/Analysis/PSubj/Data'); % Path to data matrices output by PerSubjProc
eventtype = "V"; % Set ROI type (A or V)
filedir = dir('V1*.mat'); % Set filename search criteria

for fileidx = 1:numel(filedir)
    clearvars('-except', 'eventtype', 'filedir', 'customvolsperevent', 'volsperevent', 'fileidx', 'AllStats', 'yparams', 'hrfe');
    filename = sprintf('%s', [filedir(fileidx).folder, '/', filedir(fileidx).name]);
    load(filename);
    subjname = [filedir(fileidx).name(4:10)]; % Data import and subject name definition

    conddef = {["Length 0.1 Int 0.1" "Length 0.1 Int 0.3" "Length 0.1 Int 0.9"; "Length 0.3 Int 0.1" "Length 0.3 Int 0.3" "Length 0.3 Int 0.9"; "Length 0.9 Int 0.1" "Length 0.9 Int 0.3" "Length 0.9 Int 0.9"];...
        ["Length 0.1 Int 0.01" "Length 0.1 Int 0.1" "Length 0.1 Int 1"; "Length 0.3 Int 0.01" "Length 0.3 Int 0.1" "Length 0.3 Int 1"; "Length 0.9 Int 0.01" "Length 0.9 Int 0.1" "Length 0.9 Int 1"]};
    
    if eventtype == 'V' % V1 timecourse processing to build protomatrix
        conddefinition = conddef{2};
        eventlengths = [0.1 0.3 0.9];
        eventintensities = [0.01 0.1 1];
        roiname = 'V1';
        
        for datacnt = 1:numel(CurrentV)
        VOnset(:, datacnt) = CurrentV(datacnt).VisualEvs(1:end);
        VLength(:,datacnt) = CurrentV(datacnt).VisualLengths;
        VInt(:,datacnt) = CurrentV(datacnt).VisualInts;
        end
        
        for datacnt = 1:numel(CurrentV)
        for evcnt = 1:size(VOnset(:,datacnt),1)
            VTC(:,evcnt,datacnt) = CurrentV(datacnt).RawData((VOnset(evcnt,datacnt)-10):(VOnset(evcnt,datacnt)+30));
        end
        end

        for datacnt = 1:size(VLength,2)
        for evcnt = 1:size(VLength(:,datacnt),1)
            for Lcon = 1:numel(eventlengths)
                Lsearch{Lcon}(evcnt, datacnt) = eq(VLength(evcnt, datacnt),eventlengths(Lcon));
            end
        end
        for evcnt = 1:size(VInt(:,datacnt),1)
            for Icon = 1:numel(eventintensities)
                Isearch{Icon}(evcnt, datacnt) = eq(VInt(evcnt, datacnt),eventintensities(Icon));
            end
        end
        end

        ProtoMatrix{3, 3} = [];

        for Lcon = 1:numel(eventlengths)
        for Icon = 1:numel(eventintensities)
            ConditionMap{Lcon, Icon} = [eventlengths(Lcon) eventintensities(Icon)];
            for datacnt = 1:size(Lsearch{Lcon},2)
                matcnt = 1;
                for evcnt = 1:size(Lsearch{Lcon}(:,datacnt),1)
                    if Lsearch{Lcon}(evcnt, datacnt) == 1 && Isearch{Icon}(evcnt, datacnt) == 1
                        ProtoMatrix{Lcon, Icon}(1:volsperevent, matcnt, datacnt) = VTC(1:end, evcnt,datacnt);
                        matcnt = matcnt+1;
                    end
                end
            end
        end
        end

    elseif eventtype == 'A' % A1 timecourse processing to build protomatrix
        conddefinition = conddef{1};
        eventlengths = [0.1 0.3 0.9];
        eventintensities = [0.1 0.3 1];
        roiname = 'A1';
        
        for datacnt = 1:numel(CurrentA)
        AOnset(:, datacnt) = CurrentA(datacnt).AuditoryEvs(1:end);
        ALength(:,datacnt) = CurrentA(datacnt).AuditoryLengths;
        AInt(:,datacnt) = CurrentA(datacnt).AuditoryInts;
        end
        
        for datacnt = 1:numel(CurrentA)
        for evcnt = 1:size(AOnset(:,datacnt),1)
            ATC(:,evcnt,datacnt) = CurrentA(datacnt).RawData((AOnset(evcnt,datacnt)-10):(AOnset(evcnt,datacnt)+30));
        end
        end

        for datacnt = 1:size(ALength,2)
        for evcnt = 1:size(ALength(:,datacnt),1)
            for Lcon = 1:numel(eventlengths)
                Lsearch{Lcon}(evcnt, datacnt) = eq(ALength(evcnt, datacnt),eventlengths(Lcon));
            end
        end
        for evcnt = 1:size(AInt(:,datacnt),1)
            for Icon = 1:numel(eventintensities)
                Isearch{Icon}(evcnt, datacnt) = eq(AInt(evcnt, datacnt),eventintensities(Icon));
            end
        end
        end

        ProtoMatrix{3, 3} = [];

        for Lcon = 1:numel(eventlengths)
        for Icon = 1:numel(eventintensities)
            ConditionMap{Lcon, Icon} = [eventlengths(Lcon) eventintensities(Icon)];
            for datacnt = 1:size(Lsearch{Lcon},2)
                matcnt = 1;
                for evcnt = 1:size(Lsearch{Lcon}(:,datacnt),1)
                    if Lsearch{Lcon}(evcnt, datacnt) == 1 && Isearch{Icon}(evcnt, datacnt) == 1
                        ProtoMatrix{Lcon, Icon}(1:volsperevent, matcnt, datacnt) = ATC(1:end, evcnt,datacnt);
                        matcnt = matcnt+1;
                    end
                end
            end
        end
        end

    end
    
    for conC = 1:numel(ProtoMatrix) % convert 3D cells in protomatrix to 2D cells
        ProtoMatrix{conC} = reshape(ProtoMatrix{conC}, [41 size(ProtoMatrix{conC},2)*size(ProtoMatrix{conC},3)]);
    end
    
    for conC = 1:numel(ProtoMatrix) % baseline computation
        BaselineVector{conC} = ProtoMatrix{conC}(14, :);
        for i = 1:size(ProtoMatrix{conC}, 2)
            ProtoMatrix{conC}(:, i) = ((ProtoMatrix{conC}(:, i) - BaselineVector{conC}(1,i)) / BaselineVector{conC}(1,i)) * 100;
        end
    end
    
    for lengths = 1:size(ProtoMatrix,1)
        for ints = 1:size(ProtoMatrix,2)
        
            EventAvg{lengths, ints} = nanmean(ProtoMatrix{lengths, ints}(10:customvolsperevent,:), 2);
    
            options = optimset('MaxFunEvals',100000, 'MaxIter',10000);
            p0 = [0.5  0.4  1    0.3];
            lb = [0.01 0.01 0.01 0.01];
            ub = [3    1    3    2];
            for j = 1:size(ProtoMatrix{lengths, ints}, 2)
            ydata = ProtoMatrix{lengths, ints}(10:customvolsperevent,j);
            xdata = [ones(2,1); zeros(24,1)];
            greeks{lengths, ints}(:,j) = lsqcurvefit(@fitodefun,p0,xdata,ydata,lb,ub,options);
            results{lengths, ints}(:,1:4,j) = yparams;
            hrf{lengths, ints}(:,j) = hrfe;
            end
            %%
            samplingRateIncrease = 100;
            newSamplePoints = linspace(0.625, 15, 15 * samplingRateIncrease);
            smoothhrf{lengths,ints} = spline(0:0.625:15, nanmean(hrf{lengths,ints},2), newSamplePoints);
            
            cd('/Volumes/Passport/TRoN/Analysis/PSubj/');
            
            save(sprintf('%s', [eventtype, '_', subjname, '_BalloonResult_', 'l', num2str(lengths), '_i', num2str(ints), '.mat']),'greeks', 'results', 'hrf', 'smoothhrf');
        end
    end
end