function hrf = fitodefun(p0, xdata)
global param0
global boxcar
global yparams
global hrfe
boxcar = xdata;
param0 = p0;
x0 = [0 1 1 1]; %[dz df dv dq]
tspan = [0:0.625:15];
p = 0.34;
v0 = 0.02;
k1 = 4.3*28.265*3*0.0331*p;
k2 = 0.47*110*0.0331*p;
k3 = 1-0.47;
[t,y] = ode45(@three,tspan,x0);
hrf = 100/p*v0*(k1*(1-y(:,4))+k2*(1-y(:,4)./y(:,3)+k3*(1-y(:,3))));
hrfe = hrf;
yparams = y;