function [output, bootsam] = BootStrap2(x, TRinSeconds, EvLengthinSeconds)

global TR 
global events

%samplesize = 1:size(x{1},1);
events = x;
TR = TRinSeconds;
options = statset('UseParallel', true);

[output, bootsam] = bootstrp(1000, @GammaFit, x, TR, EvLengthinSeconds, 'Options', options);

end