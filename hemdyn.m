clearvars();
global signal
global covs

%% File handling
cd('/Volumes/Passport/TRoN/Analysis/PSubj2/DataB'); % Path to data matrices output by PerSubjProc
eventtype = "A"; % Set ROI type (A or V)
filedir = dir('A1*.mat'); % Set filename search criteria


%% Input sorting
for fileidx = 1:numel(filedir)
    clearvars('-except', 'eventtype', 'filedir', 'customvolsperevent', 'volsperevent', 'fileidx', 'modelvoltkernel1', 'modelvoltkernel2', 'fullEMPTC', 'Hsave', 'EventAvg', 'signal', 'covs', 'FPcovariance', 'AllStats');
    filename = sprintf('%s', [filedir(fileidx).folder, '/', filedir(fileidx).name]);
    load(filename);
    subjname = [filedir(fileidx).name(4:10)]; % Data import and subject name definition

    conddef = {["Length 0.1 Int 0.1" "Length 0.1 Int 0.3" "Length 0.1 Int 0.9";... 
                "Length 0.3 Int 0.1" "Length 0.3 Int 0.3" "Length 0.3 Int 0.9";... 
                "Length 0.9 Int 0.1" "Length 0.9 Int 0.3" "Length 0.9 Int 0.9"];...
                ["Length 0.1 Int 0.01" "Length 0.1 Int 0.1" "Length 0.1 Int 1";... 
                "Length 0.3 Int 0.01" "Length 0.3 Int 0.1" "Length 0.3 Int 1";...
                "Length 0.9 Int 0.01" "Length 0.9 Int 0.1" "Length 0.9 Int 1"]};
    
    if eventtype == 'V' % V1 timecourse processing to build protomatrix
        conddefinition = conddef{2};
        eventlengths = [0.1 0.3 0.9];
        eventintensities = [0.01 0.1 1];
        roiname = 'V1';
        
        for datacnt = 1:numel(CurrentV)
        VOnset(:, datacnt) = CurrentV(datacnt).VisualEvs(1:end);
        VLength(:,datacnt) = CurrentV(datacnt).VisualLengths;
        VInt(:,datacnt) = CurrentV(datacnt).VisualInts;
        end
        
        for datacnt = 1:numel(CurrentV)
        for evcnt = 1:size(VOnset(:,datacnt),1)
            VTC(:,evcnt,datacnt) = CurrentV(datacnt).RawData((VOnset(evcnt,datacnt)-10):(VOnset(evcnt,datacnt)+30));
        end
        end

        for datacnt = 1:size(VLength,2)
        for evcnt = 1:size(VLength(:,datacnt),1)
            for Lcon = 1:numel(eventlengths)
                Lsearch{Lcon}(evcnt, datacnt) = eq(VLength(evcnt, datacnt),eventlengths(Lcon));
            end
        end
        for evcnt = 1:size(VInt(:,datacnt),1)
            for Icon = 1:numel(eventintensities)
                Isearch{Icon}(evcnt, datacnt) = eq(VInt(evcnt, datacnt),eventintensities(Icon));
            end
        end
        end

        ProtoMatrix{3, 3} = [];

        for Lcon = 1:numel(eventlengths)
        for Icon = 1:numel(eventintensities)
            ConditionMap{Lcon, Icon} = [eventlengths(Lcon) eventintensities(Icon)];
            for datacnt = 1:size(Lsearch{Lcon},2)
                matcnt = 1;
                for evcnt = 1:size(Lsearch{Lcon}(:,datacnt),1)
                    if Lsearch{Lcon}(evcnt, datacnt) == 1 && Isearch{Icon}(evcnt, datacnt) == 1
                        ProtoMatrix{Lcon, Icon}(1:41, matcnt, datacnt) = VTC(1:end, evcnt,datacnt);
                        matcnt = matcnt+1;
                    end
                end
            end
        end
        end

    elseif eventtype == 'A' % A1 timecourse processing to build protomatrix
        conddefinition = conddef{1};
        eventlengths = [0.1 0.3 0.9];
        eventintensities = [0.1 0.3 1];
        roiname = 'A1';
        
        for datacnt = 1:numel(CurrentA)
        AOnset(:, datacnt) = CurrentA(datacnt).AuditoryEvs(1:end);
        ALength(:,datacnt) = CurrentA(datacnt).AuditoryLengths;
        AInt(:,datacnt) = CurrentA(datacnt).AuditoryInts;
        end
        
        for datacnt = 1:numel(CurrentA)
        for evcnt = 1:size(AOnset(:,datacnt),1)
            ATC(:,evcnt,datacnt) = CurrentA(datacnt).RawData((AOnset(evcnt,datacnt)-10):(AOnset(evcnt,datacnt)+30));
        end
        end

        for datacnt = 1:size(ALength,2)
        for evcnt = 1:size(ALength(:,datacnt),1)
            for Lcon = 1:numel(eventlengths)
                Lsearch{Lcon}(evcnt, datacnt) = eq(ALength(evcnt, datacnt),eventlengths(Lcon));
            end
        end
        for evcnt = 1:size(AInt(:,datacnt),1)
            for Icon = 1:numel(eventintensities)
                Isearch{Icon}(evcnt, datacnt) = eq(AInt(evcnt, datacnt),eventintensities(Icon));
            end
        end
        end

        ProtoMatrix{3, 3} = [];

        for Lcon = 1:numel(eventlengths)
        for Icon = 1:numel(eventintensities)
            ConditionMap{Lcon, Icon} = [eventlengths(Lcon) eventintensities(Icon)];
            for datacnt = 1:size(Lsearch{Lcon},2)
                matcnt = 1;
                for evcnt = 1:size(Lsearch{Lcon}(:,datacnt),1)
                    if Lsearch{Lcon}(evcnt, datacnt) == 1 && Isearch{Icon}(evcnt, datacnt) == 1
                        ProtoMatrix{Lcon, Icon}(1:41, matcnt, datacnt) = ATC(1:end, evcnt,datacnt);
                        matcnt = matcnt+1;
                    end
                end
            end
        end
        end

    end
    
    for conC = 1:numel(ProtoMatrix) % convert 3D cells in protomatrix to 2D cells
        ProtoMatrix{conC} = reshape(ProtoMatrix{conC}, [41 size(ProtoMatrix{conC},2)*size(ProtoMatrix{conC},3)]);
    end
    
    for conC = 1:numel(ProtoMatrix) % baseline computation
        BaselineVector{conC} = ProtoMatrix{conC}(14, :);
        for i = 1:size(ProtoMatrix{conC}, 2)
            ProtoMatrix{conC}(:, i) = ((ProtoMatrix{conC}(:, i) - BaselineVector{conC}(1,i)) / BaselineVector{conC}(1,i)) * 100;
        end
        EventAvg{conC} = nanmean(ProtoMatrix{conC}(11:35,:), 2);
        ErrMargin{conC} = (nanstd(ProtoMatrix{conC}(11:35,:),0,2)/sqrt(size(ProtoMatrix{conC},2)))*1.96;
        PeakERA{conC} = max(EventAvg{conC});
        TimeToPeakERA{conC} = find(abs(EventAvg{conC}-PeakERA{conC}) < 0.001) * 0.625;
        TimeToPeakERA{conC} = TimeToPeakERA{conC}(1);
    end
        EventAvg = reshape(EventAvg, [3,3]);
        PeakERA = reshape(PeakERA, [3 3]);
        TimeToPeakERA = reshape(TimeToPeakERA, [3 3]);
        
    for lengths = 1:3
        for ints = 1:3
            
            input = ProtoMatrix{lengths,ints}(11:34,:); % 20s timecourses
            trials = size(ProtoMatrix{lengths,ints}, 2); % total number of events
            totalcourselength = numel(ProtoMatrix{lengths,ints}); % calculate total number of volumes going in
            timecourselength = 15; % length of each timecourse (s)
            numvols = 24; % number of volumes per event
            
            % simulate SPM structs
            SPM.xY.RT = 0.625; % repetition time (s)
            SPM.nscan = trials*24; % total no. of volumes (num of events * num of vols per event)
            SPM.xBF.T = 6.25; % no. of subdivisions of TR
            SPM.xBF.name = 'hrf';
            SPM.xBF.dt = SPM.xY.RT/SPM.xBF.T; % length of each subdivision (s)
            SPM.xBF.UNITS = 'secs';
            SPM.Sess.U.name = {sprintf('%s', ['vis', '_','l', num2str(lengths), 'i', num2str(ints)])}; % name of condition
            SPM.Sess.U.u = []; % this is required
            SPM.Sess.U.ons = [3:timecourselength:totalcourselength*SPM.xY.RT]; % event onsets in UNITS
            SPM.Sess.U.dur = 0; %eventlengths(lengths); %0; % set 0 for event-related designs (counter-intuitive I know; see section 8.2.1 of SPM manual)
            SPM.Sess.U.orth = 1; % orthogonality of EV
            SPM.Sess.U = spa_get_ons(SPM,1); % builds condition specification from info thus far

            s = length(SPM.Sess);
            Sess = SPM.Sess(s);
            U.dt = Sess.U(1).dt;
            u    = length(Sess.U);
            U.name = SPM.Sess.U.name;
            U.u    = Sess.U(1).u(33:end,1);

            TE    = 0.04;

            Y.y = reshape(input, [trials*numvols, 1]); % converts protomatrix into one long timecourse
            Y.dt = 0.625; %SPM.xY.RT;

            % Model specification: m input; 4 states; 1 output; m + 6 parameters
            %--------------------------------------------------------------------------
            % u(m) - mth stimulus function     (u)
            %
            % x(1) - vascular signal           log(s)
            % x(2) - rCBF                      log(f)
            % x(3) - venous volume             log(v)
            % x(4) - deoyxHb                   log(q)
            %
            % y(1) - BOLD                      (y)
            %
            % P(1)       - signal decay               d(ds/dt)/ds)      half-life = log(2)/P(1) ~ 1sec
            % P(2)       - autoregulation             d(ds/dt)/df)      2*pi*sqrt(1/P(1)) ~ 10 sec
            % P(3)       - transit time               (t0)              ~ 1 sec
            % P(4)       - exponent for Fout(v)       (alpha)           c.f. Grubb's exponent (~ 0.38)
            % P(5)       - resting oxygen extraction  (E0)              ~ range 20 - 50%
            % P(6)       - ratio of intra- to extra-  (epsilon)         ~ range 0.5 - 2
            %              vascular components   
            %              of the gradient echo signal   

            % P(6 + 1:m) - input efficacies - d(ds/dt)/du)  ~0.3 per event
            %--------------------------------------------------------------------------

            % priors (3 modes of hemodynamic variation)
            %--------------------------------------------------------------------------
            m       = size(U.u,2);
            [pE,pC] = spa_hdm_priors(m,3);

            % model
            %--------------------------------------------------------------------------
            M.f     = 'spa_fx_hdm';
            M.g     = 'spa_gx_hdm';
            M.x     = [0 0 0 0]'; 
            M.pE    = pE;    
            M.pC    = pC;
            M.m     = m;
            M.n     = 4;
            M.l     = 1;
            M.N     = 1500; % upscaling of TR for smooth model fits
            M.dt    = timecourselength/M.N; % event lengths (s) / M.N
            M.TE    = TE;
            
            % nonlinear system identification
            %--------------------------------------------------------------------------
            [Ep,Cp,Ce,K0,K1,K2,M0,M1,L1,L2,F] = spa_nlsi(M,U,Y);
            
            t       = [1:M.N]*M.dt;
            Fhdm    = spm_figure;
            set(Fhdm,'name','Hemodynamic Modeling')

            % display input parameters
            %--------------------------------------------------------------------------
            subplot(2,2,1)
            P     = Ep(7:end);
            C     = diag(Cp(7:end,7:end));
            [i,j] = max(abs(P));
            spm_barh(P,C)
            axis square
            title({'stimulus efficacy'; 'with 90% confidence intervals'},'FontSize',10)
            set(gca,'Ytick',[1:m],'YTickLabel',U.name,'FontSize',8)
            str = {};
            for i = 1:m
                str{end + 1} = U.name{i};
                str{end + 1} = sprintf('mean = %0.2f',P(i));
                str{end + 1} = '';
            end
            set(gca,'Ytick',[1:m*3]/3 + 1/2,'YTickLabel',str)
            xlabel('relative efficacy per event/sec')

            % display hemodynamic parameters
            %--------------------------------------------------------------------------
            subplot(2,2,3)
            P     = Ep(1:6);
            pE    = pE(1:6);
            C     = diag(Cp(1:6,1:6));
            spm_barh(P,C,pE)
            title({ 'hemodynamic parameters'},'FontSize',10)
            set(gca,'Ytick',[1:18]/3 + 1/2)
            set(gca,'YTickLabel',{  'SIGNAL decay',...
                        sprintf('%0.2f per sec',P(1)),'',...
                        'FEEDBACK',...
                        sprintf('%0.2f per sec',P(2)),'',...
                        'TRANSIT TIME',...
                        sprintf('%0.2f seconds',P(3)),'',...
                        'EXPONENT',...
                        sprintf('%0.2f',P(4)),'',...
                        'EXTRACTION',...
                        sprintf('%0.0f %s',P(5)*100,'%'),'',...
                        'log SIGNAL RATIO',...
                        sprintf('%0.2f %s',P(6),'%'),''},'FontSize',8)


            % get display state kernels (i.e. state dynamics) 
            %==========================================================================

            % Volterra kernels of states
            %--------------------------------------------------------------------------
            [H0,H1] = spa_kernels(M0,M1,M.N,M.dt);

            subplot(3,2,2)
            plot(t,exp(H1(:,:,j)))
            axis square
            title({['1st order kernels for ' U.name{j}];...
                'state variables'},'FontSize',9)
            ylabel('normalized values')
            legend({'s','f','v','q'}, 'Location','Best');
            grid on


            % display output kernels (i.e. BOLD response) 
            %--------------------------------------------------------------------------
            subplot(3,2,4)
            plot(t,K1(:,:,j))
            axis square
            title({'1st order kernel';...
                'output: BOLD'},'FontSize',9)
            ylabel('normalized flow signal')
            grid on

            subplot(3,2,6)
            imagesc(t,t,K2(:,:,1,j,j))
            axis square
            title({'2nd order kernel';...
                'output: BOLD'},'FontSize',9)
            xlabel({'time \{seconds\} for'; U.name{j}})
            grid on
            %%
            H2 = exp(H1);
            Hsave{lengths,ints}(:,:,fileidx) = H2;
            Hmean{lengths,ints} = nanmean(Hsave{lengths,ints},3);
            FPcovariance{lengths,ints}(1:numel(covs),fileidx) = covs;
            p = 0.4;
            v0 = 0.02;
            nu0   = 40.3;
            r0 = 25;
            E0 = P(5);
            epsi = exp(P(6));
            k1 = 6.7; %4.3.*nu0.*E0.*TE;
            k2 = 2.73; %epsi.*r0.*E0.*TE; 
            k3    = 1-epsi;
            modelvoltkernel1{lengths,ints}(:,fileidx) = 100/p*v0*(k1.*(1-Hmean{lengths,ints}(:,4))+k2.*(1-Hmean{lengths,ints}(:,4)./Hmean{lengths,ints}(:,3)+k3.*(1-Hmean{lengths,ints}(:,3))));
            modelvoltkernel2{lengths,ints}(:,fileidx) = K1;
            fullEMPTC{lengths,ints}(1:numel(signal)) = signal;
            close
        end
    end
    for lengths = 1:3
        for ints = 1:3
            Volkernel1{lengths, ints} = nanmean(modelvoltkernel1{lengths, ints},2);
            Volkernel2{lengths, ints} = nanmean(modelvoltkernel2{lengths, ints},2);
            rsEMPTC{lengths,ints} = reshape(fullEMPTC{lengths,ints}, 24, numel(fullEMPTC{lengths,ints})/24);
            EMPredict{lengths,ints} = nanmean(rsEMPTC{lengths, ints}(:,2:end),2);
            EMPredictSeg{lengths,ints} = EMPredict{lengths,ints};
            EMPredictSmooth{lengths,ints}.hrf = resample(EMPredict{lengths,ints}, 1000,1);
            [EMPredictSmooth{lengths,ints}.MaxValue, EMPredictSmooth{lengths,ints}.MaxIndex] = max(EMPredictSmooth{lengths,ints}.hrf);
            EMPredictSmooth{lengths,ints}.MaxIndexInSeconds = EMPredictSmooth{lengths,ints}.MaxIndex / 1000 * 0.625;
            
            EMPredictSmooth{lengths, ints}.HalfMax = (EMPredictSmooth{lengths,ints}.MaxValue-0)/2 + 0;
            EMPredictSmooth{lengths,ints}.OnsetMax = (EMPredictSmooth{lengths,ints}.MaxValue-0)/10 + 0;
            [EMPredictSmooth{lengths,ints}.MinValue, EMPredictSmooth{lengths,ints}.MinIndex] = min(EMPredictSmooth{lengths,ints}.hrf);

            for i = EMPredictSmooth{lengths,ints}.MaxIndex:-1:1
                EMPredictSmooth{lengths,ints}.HMPoint1 = i;
                if EMPredictSmooth{lengths,ints}.hrf(i) < EMPredictSmooth{lengths,ints}.HalfMax
                    break
                end
            end

            for i = EMPredictSmooth{lengths,ints}.MaxIndex:24000
                EMPredictSmooth{lengths,ints}.HMPoint2 = i;
                if EMPredictSmooth{lengths,ints}.hrf(i) < EMPredictSmooth{lengths,ints}.HalfMax
                    break
                end
            end

            for i = EMPredictSmooth{lengths,ints}.MaxIndex:-1:1
                EMPredictSmooth{lengths,ints}.RisePoint = i;
                if EMPredictSmooth{lengths,ints}.hrf(i) < EMPredictSmooth{lengths,ints}.OnsetMax
                    break
                end
            end

            EMPredictSmooth{lengths,ints}.FWHM = EMPredictSmooth{lengths,ints}.HMPoint2 - EMPredictSmooth{lengths,ints}.HMPoint1;
            EMPredictSmooth{lengths,ints}.OnsetTimeInSeconds = EMPredictSmooth{lengths,ints}.RisePoint / 1000 * 0.625;
            EMPredictSmooth{lengths,ints}.DispersionInSeconds = EMPredictSmooth{lengths,ints}.FWHM / 1000 * 0.625;
            
            PeakTimeMatrix{lengths,ints} = EMPredictSmooth{lengths,ints}.MaxIndexInSeconds;
            PeakAmplitudeMatrix{lengths,ints} = EMPredictSmooth{lengths,ints}.MaxValue;
            FWHMMatrix{lengths,ints} = EMPredictSmooth{lengths,ints}.DispersionInSeconds;
            PeakOnsetMatrix{lengths, ints} = EMPredictSmooth{lengths,ints}.OnsetTimeInSeconds;
            HRFMatrix{lengths,ints} = EMPredictSmooth{lengths,ints}.hrf;
        end
    end
    AllStats{fileidx,1} = [{PeakTimeMatrix}, {PeakAmplitudeMatrix}, {PeakERA}, {TimeToPeakERA}, {FWHMMatrix}, {PeakOnsetMatrix}, {HRFMatrix}];
end
%%

for idx = 1:numel(filedir)

% Analysis for I1 to I3 at L3
PeakAmplitude(1, idx) = cell2mat(AllStats{idx}{1,2}(3,1));
PeakAmplitude(2, idx) = cell2mat(AllStats{idx}{1,2}(3,2));
PeakAmplitude(3, idx) = cell2mat(AllStats{idx}{1,2}(3,3));
PeakAmpDiff(1, idx) = (cell2mat(AllStats{idx}{1,2}(3,3)) - cell2mat(AllStats{idx}{1,2}(3,2)));
PeakAmpDiff(2, idx) = (cell2mat(AllStats{idx}{1,2}(3,2)) - cell2mat(AllStats{idx}{1,2}(3,1)));
PeakAmpDiff(3, idx) = (cell2mat(AllStats{idx}{1,2}(3,3)) - cell2mat(AllStats{idx}{1,2}(3,1)));

PeakDelay(1, idx) = cell2mat(AllStats{idx}{1,1}(3,1));
PeakDelay(2, idx) = cell2mat(AllStats{idx}{1,1}(3,2));
PeakDelay(3, idx) = cell2mat(AllStats{idx}{1,1}(3,3));
PeakDelDiff(1, idx) = (cell2mat(AllStats{idx}{1,1}(3,3)) - cell2mat(AllStats{idx}{1,1}(3,2)));
PeakDelDiff(2, idx) = (cell2mat(AllStats{idx}{1,1}(3,2)) - cell2mat(AllStats{idx}{1,1}(3,1)));
PeakDelDiff(3, idx) = (cell2mat(AllStats{idx}{1,1}(3,3)) - cell2mat(AllStats{idx}{1,1}(3,1)));

PeakEra(1, idx) = cell2mat(AllStats{idx}{1,3}(3,1));
PeakEra(2, idx) = cell2mat(AllStats{idx}{1,3}(3,2));
PeakEra(3, idx) = cell2mat(AllStats{idx}{1,3}(3,3));
PeakEraDiff(1, idx) = (cell2mat(AllStats{idx}{1,3}(3,3)) - cell2mat(AllStats{idx}{1,3}(3,2)));
PeakEraDiff(2, idx) = (cell2mat(AllStats{idx}{1,3}(3,2)) - cell2mat(AllStats{idx}{1,3}(3,1)));
PeakEraDiff(3, idx) = (cell2mat(AllStats{idx}{1,3}(3,3)) - cell2mat(AllStats{idx}{1,3}(3,1)));

TTPEra(1, idx) = cell2mat(AllStats{idx}{1,4}(3,1));
TTPEra(2, idx) = cell2mat(AllStats{idx}{1,4}(3,2));
TTPEra(3, idx) = cell2mat(AllStats{idx}{1,4}(3,3));
TTPEraDiff(1, idx) = (cell2mat(AllStats{idx}{1,4}(3,3)) - cell2mat(AllStats{idx}{1,4}(3,2)));
TTPEraDiff(2, idx) = (cell2mat(AllStats{idx}{1,4}(3,2)) - cell2mat(AllStats{idx}{1,4}(3,1)));
TTPEraDiff(3, idx) = (cell2mat(AllStats{idx}{1,4}(3,3)) - cell2mat(AllStats{idx}{1,4}(3,1)));

PeakDispersion(1, idx) = cell2mat(AllStats{idx}{1,5}(3,1));
PeakDispersion(2, idx) = cell2mat(AllStats{idx}{1,5}(3,2));
PeakDispersion(3, idx) = cell2mat(AllStats{idx}{1,5}(3,3));
PeakDispDiff(1, idx) = (cell2mat(AllStats{idx}{1,5}(3,3)) - cell2mat(AllStats{idx}{1,5}(3,2)));
PeakDispDiff(2, idx) = (cell2mat(AllStats{idx}{1,5}(3,2)) - cell2mat(AllStats{idx}{1,5}(3,1)));
PeakDispDiff(3, idx) = (cell2mat(AllStats{idx}{1,5}(3,3)) - cell2mat(AllStats{idx}{1,5}(3,1)));

PeakOnset(1, idx) = cell2mat(AllStats{idx}{1,6}(3,1));
PeakOnset(2, idx) = cell2mat(AllStats{idx}{1,6}(3,2));
PeakOnset(3, idx) = cell2mat(AllStats{idx}{1,6}(3,3));
PeakOnsDiff(1, idx) = (cell2mat(AllStats{idx}{1,6}(3,3)) - cell2mat(AllStats{idx}{1,6}(3,2)));
PeakOnsDiff(2, idx) = (cell2mat(AllStats{idx}{1,6}(3,2)) - cell2mat(AllStats{idx}{1,6}(3,1)));
PeakOnsDiff(3, idx) = (cell2mat(AllStats{idx}{1,6}(3,3)) - cell2mat(AllStats{idx}{1,6}(3,1)));

% Analysis for L1 to L3 at I3
PeakAmplitude(4, idx) = cell2mat(AllStats{idx}{1,2}(1,3));
PeakAmplitude(5, idx) = cell2mat(AllStats{idx}{1,2}(2,3));
PeakAmplitude(6, idx) = cell2mat(AllStats{idx}{1,2}(3,3));
PeakAmpDiff(4, idx) = (cell2mat(AllStats{idx}{1,2}(3,3)) - cell2mat(AllStats{idx}{1,2}(2,3)));
PeakAmpDiff(5, idx) = (cell2mat(AllStats{idx}{1,2}(2,3)) - cell2mat(AllStats{idx}{1,2}(1,3)));
PeakAmpDiff(6, idx) = (cell2mat(AllStats{idx}{1,2}(3,3)) - cell2mat(AllStats{idx}{1,2}(1,3)));

PeakDelay(4, idx) = cell2mat(AllStats{idx}{1,1}(1,3));
PeakDelay(5, idx) = cell2mat(AllStats{idx}{1,1}(2,3));
PeakDelay(6, idx) = cell2mat(AllStats{idx}{1,1}(3,3));
PeakDelDiff(4, idx) = (cell2mat(AllStats{idx}{1,1}(3,3)) - cell2mat(AllStats{idx}{1,1}(2,3)));
PeakDelDiff(5, idx) = (cell2mat(AllStats{idx}{1,1}(2,3)) - cell2mat(AllStats{idx}{1,1}(1,3)));
PeakDelDiff(6, idx) = (cell2mat(AllStats{idx}{1,1}(3,3)) - cell2mat(AllStats{idx}{1,1}(1,3)));

PeakEra(4, idx) = cell2mat(AllStats{idx}{1,3}(1,3));
PeakEra(5, idx) = cell2mat(AllStats{idx}{1,3}(2,3));
PeakEra(6, idx) = cell2mat(AllStats{idx}{1,3}(3,3));
PeakEraDiff(4, idx) = (cell2mat(AllStats{idx}{1,3}(3,3)) - cell2mat(AllStats{idx}{1,3}(2,3)));
PeakEraDiff(5, idx) = (cell2mat(AllStats{idx}{1,3}(2,3)) - cell2mat(AllStats{idx}{1,3}(1,3)));
PeakEraDiff(6, idx) = (cell2mat(AllStats{idx}{1,3}(3,3)) - cell2mat(AllStats{idx}{1,3}(1,3)));

TTPEra(4, idx) = cell2mat(AllStats{idx}{1,4}(1,3));
TTPEra(5, idx) = cell2mat(AllStats{idx}{1,4}(2,3));
TTPEra(6, idx) = cell2mat(AllStats{idx}{1,4}(3,3));
TTPEraDiff(4, idx) = (cell2mat(AllStats{idx}{1,4}(3,3)) - cell2mat(AllStats{idx}{1,4}(2,3)));
TTPEraDiff(5, idx) = (cell2mat(AllStats{idx}{1,4}(2,3)) - cell2mat(AllStats{idx}{1,4}(1,3)));
TTPEraDiff(6, idx) = (cell2mat(AllStats{idx}{1,4}(3,3)) - cell2mat(AllStats{idx}{1,4}(1,3)));

PeakDispersion(4, idx) = cell2mat(AllStats{idx}{1,5}(1,3));
PeakDispersion(5, idx) = cell2mat(AllStats{idx}{1,5}(2,3));
PeakDispersion(6, idx) = cell2mat(AllStats{idx}{1,5}(3,3));
PeakDispDiff(4, idx) = (cell2mat(AllStats{idx}{1,5}(3,3)) - cell2mat(AllStats{idx}{1,5}(2,3)));
PeakDispDiff(5, idx) = (cell2mat(AllStats{idx}{1,5}(2,3)) - cell2mat(AllStats{idx}{1,5}(1,3)));
PeakDispDiff(6, idx) = (cell2mat(AllStats{idx}{1,5}(3,3)) - cell2mat(AllStats{idx}{1,5}(1,3)));

PeakOnset(4, idx) = cell2mat(AllStats{idx}{1,6}(1,3));
PeakOnset(5, idx) = cell2mat(AllStats{idx}{1,6}(2,3));
PeakOnset(6, idx) = cell2mat(AllStats{idx}{1,6}(3,3));
PeakOnsDiff(4, idx) = (cell2mat(AllStats{idx}{1,6}(3,3)) - cell2mat(AllStats{idx}{1,6}(2,3)));
PeakOnsDiff(5, idx) = (cell2mat(AllStats{idx}{1,6}(2,3)) - cell2mat(AllStats{idx}{1,6}(1,3)));
PeakOnsDiff(6, idx) = (cell2mat(AllStats{idx}{1,6}(3,3)) - cell2mat(AllStats{idx}{1,6}(1,3)));

% Analysis for I1 to I3 at L2
PeakAmplitude(7, idx) = cell2mat(AllStats{idx}{1,2}(2,1));
PeakAmplitude(8, idx) = cell2mat(AllStats{idx}{1,2}(2,2));
PeakAmplitude(9, idx) = cell2mat(AllStats{idx}{1,2}(2,3));
PeakAmpDiff(7, idx) = (cell2mat(AllStats{idx}{1,2}(2,3)) - cell2mat(AllStats{idx}{1,2}(2,2)));
PeakAmpDiff(8, idx) = (cell2mat(AllStats{idx}{1,2}(2,2)) - cell2mat(AllStats{idx}{1,2}(2,1)));
PeakAmpDiff(9, idx) = (cell2mat(AllStats{idx}{1,2}(2,3)) - cell2mat(AllStats{idx}{1,2}(2,1)));

PeakDelay(7, idx) = cell2mat(AllStats{idx}{1,1}(2,1));
PeakDelay(8, idx) = cell2mat(AllStats{idx}{1,1}(2,2));
PeakDelay(9, idx) = cell2mat(AllStats{idx}{1,1}(2,3));
PeakDelDiff(7, idx) = (cell2mat(AllStats{idx}{1,1}(2,3)) - cell2mat(AllStats{idx}{1,1}(2,2)));
PeakDelDiff(8, idx) = (cell2mat(AllStats{idx}{1,1}(2,2)) - cell2mat(AllStats{idx}{1,1}(2,1)));
PeakDelDiff(9, idx) = (cell2mat(AllStats{idx}{1,1}(2,3)) - cell2mat(AllStats{idx}{1,1}(2,1)));

PeakEra(7, idx) = cell2mat(AllStats{idx}{1,3}(2,1));
PeakEra(8, idx) = cell2mat(AllStats{idx}{1,3}(2,2));
PeakEra(9, idx) = cell2mat(AllStats{idx}{1,3}(2,3));
PeakEraDiff(7, idx) = (cell2mat(AllStats{idx}{1,3}(2,3)) - cell2mat(AllStats{idx}{1,3}(2,2)));
PeakEraDiff(8, idx) = (cell2mat(AllStats{idx}{1,3}(2,2)) - cell2mat(AllStats{idx}{1,3}(2,1)));
PeakEraDiff(9, idx) = (cell2mat(AllStats{idx}{1,3}(2,3)) - cell2mat(AllStats{idx}{1,3}(2,1)));

TTPEra(7, idx) = cell2mat(AllStats{idx}{1,4}(2,1));
TTPEra(8, idx) = cell2mat(AllStats{idx}{1,4}(2,2));
TTPEra(9, idx) = cell2mat(AllStats{idx}{1,4}(2,3));
TTPEraDiff(7, idx) = (cell2mat(AllStats{idx}{1,4}(2,3)) - cell2mat(AllStats{idx}{1,4}(2,2)));
TTPEraDiff(8, idx) = (cell2mat(AllStats{idx}{1,4}(2,2)) - cell2mat(AllStats{idx}{1,4}(2,1)));
TTPEraDiff(9, idx) = (cell2mat(AllStats{idx}{1,4}(2,3)) - cell2mat(AllStats{idx}{1,4}(2,1)));

PeakDispersion(7, idx) = cell2mat(AllStats{idx}{1,5}(2,1));
PeakDispersion(8, idx) = cell2mat(AllStats{idx}{1,5}(2,2));
PeakDispersion(9, idx) = cell2mat(AllStats{idx}{1,5}(2,3));
PeakDispDiff(7, idx) = (cell2mat(AllStats{idx}{1,5}(2,3)) - cell2mat(AllStats{idx}{1,5}(2,2)));
PeakDispDiff(8, idx) = (cell2mat(AllStats{idx}{1,5}(2,2)) - cell2mat(AllStats{idx}{1,5}(2,1)));
PeakDispDiff(9, idx) = (cell2mat(AllStats{idx}{1,5}(2,3)) - cell2mat(AllStats{idx}{1,5}(2,1)));

PeakOnset(7, idx) = cell2mat(AllStats{idx}{1,6}(2,1));
PeakOnset(8, idx) = cell2mat(AllStats{idx}{1,6}(2,2));
PeakOnset(9, idx) = cell2mat(AllStats{idx}{1,6}(2,3));
PeakOnsDiff(7, idx) = (cell2mat(AllStats{idx}{1,6}(2,3)) - cell2mat(AllStats{idx}{1,6}(2,2)));
PeakOnsDiff(8, idx) = (cell2mat(AllStats{idx}{1,6}(2,2)) - cell2mat(AllStats{idx}{1,6}(2,1)));
PeakOnsDiff(9, idx) = (cell2mat(AllStats{idx}{1,6}(2,3)) - cell2mat(AllStats{idx}{1,6}(2,1)));

% Analysis for L1 to L3 at I2
PeakAmplitude(10, idx) = cell2mat(AllStats{idx}{1,2}(1,2));
PeakAmplitude(11, idx) = cell2mat(AllStats{idx}{1,2}(2,2));
PeakAmplitude(12, idx) = cell2mat(AllStats{idx}{1,2}(3,2));
PeakAmpDiff(10, idx) = (cell2mat(AllStats{idx}{1,2}(3,2)) - cell2mat(AllStats{idx}{1,2}(2,2)));
PeakAmpDiff(11, idx) = (cell2mat(AllStats{idx}{1,2}(2,2)) - cell2mat(AllStats{idx}{1,2}(1,2)));
PeakAmpDiff(12, idx) = (cell2mat(AllStats{idx}{1,2}(3,2)) - cell2mat(AllStats{idx}{1,2}(1,2)));

PeakDelay(10, idx) = cell2mat(AllStats{idx}{1,1}(1,2));
PeakDelay(11, idx) = cell2mat(AllStats{idx}{1,1}(2,2));
PeakDelay(12, idx) = cell2mat(AllStats{idx}{1,1}(3,2));
PeakDelDiff(10, idx) = (cell2mat(AllStats{idx}{1,1}(3,2)) - cell2mat(AllStats{idx}{1,1}(2,2)));
PeakDelDiff(11, idx) = (cell2mat(AllStats{idx}{1,1}(2,2)) - cell2mat(AllStats{idx}{1,1}(1,2)));
PeakDelDiff(12, idx) = (cell2mat(AllStats{idx}{1,1}(3,2)) - cell2mat(AllStats{idx}{1,1}(1,2)));

PeakEra(10, idx) = cell2mat(AllStats{idx}{1,3}(1,2));
PeakEra(11, idx) = cell2mat(AllStats{idx}{1,3}(2,2));
PeakEra(12, idx) = cell2mat(AllStats{idx}{1,3}(3,2));
PeakEraDiff(10, idx) = (cell2mat(AllStats{idx}{1,3}(3,2)) - cell2mat(AllStats{idx}{1,3}(2,2)));
PeakEraDiff(11, idx) = (cell2mat(AllStats{idx}{1,3}(2,2)) - cell2mat(AllStats{idx}{1,3}(1,2)));
PeakEraDiff(12, idx) = (cell2mat(AllStats{idx}{1,3}(3,2)) - cell2mat(AllStats{idx}{1,3}(1,2)));

TTPEra(10, idx) = cell2mat(AllStats{idx}{1,4}(1,2));
TTPEra(11, idx) = cell2mat(AllStats{idx}{1,4}(2,2));
TTPEra(12, idx) = cell2mat(AllStats{idx}{1,4}(3,2));
TTPEraDiff(10, idx) = (cell2mat(AllStats{idx}{1,4}(3,2)) - cell2mat(AllStats{idx}{1,4}(2,2)));
TTPEraDiff(11, idx) = (cell2mat(AllStats{idx}{1,4}(2,2)) - cell2mat(AllStats{idx}{1,4}(1,2)));
TTPEraDiff(12, idx) = (cell2mat(AllStats{idx}{1,4}(3,2)) - cell2mat(AllStats{idx}{1,4}(1,2)));

PeakDispersion(10, idx) = cell2mat(AllStats{idx}{1,5}(1,2));
PeakDispersion(11, idx) = cell2mat(AllStats{idx}{1,5}(2,2));
PeakDispersion(12, idx) = cell2mat(AllStats{idx}{1,5}(3,2));
PeakDispDiff(10, idx) = (cell2mat(AllStats{idx}{1,5}(3,2)) - cell2mat(AllStats{idx}{1,5}(2,2)));
PeakDispDiff(11, idx) = (cell2mat(AllStats{idx}{1,5}(2,2)) - cell2mat(AllStats{idx}{1,5}(1,2)));
PeakDispDiff(12, idx) = (cell2mat(AllStats{idx}{1,5}(3,2)) - cell2mat(AllStats{idx}{1,5}(1,2)));

PeakOnset(10, idx) = cell2mat(AllStats{idx}{1,6}(1,2));
PeakOnset(11, idx) = cell2mat(AllStats{idx}{1,6}(2,2));
PeakOnset(12, idx) = cell2mat(AllStats{idx}{1,6}(3,2));
PeakOnsDiff(10, idx) = (cell2mat(AllStats{idx}{1,6}(3,2)) - cell2mat(AllStats{idx}{1,6}(2,2)));
PeakOnsDiff(11, idx) = (cell2mat(AllStats{idx}{1,6}(2,2)) - cell2mat(AllStats{idx}{1,6}(1,2)));
PeakOnsDiff(12, idx) = (cell2mat(AllStats{idx}{1,6}(3,2)) - cell2mat(AllStats{idx}{1,6}(1,2)));

% Analysis for I1 to I3 at L1

PeakAmplitude(13, idx) = cell2mat(AllStats{idx}{1,2}(1,1));
PeakAmplitude(14, idx) = cell2mat(AllStats{idx}{1,2}(1,2));
PeakAmplitude(15, idx) = cell2mat(AllStats{idx}{1,2}(1,3));
PeakAmpDiff(13, idx) = (cell2mat(AllStats{idx}{1,2}(1,3)) - cell2mat(AllStats{idx}{1,2}(1,2)));
PeakAmpDiff(14, idx) = (cell2mat(AllStats{idx}{1,2}(1,2)) - cell2mat(AllStats{idx}{1,2}(1,1)));
PeakAmpDiff(15, idx) = (cell2mat(AllStats{idx}{1,2}(1,3)) - cell2mat(AllStats{idx}{1,2}(1,1)));

PeakDelay(13, idx) = cell2mat(AllStats{idx}{1,1}(1,1));
PeakDelay(14, idx) = cell2mat(AllStats{idx}{1,1}(1,2));
PeakDelay(15, idx) = cell2mat(AllStats{idx}{1,1}(1,3));
PeakDelDiff(13, idx) = (cell2mat(AllStats{idx}{1,1}(1,3)) - cell2mat(AllStats{idx}{1,1}(1,2)));
PeakDelDiff(14, idx) = (cell2mat(AllStats{idx}{1,1}(1,2)) - cell2mat(AllStats{idx}{1,1}(1,1)));
PeakDelDiff(15, idx) = (cell2mat(AllStats{idx}{1,1}(1,3)) - cell2mat(AllStats{idx}{1,1}(1,1)));

PeakEra(13, idx) = cell2mat(AllStats{idx}{1,3}(1,1));
PeakEra(14, idx) = cell2mat(AllStats{idx}{1,3}(1,2));
PeakEra(15, idx) = cell2mat(AllStats{idx}{1,3}(1,3));
PeakEraDiff(13, idx) = (cell2mat(AllStats{idx}{1,3}(1,3)) - cell2mat(AllStats{idx}{1,3}(1,2)));
PeakEraDiff(14, idx) = (cell2mat(AllStats{idx}{1,3}(1,2)) - cell2mat(AllStats{idx}{1,3}(1,1)));
PeakEraDiff(15, idx) = (cell2mat(AllStats{idx}{1,3}(1,3)) - cell2mat(AllStats{idx}{1,3}(1,1)));

TTPEra(13, idx) = cell2mat(AllStats{idx}{1,4}(1,1));
TTPEra(14, idx) = cell2mat(AllStats{idx}{1,4}(1,2));
TTPEra(15, idx) = cell2mat(AllStats{idx}{1,4}(1,3));
TTPEraDiff(13, idx) = (cell2mat(AllStats{idx}{1,4}(1,3)) - cell2mat(AllStats{idx}{1,4}(1,2)));
TTPEraDiff(14, idx) = (cell2mat(AllStats{idx}{1,4}(1,2)) - cell2mat(AllStats{idx}{1,4}(1,1)));
TTPEraDiff(15, idx) = (cell2mat(AllStats{idx}{1,4}(1,3)) - cell2mat(AllStats{idx}{1,4}(1,1)));

PeakDispersion(13, idx) = cell2mat(AllStats{idx}{1,5}(1,1));
PeakDispersion(14, idx) = cell2mat(AllStats{idx}{1,5}(1,2));
PeakDispersion(15, idx) = cell2mat(AllStats{idx}{1,5}(1,3));
PeakDispDiff(13, idx) = (cell2mat(AllStats{idx}{1,5}(1,3)) - cell2mat(AllStats{idx}{1,5}(1,2)));
PeakDispDiff(14, idx) = (cell2mat(AllStats{idx}{1,5}(1,2)) - cell2mat(AllStats{idx}{1,5}(1,1)));
PeakDispDiff(15, idx) = (cell2mat(AllStats{idx}{1,5}(1,3)) - cell2mat(AllStats{idx}{1,5}(1,1)));

PeakOnset(13, idx) = cell2mat(AllStats{idx}{1,6}(1,1));
PeakOnset(14, idx) = cell2mat(AllStats{idx}{1,6}(1,2));
PeakOnset(15, idx) = cell2mat(AllStats{idx}{1,6}(1,3));
PeakOnsDiff(13, idx) = (cell2mat(AllStats{idx}{1,6}(1,3)) - cell2mat(AllStats{idx}{1,6}(1,2)));
PeakOnsDiff(14, idx) = (cell2mat(AllStats{idx}{1,6}(1,2)) - cell2mat(AllStats{idx}{1,6}(1,1)));
PeakOnsDiff(15, idx) = (cell2mat(AllStats{idx}{1,6}(1,3)) - cell2mat(AllStats{idx}{1,6}(1,1)));

% Analysis for L1 to L3 at I1
PeakAmplitude(16, idx) = cell2mat(AllStats{idx}{1,2}(1,1));
PeakAmplitude(17, idx) = cell2mat(AllStats{idx}{1,2}(2,1));
PeakAmplitude(18, idx) = cell2mat(AllStats{idx}{1,2}(3,1));
PeakAmpDiff(16, idx) = (cell2mat(AllStats{idx}{1,2}(3,1)) - cell2mat(AllStats{idx}{1,2}(2,1)));
PeakAmpDiff(17, idx) = (cell2mat(AllStats{idx}{1,2}(2,1)) - cell2mat(AllStats{idx}{1,2}(1,1)));
PeakAmpDiff(18, idx) = (cell2mat(AllStats{idx}{1,2}(3,1)) - cell2mat(AllStats{idx}{1,2}(1,1)));

PeakDelay(16, idx) = cell2mat(AllStats{idx}{1,1}(1,1));
PeakDelay(17, idx) = cell2mat(AllStats{idx}{1,1}(2,1));
PeakDelay(18, idx) = cell2mat(AllStats{idx}{1,1}(3,1));
PeakDelDiff(16, idx) = (cell2mat(AllStats{idx}{1,1}(3,1)) - cell2mat(AllStats{idx}{1,1}(2,1)));
PeakDelDiff(17, idx) = (cell2mat(AllStats{idx}{1,1}(2,1)) - cell2mat(AllStats{idx}{1,1}(1,1)));
PeakDelDiff(18, idx) = (cell2mat(AllStats{idx}{1,1}(3,1)) - cell2mat(AllStats{idx}{1,1}(1,1)));

PeakEra(16, idx) = cell2mat(AllStats{idx}{1,3}(1,1));
PeakEra(17, idx) = cell2mat(AllStats{idx}{1,3}(2,1));
PeakEra(18, idx) = cell2mat(AllStats{idx}{1,3}(3,1));
PeakEraDiff(16, idx) = (cell2mat(AllStats{idx}{1,3}(3,1)) - cell2mat(AllStats{idx}{1,3}(2,1)));
PeakEraDiff(17, idx) = (cell2mat(AllStats{idx}{1,3}(2,1)) - cell2mat(AllStats{idx}{1,3}(1,1)));
PeakEraDiff(18, idx) = (cell2mat(AllStats{idx}{1,3}(3,1)) - cell2mat(AllStats{idx}{1,3}(1,1)));

TTPEra(16, idx) = cell2mat(AllStats{idx}{1,4}(1,1));
TTPEra(17, idx) = cell2mat(AllStats{idx}{1,4}(2,1));
TTPEra(18, idx) = cell2mat(AllStats{idx}{1,4}(3,1));
TTPEraDiff(16, idx) = (cell2mat(AllStats{idx}{1,4}(3,1)) - cell2mat(AllStats{idx}{1,4}(2,1)));
TTPEraDiff(17, idx) = (cell2mat(AllStats{idx}{1,4}(2,1)) - cell2mat(AllStats{idx}{1,4}(1,1)));
TTPEraDiff(18, idx) = (cell2mat(AllStats{idx}{1,4}(3,1)) - cell2mat(AllStats{idx}{1,4}(1,1)));

PeakDispersion(16, idx) = cell2mat(AllStats{idx}{1,5}(1,1));
PeakDispersion(17, idx) = cell2mat(AllStats{idx}{1,5}(2,1));
PeakDispersion(18, idx) = cell2mat(AllStats{idx}{1,5}(3,1));
PeakDispDiff(16, idx) = (cell2mat(AllStats{idx}{1,5}(3,1)) - cell2mat(AllStats{idx}{1,5}(2,1)));
PeakDispDiff(17, idx) = (cell2mat(AllStats{idx}{1,5}(2,1)) - cell2mat(AllStats{idx}{1,5}(1,1)));
PeakDispDiff(18, idx) = (cell2mat(AllStats{idx}{1,5}(3,1)) - cell2mat(AllStats{idx}{1,5}(1,1)));

PeakOnset(16, idx) = cell2mat(AllStats{idx}{1,6}(1,1));
PeakOnset(17, idx) = cell2mat(AllStats{idx}{1,6}(2,1));
PeakOnset(18, idx) = cell2mat(AllStats{idx}{1,6}(3,1));
PeakOnsDiff(16, idx) = (cell2mat(AllStats{idx}{1,6}(3,1)) - cell2mat(AllStats{idx}{1,6}(2,1)));
PeakOnsDiff(17, idx) = (cell2mat(AllStats{idx}{1,6}(2,1)) - cell2mat(AllStats{idx}{1,6}(1,1)));
PeakOnsDiff(18, idx) = (cell2mat(AllStats{idx}{1,6}(3,1)) - cell2mat(AllStats{idx}{1,6}(1,1)));

end

for condix = 1:size(PeakAmplitude, 1)
    PAV = PeakAmplitude(condix, :)';
    PDV = PeakDelay(condix, :)';
    PADV = PeakAmpDiff(condix, :)';
    PDDV = PeakDelDiff(condix, :)';
    PERAV = PeakEra(condix, :)';
    TTPV = TTPEra(condix, :)';
    PERADV = PeakEraDiff(condix, :)';
    TTPDV = TTPEraDiff(condix, :)';
    PDPV = PeakDispersion(condix, :)';
    PDPDV = PeakDispDiff(condix, :)';
    PONV = PeakOnset(condix, :)';
    PONDV = PeakOnsDiff(condix, :)';
    
    ampmean(condix) = nanmean(PeakAmplitude(condix, :),2);
    delmean(condix) = nanmean(PeakDelay(condix, :),2);
    ampdiffmean(condix) = nanmean(PeakAmpDiff(condix, :),2);
    deldiffmean(condix) = nanmean(PeakDelDiff(condix, :),2);
    ampstd(condix) = nanstd(PeakAmplitude(condix, :));
    delstd(condix) = nanstd(PeakDelay(condix, :));
    ampdiffstd(condix) = nanstd(PeakAmpDiff(condix, :));
    deldiffstd(condix) = nanstd(PeakDelDiff(condix, :));
    
    amppd(condix) = fitdist(PAV, 'Normal');
    ampci{condix} = paramci(amppd(condix));
    ampci{condix} = ampci{condix}(:,1);
    delpd(condix) = fitdist(PDV, 'Normal');
    delci{condix} = paramci(delpd(condix));
    delci{condix} = delci{condix}(:,1);
    ampdiffpd(condix) = fitdist(PADV, 'Normal');
    ampdiffci{condix} = paramci(ampdiffpd(condix));
    ampdiffci{condix} = ampdiffci{condix}(:,1);
    deldiffpd(condix) = fitdist(PDDV, 'Normal');
    deldiffci{condix} = paramci(deldiffpd(condix));
    deldiffci{condix} = deldiffci{condix}(:,1);
    
    peramean(condix) = nanmean(PeakEra(condix,:),2);
    perastd(condix) = nanstd(PeakEra(condix,:));
    perapd(condix) = fitdist(PERAV, 'Normal');
    peraci{condix} = paramci(perapd(condix));
    peraci{condix} = peraci{condix}(:,1);
    peradiffmean(condix) = nanmean(PeakEraDiff(condix,:),2);
    peradiffstd(condix) = nanstd(PeakEraDiff(condix,:));
    peradiffpd(condix) = fitdist(PERADV, 'Normal');
    peradiffci{condix} = paramci(peradiffpd(condix));
    peradiffci{condix} = peradiffci{condix}(:,1);
    
    ttpmean(condix) = nanmean(TTPEra(condix,:),2);
    ttpstd(condix) = nanstd(TTPEra(condix,:));
    ttppd(condix) = fitdist(TTPV, 'Normal');
    ttpci{condix} = paramci(ttppd(condix));
    ttpci{condix} = ttpci{condix}(:,1);
    ttpdiffmean(condix) = nanmean(TTPEraDiff(condix,:),2);
    ttpdiffstd(condix) = nanstd(TTPEraDiff(condix,:));
    ttpdiffpd(condix) = fitdist(TTPDV, 'Normal');
    ttpdiffci{condix} = paramci(ttpdiffpd(condix));
    ttpdiffci{condix} = ttpdiffci{condix}(:,1);
    
    pdpmean(condix) = nanmean(PeakDispersion(condix,:),2);
    pdpstd(condix) = nanstd(PeakDispersion(condix,:));
    pdppd(condix) = fitdist(PDPV, 'Normal');
    pdpci{condix} = paramci(pdppd(condix));
    pdpci{condix} = pdpci{condix}(:,1);
    pdpdiffmean(condix) = nanmean(PeakDispDiff(condix,:),2);
    pdpdiffstd(condix) = nanstd(PeakDispDiff(condix,:));
    pdpdiffpd(condix) = fitdist(PDPDV, 'Normal');
    pdpdiffci{condix} = paramci(pdpdiffpd(condix));
    pdpdiffci{condix} = pdpdiffci{condix}(:,1);
    
    ponmean(condix) = nanmean(PeakOnset(condix,:),2);
    ponstd(condix) = nanstd(PeakOnset(condix,:));
    ponpd(condix) = fitdist(PONV, 'Normal');
    ponci{condix} = paramci(ponpd(condix));
    ponci{condix} = ponci{condix}(:,1);
    pondiffmean(condix) = nanmean(PeakOnsDiff(condix,:),2);
    pondiffstd(condix) = nanstd(PeakOnsDiff(condix,:));
    pondiffpd(condix) = fitdist(PONDV, 'Normal');
    pondiffci{condix} = paramci(pondiffpd(condix));
    pondiffci{condix} = pondiffci{condix}(:,1);
    
    ampeffect(condix) = ampdiffmean(condix)/ampdiffstd(condix);
    deleffect(condix) = deldiffmean(condix)/deldiffstd(condix);
    peraeffect(condix) = peradiffmean(condix)/peradiffstd(condix);
    ttpeffect(condix) = ttpdiffmean(condix)/ttpdiffstd(condix);
    pdpeffect(condix) = pdpdiffmean(condix)/pdpdiffstd(condix);
    poneffect(condix) = pondiffmean(condix)/pondiffstd(condix);
end
%%

varnames = [{'PeakAmplitude'}, {'PeakAmplitudeSD'}, {'PeakAmplitudeCI'}, {'PeakDelay'}, {'PeakDelaySD'}, {'PeakDelayCI'}, {'EvPeak'}, {'EvPeakSD'}, {'EvPeakCI'}, {'TimetoEvPeak'}, {'TimetoEvPeakSD'}, {'TimetoEvPeakCI'}, {'PeakDispersion'}, {'PeakDispersionSD'}, {'PeakDispersionCI'}, {'PeakOnset'}, {'PeakOnsetSD'}, {'PeakOnsetCI'}];
rownames = [{'L3I1'}, {'L3I2'}, {'L3I3'}, {'L1I3'}, {'L2I3'}, {'L3i3'}, {'L2I1'}, {'L2I2'}, {'L2i3'}, {'L1I2'}, {'L2i2'}, {'L3i2'}, {'L1I1'}, {'L1i2'}, {'L1i3'}, {'L1i1'}, {'L2i1'}, {'L3i1'}];
summaryVals = table(ampmean', ampstd', ampci', delmean', delstd', cdelci', peramean', perastd', peraci', ttpmean', ttpstd', ttpci', pdpmean', pdpstd', pdpci', ponmean', ponstd', ponci', 'VariableNames', varnames, 'RowNames', rownames);
summaryVals2(1:3,:) = summaryVals(13:15,:);
summaryVals2(4:6,:) = summaryVals(7:9,:);
summaryVals2(7:9,:) = summaryVals(1:3,:);
summaryVals2(10:12,:) = summaryVals(16:18,:);
summaryVals2(13:15,:) = summaryVals(10:12,:);
summaryVals2(16:18,:) = summaryVals(4:6,:);
writetable(summaryVals2, 'APSSummaryVals.csv');

varnamesd = [{'PeakAmpDiff'}, {'PeakAmpDiffSD'}, {'PeakAmpDiffCI'}, {'PeakAmpEffectSz'}, {'PeakDelDiff'}, {'PeakDelDiffSD'}, {'PeakDelDiffCI'}, {'PeakDelEffectSz'}, {'EvPeakDiff'}, {'EvPeakDiffSD'}, {'EvPeakDiffCI'}, {'EvPeakEffectSz'}, {'TimetoEvPeakDiff'}, {'TimetoEvPeakDiffSD'}, {'TimetoEvPeakDiffCI'}, {'TimetoEvPeakEffectSz'}, {'PeakDispDiff'}, {'PeakDispDiffSD'}, {'PeakDispDiffCI'}, {'PeakDispEffectSz'}, {'PeakOnsDiff'}, {'PeakOnsDiffSD'}, {'PeakOnsDiffCI'}, {'PeakOnsEffectSz'}];
rownamesd = [{'L3I3 - L3I2'}, {'L3I2 - L3I1'}, {'L3I3 - L3I1'}, {'L3I3 - L2I3'}, {'L2I3 - L1I3'}, {'L3I3 - L1I3'}, {'L2I3 - L2I2'}, {'L2I2 - L2I1'}, {'L2I3 - L2I1'}, {'L3I2 - L2I2'}, {'L2I2 - L1I2'}, {'L3I2 - L1I2'}, {'L1I3 - L1I2'}, {'L1I2 - L1I1'}, {'L1I3 - L1I1'}, {'L3I1 - L2I1'}, {'L2I1 - L1I1'}, {'L3I1 - L1I1'}];
summaryDiffs = table(ampdiffmean', ampdiffstd', ampdiffci', ampeffect', deldiffmean', deldiffstd', deldiffci', deleffect', peradiffmean', peradiffstd', peradiffci', peraeffect', ttpdiffmean', ttpdiffstd', ttpdiffci', ttpeffect', pdpdiffmean', pdpdiffstd', pdpdiffci', pdpeffect', pondiffmean', pondiffstd', pondiffci', poneffect', 'VariableNames', varnamesd, 'RowNames', rownamesd);
summaryDiffs2(1:3,:) = summaryDiffs(13:15,:);
summaryDiffs2(4:6,:) = summaryDiffs(7:9,:);
summaryDiffs2(7:9,:) = summaryDiffs(1:3,:);
summaryDiffs2(10:12,:) = summaryDiffs(16:18,:);
summaryDiffs2(13:15,:) = summaryDiffs(10:12,:);
summaryDiffs2(16:18,:) = summaryDiffs(4:6,:);
writetable(summaryDiffs2, 'APSSummaryDiffs.csv');

