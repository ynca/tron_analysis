clearvars();
global signal
global covs
cd('/Volumes/Passport/TRoN/TRoN_data');
load('BigKahunaMatrix2');

%% IV (condition) specification
eventtype = 'A';

conddef = {["Length 0.1 Int 0.1" "Length 0.1 Int 0.3" "Length 0.1 Int 0.9"; "Length 0.3 Int 0.1" "Length 0.3 Int 0.3" "Length 0.3 Int 0.9"; "Length 0.9 Int 0.1" "Length 0.9 Int 0.3" "Length 0.9 Int 0.9"];...
    ["Length 0.1 Int 0.01" "Length 0.1 Int 0.1" "Length 0.1 Int 1"; "Length 0.3 Int 0.01" "Length 0.3 Int 0.1" "Length 0.3 Int 1"; "Length 0.9 Int 0.01" "Length 0.9 Int 0.1" "Length 0.9 Int 1"]};

Amatcnt = 1;
Vmatcnt = 1;
%Mmatcnt = 1;
for datacnt = 1:numel(BigKahunaMatrix)
    if BigKahunaMatrix(datacnt).ROI == "A1.txt"
        AData(Amatcnt) = BigKahunaMatrix(datacnt);
        Amatcnt = Amatcnt+1;
    elseif BigKahunaMatrix(datacnt).ROI == "V1.txt"
        VData(Vmatcnt) = BigKahunaMatrix(datacnt);
        Vmatcnt = Vmatcnt+1;
%     elseif BigKahunaMatrix(datacnt).ROI == "M1"
%         MData(Mmatcnt) = BigKahunaMatrix(datacnt);
%         Mmatcnt = Mmatcnt+1;
    end
end

%%
if eventtype == 'V'
    conddefinition = conddef{2};
    eventlengths = [0.1 0.3 0.9];
    eventintensities = [0.01 0.1 1];
    
    for datacnt = 1:numel(VData)
    VName(:,datacnt) = VData(datacnt).SID;
    VOnset(:,datacnt) = VData(datacnt).VSTARTSEC;
    VLength(:,datacnt) = VData(datacnt).VLENGTH;
    VInt(:,datacnt) = VData(datacnt).VINTENS;
    end
    VOnset = floor((VOnset / 0.625) - 4);

    for datacnt = 1:numel(VData)
    for evcnt = 1:size(VOnset(:,datacnt),1)
        VTC(:,evcnt,datacnt) = VData(datacnt).DATA((VOnset(evcnt,datacnt)-10):(VOnset(evcnt,datacnt)+30));
    end
    end

    for datacnt = 1:size(VLength,2)
    for evcnt = 1:size(VLength(:,datacnt),1)
        for Lcon = 1:numel(eventlengths)
            Lsearch{Lcon}(evcnt, datacnt) = eq(VLength(evcnt, datacnt),eventlengths(Lcon));
        end
    end
    for evcnt = 1:size(VInt(:,datacnt),1)
        for Icon = 1:numel(eventintensities)
            Isearch{Icon}(evcnt, datacnt) = eq(VInt(evcnt, datacnt),eventintensities(Icon));
        end
    end
    end

    ProtoMatrix{3, 3} = [];

    for Lcon = 1:numel(eventlengths)
    for Icon = 1:numel(eventintensities)
        ConditionMap{Lcon, Icon} = [eventlengths(Lcon) eventintensities(Icon)];
        for datacnt = 1:size(Lsearch{Lcon},2)
            matcnt = 1;
            for evcnt = 1:size(Lsearch{Lcon}(:,datacnt),1)
                if Lsearch{Lcon}(evcnt, datacnt) == 1 && Isearch{Icon}(evcnt, datacnt) == 1
                    ProtoMatrix{Lcon, Icon}(1:41, matcnt, datacnt) = VTC(1:end, evcnt,datacnt);
                    matcnt = matcnt+1;
                end
            end
        end
    end
    end

elseif eventtype == 'A'
    conddefinition = conddef{1};
    eventlengths = [0.1 0.3 0.9];
    eventintensities = [0.1 0.3 1];

    for datacnt = 1:numel(AData)
    AOnset(:,datacnt) = AData(datacnt).ASTARTSEC;
    ALength(:,datacnt) = AData(datacnt).ALENGTH;
    AInt(:,datacnt) = round(AData(datacnt).AINTENS, 1);
    end
    AOnset = floor((AOnset / 0.625) - 4);

    for datacnt = 1:numel(AData)
    for evcnt = 1:size(AOnset(:,datacnt),1)
        ATC(:,evcnt,datacnt) = AData(datacnt).DATA((AOnset(evcnt,datacnt)-10):(AOnset(evcnt,datacnt)+30));
    end
    end

    for datacnt = 1:size(ALength,2)
    for evcnt = 1:size(ALength(:,datacnt),1)
        for Lcon = 1:numel(eventlengths)
            Lsearch{Lcon}(evcnt, datacnt) = eq(ALength(evcnt, datacnt),eventlengths(Lcon));
        end
    end
    for evcnt = 1:size(AInt(:,datacnt),1)
        for Icon = 1:numel(eventintensities)
            Isearch{Icon}(evcnt, datacnt) = eq(AInt(evcnt, datacnt),eventintensities(Icon));
        end
    end
    end
    
    ProtoMatrix{3, 3} = [];

    for Lcon = 1:numel(eventlengths)
    for Icon = 1:numel(eventintensities)
        ConditionMap{Lcon, Icon} = [eventlengths(Lcon) eventintensities(Icon)];
        for datacnt = 1:size(Lsearch{Lcon},2)
            matcnt = 1;
            for evcnt = 1:size(Lsearch{Lcon}(:,datacnt),1)
                if Lsearch{Lcon}(evcnt, datacnt) == 1 && Isearch{Icon}(evcnt, datacnt) == 1
                    ProtoMatrix{Lcon, Icon}(1:41, matcnt, datacnt) = ATC(1:end, evcnt,datacnt);
                    matcnt = matcnt+1;
                end
            end
        end
    end
    end
    
end

for conC = 1:numel(ProtoMatrix)
    ProtoMatrix{conC} = reshape(ProtoMatrix{conC}, [41 516]);
end

for conC = 1:numel(ProtoMatrix)
    BaselineVector{conC} = ProtoMatrix{conC}(15, :);
    for i = 1:size(ProtoMatrix{conC}, 2)
        ProtoMatrix{conC}(:, i) = ((ProtoMatrix{conC}(:, i) - BaselineVector{conC}(:, i)) / BaselineVector{conC}(:, i)) * 100;
    end
    EventAvg{conC} = nanmean(ProtoMatrix{conC}(11:35,:), 2);
    PeakERA{conC} = max(EventAvg{conC});
    TimeToPeakERA{conC} = find(abs(EventAvg{conC}-PeakERA{conC}) < 0.001) * 0.625;
    TimeToPeakERA{conC} = TimeToPeakERA{conC}(1);
    ErrMargin{conC} = (nanstd(ProtoMatrix{conC}(11:35,:),0,2)/sqrt(size(ProtoMatrix{conC},2)))*1.96;
end

    EventAvg = reshape(EventAvg, [3 3]);
    ErrMargin = reshape(ErrMargin, [3 3]);
    PeakERA = reshape(PeakERA, [3 3]);
    TimeToPeakERA = reshape(TimeToPeakERA, [3 3]);
    
    for lengths = 1:3
        for ints = 1:3
            input = ProtoMatrix{lengths,ints}(11:34,:); % 20s timecourses
            trials = size(ProtoMatrix{lengths,ints}, 2); % total number of events
            totalcourselength = numel(ProtoMatrix{lengths,ints}); % calculate total number of volumes going in
            timecourselength = 15; % length of each timecourse (s)
            numvols = 24; % number of volumes per event
            
            % simulate SPM structs
            SPM.xY.RT = 0.625; % repetition time (s)
            SPM.nscan = trials*24; % total no. of volumes (num of events * num of vols per event)
            SPM.xBF.T = 12; % no. of subdivisions of TR
            SPM.xBF.name = 'hrf';
            SPM.xBF.dt = SPM.xY.RT/SPM.xBF.T; % length of each subdivision (s)
            SPM.xBF.UNITS = 'secs';
            SPM.Sess.U.name = {sprintf('%s', ['vis', '_','l', num2str(lengths), 'i', num2str(ints)])}; % name of condition
            SPM.Sess.U.u = []; % this is required
            SPM.Sess.U.ons = [3:timecourselength:totalcourselength*SPM.xY.RT]; % event onsets in UNITS
            SPM.Sess.U.dur = 0; %eventlengths(lengths); %0; % set 0 for event-related designs (counter-intuitive I know; see section 8.2.1 of SPM manual)
            SPM.Sess.U.orth = 1; % orthogonality of EV
            SPM.Sess.U = spa_get_ons(SPM,1); % builds condition specification from info thus far

            s = length(SPM.Sess);
            Sess = SPM.Sess(s);
            U.dt = Sess.U(1).dt;
            u    = length(Sess.U);
            U.name = SPM.Sess.U.name;
            U.u    = Sess.U(1).u(33:end,1);

            TE    = 0.04;

            Y.y = reshape(input, [trials*numvols, 1]); % converts protomatrix into one long timecourse
            Y.dt = 0.625; %SPM.xY.RT;

            % Model specification: m input; 4 states; 1 output; m + 6 parameters
            %--------------------------------------------------------------------------
            % u(m) - mth stimulus function     (u)
            %
            % x(1) - vascular signal           log(s)
            % x(2) - rCBF                      log(f)
            % x(3) - venous volume             log(v)
            % x(4) - deoyxHb                   log(q)
            %
            % y(1) - BOLD                      (y)
            %
            % P(1)       - signal decay               d(ds/dt)/ds)      half-life = log(2)/P(1) ~ 1sec
            % P(2)       - autoregulation             d(ds/dt)/df)      2*pi*sqrt(1/P(1)) ~ 10 sec
            % P(3)       - transit time               (t0)              ~ 1 sec
            % P(4)       - exponent for Fout(v)       (alpha)           c.f. Grubb's exponent (~ 0.38)
            % P(5)       - resting oxygen extraction  (E0)              ~ range 20 - 50%
            % P(6)       - ratio of intra- to extra-  (epsilon)         ~ range 0.5 - 2
            %              vascular components   
            %              of the gradient echo signal   

            % P(6 + 1:m) - input efficacies - d(ds/dt)/du)  ~0.3 per event
            %--------------------------------------------------------------------------

            % priors (3 modes of hemodynamic variation)
            %--------------------------------------------------------------------------
            m       = size(U.u,2);
            [pE,pC] = spa_hdm_priors(m,3);

            % model
            %--------------------------------------------------------------------------
            M.f     = 'spa_fx_hdm';
            M.g     = 'spa_gx_hdm';
            M.x     = [0 0 0 0]'; 
            M.pE    = pE;    
            M.pC    = pC;
            M.m     = m;
            M.n     = 4;
            M.l     = 1;
            M.N     = 1500; % upscaling of TR for smooth model fits
            M.dt    = timecourselength/M.N; % event lengths (s) / M.N
            M.TE    = TE;

            % nonlinear system identification
            %--------------------------------------------------------------------------
            [Ep,Cp,Ce,K0,K1,K2,M0,M1,L1,L2,F] = spa_nlsi(M,U,Y);

            t       = [1:M.N]*M.dt;
            Fhdm    = spm_figure;
            set(Fhdm,'name','Hemodynamic Modeling')

            % display input parameters
            %--------------------------------------------------------------------------
            subplot(2,2,1)
            P     = Ep(7:end);
            C     = diag(Cp(7:end,7:end));
            [i,j] = max(abs(P));
            spm_barh(P,C)
            axis square
            title({'stimulus efficacy'; 'with 90% confidence intervals'},'FontSize',10)
            set(gca,'Ytick',[1:m],'YTickLabel',U.name,'FontSize',8)
            str = {};
            for i = 1:m
                str{end + 1} = U.name{i};
                str{end + 1} = sprintf('mean = %0.2f',P(i));
                str{end + 1} = '';
            end
            set(gca,'Ytick',[1:m*3]/3 + 1/2,'YTickLabel',str)
            xlabel('relative efficacy per event/sec')

            % display hemodynamic parameters
            %--------------------------------------------------------------------------
            subplot(2,2,3)
            P     = Ep(1:6);
            pE    = pE(1:6);
            C     = diag(Cp(1:6,1:6));
            spm_barh(P,C,pE)
            title({ 'hemodynamic parameters'},'FontSize',10)
            set(gca,'Ytick',[1:18]/3 + 1/2)
            set(gca,'YTickLabel',{  'SIGNAL decay',...
                        sprintf('%0.2f per sec',P(1)),'',...
                        'FEEDBACK',...
                        sprintf('%0.2f per sec',P(2)),'',...
                        'TRANSIT TIME',...
                        sprintf('%0.2f seconds',P(3)),'',...
                        'EXPONENT',...
                        sprintf('%0.2f',P(4)),'',...
                        'EXTRACTION',...
                        sprintf('%0.0f %s',P(5)*100,'%'),'',...
                        'log SIGNAL RATIO',...
                        sprintf('%0.2f %s',P(6),'%'),''},'FontSize',8)


            % get display state kernels (i.e. state dynamics) 
            %==========================================================================

            % Volterra kernels of states
            %--------------------------------------------------------------------------
            [H0,H1] = spa_kernels(M0,M1,M.N,M.dt);

            subplot(3,2,2)
            plot(t,exp(H1(:,:,j)))
            axis square
            title({['1st order kernels for ' U.name{j}];...
                'state variables'},'FontSize',9)
            ylabel('normalized values')
            legend({'s','f','v','q'}, 'Location','Best');
            grid on


            % display output kernels (i.e. BOLD response) 
            %--------------------------------------------------------------------------
            subplot(3,2,4)
            plot(t,K1(:,:,j))
            axis square
            title({'1st order kernel';...
                'output: BOLD'},'FontSize',9)
            ylabel('normalized flow signal')
            grid on

            subplot(3,2,6)
            imagesc(t,t,K2(:,:,1,j,j))
            axis square
            title({'2nd order kernel';...
                'output: BOLD'},'FontSize',9)
            xlabel({'time \{seconds\} for'; U.name{j}})
            grid on
            %%
            H2 = exp(H1);
            Hsave{lengths,ints}(:,:) = H2;
            Hmean{lengths,ints} = nanmean(Hsave{lengths,ints},3);
            FPcovariance{lengths,ints}(1:numel(covs)) = covs;
            p = 0.4;
            v0 = 0.02;
            nu0   = 40.3;
            r0 = 25;
            E0 = P(5);
            epsi = exp(P(6));
            k1 = 6.7; %4.3.*nu0.*E0.*TE;
            k2 = 2.73; %epsi.*r0.*E0.*TE; 
            k3    = 1-epsi;
            modelvoltkernel1{lengths,ints} = 100/p*v0*(k1.*(1-Hmean{lengths,ints}(:,4))+k2.*(1-Hmean{lengths,ints}(:,4)./Hmean{lengths,ints}(:,3)+k3.*(1-Hmean{lengths,ints}(:,3))));
            modelvoltkernel2{lengths,ints} = K1;
            fullEMPTC{lengths,ints}(1:numel(signal)) = signal;
            close
        end
    end

%%
for lengths = 1:3
    for ints = 1:3
        
        Volkernel1{lengths, ints} = nanmean(modelvoltkernel1{lengths, ints},2);
        Volkernel2{lengths, ints} = nanmean(modelvoltkernel2{lengths, ints},2);
        fullEMPTC{lengths,ints} = reshape(fullEMPTC{lengths,ints}, 48, numel(fullEMPTC{lengths,ints})/48);
        EMPredict{lengths,ints} = nanmean(fullEMPTC{lengths, ints},2);
        EMPredictSeg{lengths,ints} = EMPredict{lengths,ints}(2:26); %(51:74);
        EMPredictSmooth{lengths,ints}.hrf = resample(EMPredictSeg{lengths,ints}, 1000,1);
        [EMPredictSmooth{lengths,ints}.MaxValue, EMPredictSmooth{lengths,ints}.MaxIndex] = max(EMPredictSmooth{lengths,ints}.hrf);
        EMPredictSmooth{lengths,ints}.MaxIndexInSeconds = EMPredictSmooth{lengths,ints}.MaxIndex / 1000 * 0.625;
        EMPredictSmooth{lengths, ints}.HalfMax = (EMPredictSmooth{lengths,ints}.MaxValue-0)/2 + 0;
        EMPredictSmooth{lengths,ints}.OnsetMax = (EMPredictSmooth{lengths,ints}.MaxValue-0)/10 + 0;
        [EMPredictSmooth{lengths,ints}.MinValue, EMPredictSmooth{lengths,ints}.MinIndex] = min(EMPredictSmooth{lengths,ints}.hrf);

        for i = EMPredictSmooth{lengths,ints}.MaxIndex:-1:1
            EMPredictSmooth{lengths,ints}.HMPoint1 = i;
            if EMPredictSmooth{lengths,ints}.hrf(i) < EMPredictSmooth{lengths,ints}.HalfMax
                break
            end
        end

        for i = EMPredictSmooth{lengths,ints}.MaxIndex:size(EMPredictSmooth{lengths,ints}.hrf,1)
            EMPredictSmooth{lengths,ints}.HMPoint2 = i;
            if EMPredictSmooth{lengths,ints}.hrf(i) < EMPredictSmooth{lengths,ints}.HalfMax
                break
            end
        end

        for i = EMPredictSmooth{lengths,ints}.MaxIndex:-1:1
            EMPredictSmooth{lengths,ints}.RisePoint = i;
            if EMPredictSmooth{lengths,ints}.hrf(i) < EMPredictSmooth{lengths,ints}.OnsetMax
                break
            end
        end

        EMPredictSmooth{lengths,ints}.FWHM = EMPredictSmooth{lengths,ints}.HMPoint2 - EMPredictSmooth{lengths,ints}.HMPoint1;
        EMPredictSmooth{lengths,ints}.OnsetInSeconds = EMPredictSmooth{lengths,ints}.RisePoint / 1000 * 0.625;
        EMPredictSmooth{lengths,ints}.DispersionInSeconds = EMPredictSmooth{lengths,ints}.FWHM / 1000 * 0.625;
        
    end
end
%%
linecolor = ['r' 'g' 'b'];
k = 1;

for lengths = 1:3
    figure;
    for ints = 1:size(ProtoMatrix,2)
        plot(0:0.625:15, nanmean(EventAvg{lengths, ints},2), ':', 'Color', linecolor(ints), 'LineWidth', 1, 'DisplayName', sprintf('%s', ['EventAvg-', conddefinition(lengths, ints)]));
        hold on;
        plot(0:0.625/1000:(25000-1)*0.625/1000, EMPredictSmooth{lengths,ints}.hrf(1:end), linecolor(ints), 'LineWidth', 2, 'DisplayName', sprintf('%s', ['Modelled ', 'max: ', num2str(EMPredictSmooth{lengths,ints}.MaxValue), ' time: ', num2str(EMPredictSmooth{lengths,ints}.MaxIndexInSeconds)]));
        plot(0:0.625:15, EMPredictSeg{lengths,ints}(1:25), '--', 'Color', linecolor(ints), 'LineWidth', 1, 'DisplayName', 'EM Prediction');
        hold on;
        legend;
    
    end
    plotTitle = sprintf('%s', ["Comparison of Balloon Function Fits for ", eventtype, "1 ", "by stimulus intensity for duration ", eventlengths(k)]);
    xlabel('Time from event onset (secs)', 'fontsize', 14);
    ylabel('Response', 'fontsize', 14);
    ylim([-1 2]);
    xlim([0 15]);
    set(gca, 'fontsize', 14);
    title([plotTitle],'fontsize',14); % 
    
    hold off;
    k = k+1;
end
%%
k = 1;
for ints = 1:3
    figure;
    for lengths = 1:size(ProtoMatrix,1)
        plot(0:0.625:15, nanmean(EventAvg{lengths, ints},2), ':', 'Color', linecolor(lengths), 'LineWidth', 1, 'DisplayName', sprintf('%s', ['EventAvg-', conddefinition(lengths, ints)]));
        hold on;
        plot(0:0.625/1000:(25000-1)*0.625/1000, EMPredictSmooth{lengths,ints}.hrf, linecolor(lengths), 'LineWidth', 2, 'DisplayName', sprintf('%s', ['Modelled ', 'max: ', num2str(EMPredictSmooth{lengths,ints}.MaxValue), ' time: ', num2str(EMPredictSmooth{lengths,ints}.MaxIndexInSeconds)]));
        plot(0:0.625:15, EMPredictSeg{lengths,ints}(1:25), '--', 'Color', linecolor(lengths), 'LineWidth', 1, 'DisplayName', 'EM Prediction');
        hold on;
        legend;
    end
    plotTitle = sprintf('%s', ["Comparison of Balloon Function Fits for ", eventtype, "1 ", "by stimulus duration for intensity ", eventintensities(k)]);
    xlabel('Time from event onset (secs)', 'fontsize', 14);
    ylabel('Response', 'fontsize', 14); 
    ylim([-1 2]);
    xlim([0 15]);
    set(gca, 'fontsize', 14);
    title([plotTitle],'fontsize',14); 
    hold off;
    k = k+1;
end