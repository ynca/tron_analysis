clearvars();

% Per-subject ROI timecourse event-related averaging, linear gamma function
% fitting and point estimate computation

% Written Sep 2018 by Alvin Wong for TRoN Project

cd('/Volumes/Passport/TRoN/Analysis/PSubj2/Data'); % Path to data matrices output by PerSubjProc
eventtype = "A"; % Set ROI type (A or V)
filedir = dir('A1*.mat'); % Set filename search criteria

for fileidx = 1:numel(filedir)
    clearvars('-except', 'eventtype', 'filedir', 'customvolsperevent', 'volsperevent', 'fileidx', 'AllStats');
    filename = sprintf('%s', [filedir(fileidx).folder, '/', filedir(fileidx).name]);
    load(filename);
    subjname = [filedir(fileidx).name(4:10)]; % Data import and subject name definition

    conddef = {["Length 0.1 Int 0.1" "Length 0.1 Int 0.3" "Length 0.1 Int 0.9";... 
                "Length 0.3 Int 0.1" "Length 0.3 Int 0.3" "Length 0.3 Int 0.9";... 
                "Length 0.9 Int 0.1" "Length 0.9 Int 0.3" "Length 0.9 Int 0.9"];...
                ["Length 0.1 Int 0.01" "Length 0.1 Int 0.1" "Length 0.1 Int 1";... 
                "Length 0.3 Int 0.01" "Length 0.3 Int 0.1" "Length 0.3 Int 1";...
                "Length 0.9 Int 0.01" "Length 0.9 Int 0.1" "Length 0.9 Int 1"]};
                
    if eventtype == 'V' % V1 timecourse processing to build protomatrix
        conddefinition = conddef{2};
        eventlengths = [0.1 0.3 0.9];
        eventintensities = [0.01 0.1 1];
        roiname = 'V1';
        
        for datacnt = 1:numel(CurrentV)
        VOnset(:, datacnt) = CurrentV(datacnt).VisualEvs(1:end);
        VLength(:,datacnt) = CurrentV(datacnt).VisualLengths;
        VInt(:,datacnt) = CurrentV(datacnt).VisualInts;
        end
        
        for datacnt = 1:numel(CurrentV)
        for evcnt = 1:size(VOnset(:,datacnt),1)
            VTC(:,evcnt,datacnt) = CurrentV(datacnt).RawData((VOnset(evcnt,datacnt)-10):(VOnset(evcnt,datacnt)+30));
        end
        end

        for datacnt = 1:size(VLength,2)
        for evcnt = 1:size(VLength(:,datacnt),1)
            for Lcon = 1:numel(eventlengths)
                Lsearch{Lcon}(evcnt, datacnt) = eq(VLength(evcnt, datacnt),eventlengths(Lcon));
            end
        end
        for evcnt = 1:size(VInt(:,datacnt),1)
            for Icon = 1:numel(eventintensities)
                Isearch{Icon}(evcnt, datacnt) = eq(VInt(evcnt, datacnt),eventintensities(Icon));
            end
        end
        end

        ProtoMatrix{3, 3} = [];

        for Lcon = 1:numel(eventlengths)
        for Icon = 1:numel(eventintensities)
            ConditionMap{Lcon, Icon} = [eventlengths(Lcon) eventintensities(Icon)];
            for datacnt = 1:size(Lsearch{Lcon},2)
                matcnt = 1;
                for evcnt = 1:size(Lsearch{Lcon}(:,datacnt),1)
                    if Lsearch{Lcon}(evcnt, datacnt) == 1 && Isearch{Icon}(evcnt, datacnt) == 1
                        ProtoMatrix{Lcon, Icon}(1:41, matcnt, datacnt) = VTC(1:end, evcnt,datacnt);
                        matcnt = matcnt+1;
                    end
                end
            end
        end
        end

    elseif eventtype == 'A' % A1 timecourse processing to build protomatrix
        conddefinition = conddef{1};
        eventlengths = [0.1 0.3 0.9];
        eventintensities = [0.1 0.3 1];
        roiname = 'A1';
        
        for datacnt = 1:numel(CurrentA)
        AOnset(:, datacnt) = CurrentA(datacnt).AuditoryEvs(1:end);
        ALength(:,datacnt) = CurrentA(datacnt).AuditoryLengths;
        AInt(:,datacnt) = CurrentA(datacnt).AuditoryInts;
        end
        
        for datacnt = 1:numel(CurrentA)
        for evcnt = 1:size(AOnset(:,datacnt),1)
            ATC(:,evcnt,datacnt) = CurrentA(datacnt).RawData((AOnset(evcnt,datacnt)-10):(AOnset(evcnt,datacnt)+30));
        end
        end

        for datacnt = 1:size(ALength,2)
        for evcnt = 1:size(ALength(:,datacnt),1)
            for Lcon = 1:numel(eventlengths)
                Lsearch{Lcon}(evcnt, datacnt) = eq(ALength(evcnt, datacnt),eventlengths(Lcon));
            end
        end
        for evcnt = 1:size(AInt(:,datacnt),1)
            for Icon = 1:numel(eventintensities)
                Isearch{Icon}(evcnt, datacnt) = eq(AInt(evcnt, datacnt),eventintensities(Icon));
            end
        end
        end

        ProtoMatrix{3, 3} = [];

        for Lcon = 1:numel(eventlengths)
        for Icon = 1:numel(eventintensities)
            ConditionMap{Lcon, Icon} = [eventlengths(Lcon) eventintensities(Icon)];
            for datacnt = 1:size(Lsearch{Lcon},2)
                matcnt = 1;
                for evcnt = 1:size(Lsearch{Lcon}(:,datacnt),1)
                    if Lsearch{Lcon}(evcnt, datacnt) == 1 && Isearch{Icon}(evcnt, datacnt) == 1
                        ProtoMatrix{Lcon, Icon}(1:41, matcnt, datacnt) = ATC(1:end, evcnt,datacnt);
                        matcnt = matcnt+1;
                    end
                end
            end
        end
        end

    end
    
    for conC = 1:numel(ProtoMatrix) % convert 3D cells in protomatrix to 2D cells
        ProtoMatrix{conC} = reshape(ProtoMatrix{conC}, [41 size(ProtoMatrix{conC},2)*size(ProtoMatrix{conC},3)]);
    end
    
    for conC = 1:numel(ProtoMatrix) % baseline computation
        BaselineVector{conC} = ProtoMatrix{conC}(14, :);
        for i = 1:size(ProtoMatrix{conC}, 2)
            ProtoMatrix{conC}(:, i) = ((ProtoMatrix{conC}(:, i) - BaselineVector{conC}(1,i)) / BaselineVector{conC}(1,i)) * 100;
        end
        EventAvg{conC} = nanmean(ProtoMatrix{conC}(11:35,:), 2);
        PeakERA{conC} = max(EventAvg{conC});
        TimeToPeakERA{conC} = find(abs(EventAvg{conC}-PeakERA{conC}) < 0.001) * 0.625;
        TimeToPeakERA{conC} = TimeToPeakERA{conC}(1);
        ErrMargin{conC} = (nanstd(ProtoMatrix{conC}(11:35,:),0,2)/sqrt(size(ProtoMatrix{conC},2)))*1.96;
    end

        EventAvg = reshape(EventAvg, [3 3]);
        ErrMargin = reshape(ErrMargin, [3 3]);
        PeakERA = reshape(PeakERA, [3 3]);
        TimeToPeakERA = reshape(TimeToPeakERA, [3 3]);
    
    cd('/Volumes/Passport/TRoN/Analysis/PSubj2/out'); % save all event averages to MATs
    save(sprintf('%s', [subjname, '_', roiname, '_ERAvgs']), 'ProtoMatrix', 'EventAvg', 'ErrMargin', 'PeakERA', 'TimeToPeakERA');
    
    % Per-condition linear gamma function fitting for each subject
    for lengths = 1:size(ProtoMatrix,1)
        for ints = 1:size(ProtoMatrix,2)
            [FittingMatrix{lengths, ints}] = GammaFit(ProtoMatrix{lengths, ints}(11:35,:)', 0.625, 15);
            DelayMatrix{lengths, ints} = FittingMatrix{lengths, ints}.delay;
            AmplitudeMatrix{lengths, ints} = FittingMatrix{lengths, ints}.amplitude;
            DispersionMatrix{lengths, ints} = FittingMatrix{lengths, ints}.dispersion;
            OnsetMatrix{lengths, ints} = FittingMatrix{lengths, ints}.onset;
            PeakTimeMatrix{lengths, ints} = FittingMatrix{lengths, ints}.MaxIndexInSeconds; % Best peak delay estimate
            PeakAmplitudeMatrix{lengths, ints} = FittingMatrix{lengths, ints}.MaxValue; % Best peak amplitude estimate
            FWHMMatrix{lengths, ints} = FittingMatrix{lengths, ints}.FWHMInSeconds; % Best dispersion estimate
            PeakOnsetMatrix{lengths, ints} = FittingMatrix{lengths, ints}.OnsetTimeInSeconds; % Best peak onset estimate
            
            hold on;
            
%             funcplot{lengths, ints} = plot(0:0.625:0.625*(customvolsperevent-10), nanmean(ProtoMatrix{lengths, ints}(10:customvolsperevent, :), 2), 'r', 'LineWidth', 2);
%             ylim([-3 3]); % y-axis range
%             xlabel('Time from event onset (secs)', 'fontsize', 14);
%             ylabel('Percent signal change', 'fontsize', 14);
%             set(gca, 'fontsize', 14);
%             hold on;
%             savefig(sprintf('%s', [eventtype, '_', subjname, '_SingleGammaFit_', 'l', lengths, '_i', ints, '.fig']));
            
        end
    end
    save(sprintf('%s', [eventtype, '_', subjname, '_FittingMatrix', '.mat']),'FittingMatrix');
%%
% linecolor = ['r' 'g' 'b'];
% k = 1;
%     for lengths = 1:3
%         for ints = 1:size(ProtoMatrix,2)
% 
%             plot(0:0.625:15, nanmean(ProtoMatrix{lengths, ints}(11:35, :),2), linecolor(ints), 'LineWidth', 1, 'DisplayName', sprintf('%s', conddefinition(lengths, ints)));
%             hold on;
%             TR = 0.625/1000;
%             eventlength = 15*1000;
%             
%             t = 0:(1/1000):15-(0.625/1000);
% 
%             plot(t,FittingMatrix{lengths, ints}.hrf(1,:), linecolor(ints), 'LineWidth', 2);
%             hold on;
%             plot(t,FittingMatrix{lengths, ints}.hrf(1,:), linecolor(ints), 'LineWidth', 2);
%             plot(t,FittingMatrix{lengths, ints}.hrf(1,:), linecolor(ints), 'LineWidth', 2);
%             hold on;
%         end
% 
%         plotTitle = sprintf('%s', ["Comparison of Gamma Function Fits for ", eventtype, "1 ", "by stimulus intensity for duration ", eventlengths(lengths)]);
%         xlabel('Time from event onset (secs)', 'fontsize', 14); % x-axis label
%         ylabel('Response', 'fontsize', 14); % y-axis label
%         ylim([-1 2]);
%         set(gca, 'fontsize', 14);
%         title([plotTitle],'fontsize',14); % plot title
% 
%         %legend(timecoursePlot(lengths, 1:3), [sprintf('%s', conddefinition(lengths, 1)), '; a: ' num2str(PeakAmplitudeMatrix{lengths, 1}) ', d: ' num2str(PeakTimeMatrix{lengths, 1})],...
%         [sprintf('%s', conddefinition(lengths, 2)), '; a: ' num2str(PeakAmplitudeMatrix{lengths, 2}) ', d: ' num2str(PeakTimeMatrix{lengths, 2})],...
%         [sprintf('%s', conddefinition(lengths, 3)), '; a: ' num2str(PeakAmplitudeMatrix{lengths, 3}) ', d: ' num2str(PeakTimeMatrix{lengths, 3})]);;
%         savefig(sprintf('%s', [eventtype, '_', subjname, '_CompGammaFitLengths_', 'l', lengths, '_i', ints, '.fig']));
%         hold off;
%         k = k+1;
%     end
% 
% k = 1;
%     for ints = 1:3
%         for lengths = 1:size(ProtoMatrix,1)
% 
%             plot(0:0.625:15, nanmean(ProtoMatrix{lengths, ints}(11:35, :),2), linecolor(lengths), 'LineWidth', 1, 'DisplayName', sprintf('%s', conddefinition(lengths, ints)));
%             hold on;
%             TR = 0.625/1000;
%             eventlength = 15*1000;
%             t = 0:TR*1000:(eventlength);
% 
%             plot(t,FittingMatrix{lengths, ints}.hrf(1,:), linecolor(lengths), 'LineWidth', 2);
%             hold on;
%             plot(t,FittingMatrix{lengths, ints}.hrf(1,:), linecolor(lengths), 'LineWidth', 2);
%             plot(t,FittingMatrix{lengths, ints}.hrf(1,:), linecolor(lengths), 'LineWidth', 2);
%             hold on;
%         end
% 
%         plotTitle = sprintf('%s', ["Comparison of Gamma Function Fits for ", eventtype, "1 ", "by stimulus duration for intensity ", eventlengths(ints)]);
%         xlabel('Time from event onset (secs)', 'fontsize', 14); % x-axis label
%         ylabel('Response', 'fontsize', 14); % y-axis label
%         ylim([-1 2]);
%         set(gca, 'fontsize', 14);
%         title([plotTitle],'fontsize',14); % plot title
% 
%         legend(timecoursePlot(1:3, ints), [sprintf('%s', conddefinition(1, ints)), '; a: ' num2str(PeakAmplitudeMatrix{1, ints}) ', d: ' num2str(PeakTimeMatrix{1, ints})],...
%         [sprintf('%s', conddefinition(2, ints)), '; a: ' num2str(PeakAmplitudeMatrix{2, ints}) ', d: ' num2str(PeakTimeMatrix{2, ints})],...
%         [sprintf('%s', conddefinition(3, ints)), '; a: ' num2str(PeakAmplitudeMatrix{3, ints}) ', d: ' num2str(PeakTimeMatrix{3, ints})]);
%         savefig(sprintf('%s', [eventtype, '_', subjname, '_CompGammaFitInts_', 'l', lengths, '_i', ints, '.fig']));
%         hold off;
%         k = k+1;
%     end
    
    AllStats{fileidx,1} = [{PeakTimeMatrix}, {PeakAmplitudeMatrix}, {PeakERA}, {TimeToPeakERA}, {FWHMMatrix}, {PeakOnsetMatrix}];
    
end
%%
% Point estimate, confidence interval and effect size computation for each
% parameter
for idx = 1:numel(filedir)
    
% Analysis for I1 to I3 at L1

PeakAmplitude(1, idx) = cell2mat(AllStats{idx}{1,2}(1,1));
PeakAmplitude(2, idx) = cell2mat(AllStats{idx}{1,2}(1,2));
PeakAmplitude(3, idx) = cell2mat(AllStats{idx}{1,2}(1,3));
PeakAmpDiff(1, idx) = (cell2mat(AllStats{idx}{1,2}(1,3)) - cell2mat(AllStats{idx}{1,2}(1,2)));
PeakAmpDiff(2, idx) = (cell2mat(AllStats{idx}{1,2}(1,2)) - cell2mat(AllStats{idx}{1,2}(1,1)));
PeakAmpDiff(3, idx) = (cell2mat(AllStats{idx}{1,2}(1,3)) - cell2mat(AllStats{idx}{1,2}(1,1)));

PeakDelay(1, idx) = cell2mat(AllStats{idx}{1,1}(1,1));
PeakDelay(2, idx) = cell2mat(AllStats{idx}{1,1}(1,2));
PeakDelay(3, idx) = cell2mat(AllStats{idx}{1,1}(1,3));
PeakDelDiff(1, idx) = (cell2mat(AllStats{idx}{1,1}(1,3)) - cell2mat(AllStats{idx}{1,1}(1,2)));
PeakDelDiff(2, idx) = (cell2mat(AllStats{idx}{1,1}(1,2)) - cell2mat(AllStats{idx}{1,1}(1,1)));
PeakDelDiff(3, idx) = (cell2mat(AllStats{idx}{1,1}(1,3)) - cell2mat(AllStats{idx}{1,1}(1,1)));

PeakEra(1, idx) = cell2mat(AllStats{idx}{1,3}(1,1));
PeakEra(2, idx) = cell2mat(AllStats{idx}{1,3}(1,2));
PeakEra(3, idx) = cell2mat(AllStats{idx}{1,3}(1,3));
PeakEraDiff(1, idx) = (cell2mat(AllStats{idx}{1,3}(1,3)) - cell2mat(AllStats{idx}{1,3}(1,2)));
PeakEraDiff(2, idx) = (cell2mat(AllStats{idx}{1,3}(1,2)) - cell2mat(AllStats{idx}{1,3}(1,1)));
PeakEraDiff(3, idx) = (cell2mat(AllStats{idx}{1,3}(1,3)) - cell2mat(AllStats{idx}{1,3}(1,1)));

TTPEra(1, idx) = cell2mat(AllStats{idx}{1,4}(1,1));
TTPEra(2, idx) = cell2mat(AllStats{idx}{1,4}(1,2));
TTPEra(3, idx) = cell2mat(AllStats{idx}{1,4}(1,3));
TTPEraDiff(1, idx) = (cell2mat(AllStats{idx}{1,4}(1,3)) - cell2mat(AllStats{idx}{1,4}(1,2)));
TTPEraDiff(2, idx) = (cell2mat(AllStats{idx}{1,4}(1,2)) - cell2mat(AllStats{idx}{1,4}(1,1)));
TTPEraDiff(3, idx) = (cell2mat(AllStats{idx}{1,4}(1,3)) - cell2mat(AllStats{idx}{1,4}(1,1)));

PeakDispersion(1, idx) = cell2mat(AllStats{idx}{1,5}(1,1));
PeakDispersion(2, idx) = cell2mat(AllStats{idx}{1,5}(1,2));
PeakDispersion(3, idx) = cell2mat(AllStats{idx}{1,5}(1,3));
PeakDispDiff(1, idx) = (cell2mat(AllStats{idx}{1,5}(1,3)) - cell2mat(AllStats{idx}{1,5}(1,2)));
PeakDispDiff(2, idx) = (cell2mat(AllStats{idx}{1,5}(1,2)) - cell2mat(AllStats{idx}{1,5}(1,1)));
PeakDispDiff(3, idx) = (cell2mat(AllStats{idx}{1,5}(1,3)) - cell2mat(AllStats{idx}{1,5}(1,1)));

PeakOnset(1, idx) = cell2mat(AllStats{idx}{1,6}(1,1));
PeakOnset(2, idx) = cell2mat(AllStats{idx}{1,6}(1,2));
PeakOnset(3, idx) = cell2mat(AllStats{idx}{1,6}(1,3));
PeakOnsDiff(1, idx) = (cell2mat(AllStats{idx}{1,6}(1,3)) - cell2mat(AllStats{idx}{1,6}(1,2)));
PeakOnsDiff(2, idx) = (cell2mat(AllStats{idx}{1,6}(1,2)) - cell2mat(AllStats{idx}{1,6}(1,1)));
PeakOnsDiff(3, idx) = (cell2mat(AllStats{idx}{1,6}(1,3)) - cell2mat(AllStats{idx}{1,6}(1,1)));

% Analysis for I1 to I3 at L2
PeakAmplitude(4, idx) = cell2mat(AllStats{idx}{1,2}(2,1));
PeakAmplitude(5, idx) = cell2mat(AllStats{idx}{1,2}(2,2));
PeakAmplitude(6, idx) = cell2mat(AllStats{idx}{1,2}(2,3));
PeakAmpDiff(4, idx) = (cell2mat(AllStats{idx}{1,2}(2,3)) - cell2mat(AllStats{idx}{1,2}(2,2)));
PeakAmpDiff(5, idx) = (cell2mat(AllStats{idx}{1,2}(2,2)) - cell2mat(AllStats{idx}{1,2}(2,1)));
PeakAmpDiff(6, idx) = (cell2mat(AllStats{idx}{1,2}(2,3)) - cell2mat(AllStats{idx}{1,2}(2,1)));

PeakDelay(4, idx) = cell2mat(AllStats{idx}{1,1}(2,1));
PeakDelay(5, idx) = cell2mat(AllStats{idx}{1,1}(2,2));
PeakDelay(6, idx) = cell2mat(AllStats{idx}{1,1}(2,3));
PeakDelDiff(4, idx) = (cell2mat(AllStats{idx}{1,1}(2,3)) - cell2mat(AllStats{idx}{1,1}(2,2)));
PeakDelDiff(5, idx) = (cell2mat(AllStats{idx}{1,1}(2,2)) - cell2mat(AllStats{idx}{1,1}(2,1)));
PeakDelDiff(6, idx) = (cell2mat(AllStats{idx}{1,1}(2,3)) - cell2mat(AllStats{idx}{1,1}(2,1)));

PeakEra(4, idx) = cell2mat(AllStats{idx}{1,3}(2,1));
PeakEra(5, idx) = cell2mat(AllStats{idx}{1,3}(2,2));
PeakEra(6, idx) = cell2mat(AllStats{idx}{1,3}(2,3));
PeakEraDiff(4, idx) = (cell2mat(AllStats{idx}{1,3}(2,3)) - cell2mat(AllStats{idx}{1,3}(2,2)));
PeakEraDiff(5, idx) = (cell2mat(AllStats{idx}{1,3}(2,2)) - cell2mat(AllStats{idx}{1,3}(2,1)));
PeakEraDiff(6, idx) = (cell2mat(AllStats{idx}{1,3}(2,3)) - cell2mat(AllStats{idx}{1,3}(2,1)));

TTPEra(4, idx) = cell2mat(AllStats{idx}{1,4}(2,1));
TTPEra(5, idx) = cell2mat(AllStats{idx}{1,4}(2,2));
TTPEra(6, idx) = cell2mat(AllStats{idx}{1,4}(2,3));
TTPEraDiff(4, idx) = (cell2mat(AllStats{idx}{1,4}(2,3)) - cell2mat(AllStats{idx}{1,4}(2,2)));
TTPEraDiff(5, idx) = (cell2mat(AllStats{idx}{1,4}(2,2)) - cell2mat(AllStats{idx}{1,4}(2,1)));
TTPEraDiff(6, idx) = (cell2mat(AllStats{idx}{1,4}(2,3)) - cell2mat(AllStats{idx}{1,4}(2,1)));

PeakDispersion(4, idx) = cell2mat(AllStats{idx}{1,5}(2,1));
PeakDispersion(5, idx) = cell2mat(AllStats{idx}{1,5}(2,2));
PeakDispersion(6, idx) = cell2mat(AllStats{idx}{1,5}(2,3));
PeakDispDiff(4, idx) = (cell2mat(AllStats{idx}{1,5}(2,3)) - cell2mat(AllStats{idx}{1,5}(2,2)));
PeakDispDiff(5, idx) = (cell2mat(AllStats{idx}{1,5}(2,2)) - cell2mat(AllStats{idx}{1,5}(2,1)));
PeakDispDiff(6, idx) = (cell2mat(AllStats{idx}{1,5}(2,3)) - cell2mat(AllStats{idx}{1,5}(2,1)));

PeakOnset(4, idx) = cell2mat(AllStats{idx}{1,6}(2,1));
PeakOnset(5, idx) = cell2mat(AllStats{idx}{1,6}(2,2));
PeakOnset(6, idx) = cell2mat(AllStats{idx}{1,6}(2,3));
PeakOnsDiff(4, idx) = (cell2mat(AllStats{idx}{1,6}(2,3)) - cell2mat(AllStats{idx}{1,6}(2,2)));
PeakOnsDiff(5, idx) = (cell2mat(AllStats{idx}{1,6}(2,2)) - cell2mat(AllStats{idx}{1,6}(2,1)));
PeakOnsDiff(6, idx) = (cell2mat(AllStats{idx}{1,6}(2,3)) - cell2mat(AllStats{idx}{1,6}(2,1)));

% Analysis for I1 to I3 at L3
PeakAmplitude(7, idx) = cell2mat(AllStats{idx}{1,2}(3,1));
PeakAmplitude(8, idx) = cell2mat(AllStats{idx}{1,2}(3,2));
PeakAmplitude(9, idx) = cell2mat(AllStats{idx}{1,2}(3,3));
PeakAmpDiff(7, idx) = (cell2mat(AllStats{idx}{1,2}(3,3)) - cell2mat(AllStats{idx}{1,2}(3,2)));
PeakAmpDiff(8, idx) = (cell2mat(AllStats{idx}{1,2}(3,2)) - cell2mat(AllStats{idx}{1,2}(3,1)));
PeakAmpDiff(9, idx) = (cell2mat(AllStats{idx}{1,2}(3,3)) - cell2mat(AllStats{idx}{1,2}(3,1)));

PeakDelay(7, idx) = cell2mat(AllStats{idx}{1,1}(3,1));
PeakDelay(8, idx) = cell2mat(AllStats{idx}{1,1}(3,2));
PeakDelay(9, idx) = cell2mat(AllStats{idx}{1,1}(3,3));
PeakDelDiff(7, idx) = (cell2mat(AllStats{idx}{1,1}(3,3)) - cell2mat(AllStats{idx}{1,1}(3,2)));
PeakDelDiff(8, idx) = (cell2mat(AllStats{idx}{1,1}(3,2)) - cell2mat(AllStats{idx}{1,1}(3,1)));
PeakDelDiff(9, idx) = (cell2mat(AllStats{idx}{1,1}(3,3)) - cell2mat(AllStats{idx}{1,1}(3,1)));

PeakEra(7, idx) = cell2mat(AllStats{idx}{1,3}(3,1));
PeakEra(8, idx) = cell2mat(AllStats{idx}{1,3}(3,2));
PeakEra(9, idx) = cell2mat(AllStats{idx}{1,3}(3,3));
PeakEraDiff(7, idx) = (cell2mat(AllStats{idx}{1,3}(3,3)) - cell2mat(AllStats{idx}{1,3}(3,2)));
PeakEraDiff(8, idx) = (cell2mat(AllStats{idx}{1,3}(3,2)) - cell2mat(AllStats{idx}{1,3}(3,1)));
PeakEraDiff(9, idx) = (cell2mat(AllStats{idx}{1,3}(3,3)) - cell2mat(AllStats{idx}{1,3}(3,1)));

TTPEra(7, idx) = cell2mat(AllStats{idx}{1,4}(3,1));
TTPEra(8, idx) = cell2mat(AllStats{idx}{1,4}(3,2));
TTPEra(9, idx) = cell2mat(AllStats{idx}{1,4}(3,3));
TTPEraDiff(7, idx) = (cell2mat(AllStats{idx}{1,4}(3,3)) - cell2mat(AllStats{idx}{1,4}(3,2)));
TTPEraDiff(8, idx) = (cell2mat(AllStats{idx}{1,4}(3,2)) - cell2mat(AllStats{idx}{1,4}(3,1)));
TTPEraDiff(9, idx) = (cell2mat(AllStats{idx}{1,4}(3,3)) - cell2mat(AllStats{idx}{1,4}(3,1)));

PeakDispersion(7, idx) = cell2mat(AllStats{idx}{1,5}(3,1));
PeakDispersion(8, idx) = cell2mat(AllStats{idx}{1,5}(3,2));
PeakDispersion(9, idx) = cell2mat(AllStats{idx}{1,5}(3,3));
PeakDispDiff(7, idx) = (cell2mat(AllStats{idx}{1,5}(3,3)) - cell2mat(AllStats{idx}{1,5}(3,2)));
PeakDispDiff(8, idx) = (cell2mat(AllStats{idx}{1,5}(3,2)) - cell2mat(AllStats{idx}{1,5}(3,1)));
PeakDispDiff(9, idx) = (cell2mat(AllStats{idx}{1,5}(3,3)) - cell2mat(AllStats{idx}{1,5}(3,1)));

PeakOnset(7, idx) = cell2mat(AllStats{idx}{1,6}(3,1));
PeakOnset(8, idx) = cell2mat(AllStats{idx}{1,6}(3,2));
PeakOnset(9, idx) = cell2mat(AllStats{idx}{1,6}(3,3));
PeakOnsDiff(7, idx) = (cell2mat(AllStats{idx}{1,6}(3,3)) - cell2mat(AllStats{idx}{1,6}(3,2)));
PeakOnsDiff(8, idx) = (cell2mat(AllStats{idx}{1,6}(3,2)) - cell2mat(AllStats{idx}{1,6}(3,1)));
PeakOnsDiff(9, idx) = (cell2mat(AllStats{idx}{1,6}(3,3)) - cell2mat(AllStats{idx}{1,6}(3,1)));

% Analysis for L1 to L3 at I1
PeakAmplitude(10, idx) = cell2mat(AllStats{idx}{1,2}(1,1));
PeakAmplitude(11, idx) = cell2mat(AllStats{idx}{1,2}(2,1));
PeakAmplitude(12, idx) = cell2mat(AllStats{idx}{1,2}(3,1));
PeakAmpDiff(10, idx) = (cell2mat(AllStats{idx}{1,2}(3,1)) - cell2mat(AllStats{idx}{1,2}(2,1)));
PeakAmpDiff(11, idx) = (cell2mat(AllStats{idx}{1,2}(2,1)) - cell2mat(AllStats{idx}{1,2}(1,1)));
PeakAmpDiff(12, idx) = (cell2mat(AllStats{idx}{1,2}(3,1)) - cell2mat(AllStats{idx}{1,2}(1,1)));

PeakDelay(10, idx) = cell2mat(AllStats{idx}{1,1}(1,1));
PeakDelay(11, idx) = cell2mat(AllStats{idx}{1,1}(2,1));
PeakDelay(12, idx) = cell2mat(AllStats{idx}{1,1}(3,1));
PeakDelDiff(10, idx) = (cell2mat(AllStats{idx}{1,1}(3,1)) - cell2mat(AllStats{idx}{1,1}(2,1)));
PeakDelDiff(11, idx) = (cell2mat(AllStats{idx}{1,1}(2,1)) - cell2mat(AllStats{idx}{1,1}(1,1)));
PeakDelDiff(12, idx) = (cell2mat(AllStats{idx}{1,1}(3,1)) - cell2mat(AllStats{idx}{1,1}(1,1)));

PeakEra(10, idx) = cell2mat(AllStats{idx}{1,3}(1,1));
PeakEra(11, idx) = cell2mat(AllStats{idx}{1,3}(2,1));
PeakEra(12, idx) = cell2mat(AllStats{idx}{1,3}(3,1));
PeakEraDiff(10, idx) = (cell2mat(AllStats{idx}{1,3}(3,1)) - cell2mat(AllStats{idx}{1,3}(2,1)));
PeakEraDiff(11, idx) = (cell2mat(AllStats{idx}{1,3}(2,1)) - cell2mat(AllStats{idx}{1,3}(1,1)));
PeakEraDiff(12, idx) = (cell2mat(AllStats{idx}{1,3}(3,1)) - cell2mat(AllStats{idx}{1,3}(1,1)));

TTPEra(10, idx) = cell2mat(AllStats{idx}{1,4}(1,1));
TTPEra(11, idx) = cell2mat(AllStats{idx}{1,4}(2,1));
TTPEra(12, idx) = cell2mat(AllStats{idx}{1,4}(3,1));
TTPEraDiff(10, idx) = (cell2mat(AllStats{idx}{1,4}(3,1)) - cell2mat(AllStats{idx}{1,4}(2,1)));
TTPEraDiff(11, idx) = (cell2mat(AllStats{idx}{1,4}(2,1)) - cell2mat(AllStats{idx}{1,4}(1,1)));
TTPEraDiff(12, idx) = (cell2mat(AllStats{idx}{1,4}(3,1)) - cell2mat(AllStats{idx}{1,4}(1,1)));

PeakDispersion(10, idx) = cell2mat(AllStats{idx}{1,5}(1,1));
PeakDispersion(11, idx) = cell2mat(AllStats{idx}{1,5}(2,1));
PeakDispersion(12, idx) = cell2mat(AllStats{idx}{1,5}(3,1));
PeakDispDiff(10, idx) = (cell2mat(AllStats{idx}{1,5}(3,1)) - cell2mat(AllStats{idx}{1,5}(2,1)));
PeakDispDiff(11, idx) = (cell2mat(AllStats{idx}{1,5}(2,1)) - cell2mat(AllStats{idx}{1,5}(1,1)));
PeakDispDiff(12, idx) = (cell2mat(AllStats{idx}{1,5}(3,1)) - cell2mat(AllStats{idx}{1,5}(1,1)));

PeakOnset(10, idx) = cell2mat(AllStats{idx}{1,6}(1,1));
PeakOnset(11, idx) = cell2mat(AllStats{idx}{1,6}(2,1));
PeakOnset(12, idx) = cell2mat(AllStats{idx}{1,6}(3,1));
PeakOnsDiff(10, idx) = (cell2mat(AllStats{idx}{1,6}(3,1)) - cell2mat(AllStats{idx}{1,6}(2,1)));
PeakOnsDiff(11, idx) = (cell2mat(AllStats{idx}{1,6}(2,1)) - cell2mat(AllStats{idx}{1,6}(1,1)));
PeakOnsDiff(12, idx) = (cell2mat(AllStats{idx}{1,6}(3,1)) - cell2mat(AllStats{idx}{1,6}(1,1)));

% Analysis for L1 to L3 at I2
PeakAmplitude(13, idx) = cell2mat(AllStats{idx}{1,2}(1,2));
PeakAmplitude(14, idx) = cell2mat(AllStats{idx}{1,2}(2,2));
PeakAmplitude(15, idx) = cell2mat(AllStats{idx}{1,2}(3,2));
PeakAmpDiff(13, idx) = (cell2mat(AllStats{idx}{1,2}(3,2)) - cell2mat(AllStats{idx}{1,2}(2,2)));
PeakAmpDiff(14, idx) = (cell2mat(AllStats{idx}{1,2}(2,2)) - cell2mat(AllStats{idx}{1,2}(1,2)));
PeakAmpDiff(15, idx) = (cell2mat(AllStats{idx}{1,2}(3,2)) - cell2mat(AllStats{idx}{1,2}(1,2)));

PeakDelay(13, idx) = cell2mat(AllStats{idx}{1,1}(1,2));
PeakDelay(14, idx) = cell2mat(AllStats{idx}{1,1}(2,2));
PeakDelay(15, idx) = cell2mat(AllStats{idx}{1,1}(3,2));
PeakDelDiff(13, idx) = (cell2mat(AllStats{idx}{1,1}(3,2)) - cell2mat(AllStats{idx}{1,1}(2,2)));
PeakDelDiff(14, idx) = (cell2mat(AllStats{idx}{1,1}(2,2)) - cell2mat(AllStats{idx}{1,1}(1,2)));
PeakDelDiff(15, idx) = (cell2mat(AllStats{idx}{1,1}(3,2)) - cell2mat(AllStats{idx}{1,1}(1,2)));

PeakEra(13, idx) = cell2mat(AllStats{idx}{1,3}(1,2));
PeakEra(14, idx) = cell2mat(AllStats{idx}{1,3}(2,2));
PeakEra(15, idx) = cell2mat(AllStats{idx}{1,3}(3,2));
PeakEraDiff(13, idx) = (cell2mat(AllStats{idx}{1,3}(3,2)) - cell2mat(AllStats{idx}{1,3}(2,2)));
PeakEraDiff(14, idx) = (cell2mat(AllStats{idx}{1,3}(2,2)) - cell2mat(AllStats{idx}{1,3}(1,2)));
PeakEraDiff(15, idx) = (cell2mat(AllStats{idx}{1,3}(3,2)) - cell2mat(AllStats{idx}{1,3}(1,2)));

TTPEra(13, idx) = cell2mat(AllStats{idx}{1,4}(1,2));
TTPEra(14, idx) = cell2mat(AllStats{idx}{1,4}(2,2));
TTPEra(15, idx) = cell2mat(AllStats{idx}{1,4}(3,2));
TTPEraDiff(13, idx) = (cell2mat(AllStats{idx}{1,4}(3,2)) - cell2mat(AllStats{idx}{1,4}(2,2)));
TTPEraDiff(14, idx) = (cell2mat(AllStats{idx}{1,4}(2,2)) - cell2mat(AllStats{idx}{1,4}(1,2)));
TTPEraDiff(15, idx) = (cell2mat(AllStats{idx}{1,4}(3,2)) - cell2mat(AllStats{idx}{1,4}(1,2)));

PeakDispersion(13, idx) = cell2mat(AllStats{idx}{1,5}(1,2));
PeakDispersion(14, idx) = cell2mat(AllStats{idx}{1,5}(2,2));
PeakDispersion(15, idx) = cell2mat(AllStats{idx}{1,5}(3,2));
PeakDispDiff(13, idx) = (cell2mat(AllStats{idx}{1,5}(3,2)) - cell2mat(AllStats{idx}{1,5}(2,2)));
PeakDispDiff(14, idx) = (cell2mat(AllStats{idx}{1,5}(2,2)) - cell2mat(AllStats{idx}{1,5}(1,2)));
PeakDispDiff(15, idx) = (cell2mat(AllStats{idx}{1,5}(3,2)) - cell2mat(AllStats{idx}{1,5}(1,2)));

PeakOnset(13, idx) = cell2mat(AllStats{idx}{1,6}(1,2));
PeakOnset(14, idx) = cell2mat(AllStats{idx}{1,6}(2,2));
PeakOnset(15, idx) = cell2mat(AllStats{idx}{1,6}(3,2));
PeakOnsDiff(13, idx) = (cell2mat(AllStats{idx}{1,6}(3,2)) - cell2mat(AllStats{idx}{1,6}(2,2)));
PeakOnsDiff(14, idx) = (cell2mat(AllStats{idx}{1,6}(2,2)) - cell2mat(AllStats{idx}{1,6}(1,2)));
PeakOnsDiff(15, idx) = (cell2mat(AllStats{idx}{1,6}(3,2)) - cell2mat(AllStats{idx}{1,6}(1,2)));

% Analysis for L1 to L3 at I3
PeakAmplitude(16, idx) = cell2mat(AllStats{idx}{1,2}(1,3));
PeakAmplitude(17, idx) = cell2mat(AllStats{idx}{1,2}(2,3));
PeakAmplitude(18, idx) = cell2mat(AllStats{idx}{1,2}(3,3));
PeakAmpDiff(16, idx) = (cell2mat(AllStats{idx}{1,2}(3,3)) - cell2mat(AllStats{idx}{1,2}(2,3)));
PeakAmpDiff(17, idx) = (cell2mat(AllStats{idx}{1,2}(2,3)) - cell2mat(AllStats{idx}{1,2}(1,3)));
PeakAmpDiff(18, idx) = (cell2mat(AllStats{idx}{1,2}(3,3)) - cell2mat(AllStats{idx}{1,2}(1,3)));

PeakDelay(16, idx) = cell2mat(AllStats{idx}{1,1}(1,3));
PeakDelay(17, idx) = cell2mat(AllStats{idx}{1,1}(2,3));
PeakDelay(18, idx) = cell2mat(AllStats{idx}{1,1}(3,3));
PeakDelDiff(16, idx) = (cell2mat(AllStats{idx}{1,1}(3,3)) - cell2mat(AllStats{idx}{1,1}(2,3)));
PeakDelDiff(17, idx) = (cell2mat(AllStats{idx}{1,1}(2,3)) - cell2mat(AllStats{idx}{1,1}(1,3)));
PeakDelDiff(18, idx) = (cell2mat(AllStats{idx}{1,1}(3,3)) - cell2mat(AllStats{idx}{1,1}(1,3)));

PeakEra(16, idx) = cell2mat(AllStats{idx}{1,3}(1,3));
PeakEra(17, idx) = cell2mat(AllStats{idx}{1,3}(2,3));
PeakEra(18, idx) = cell2mat(AllStats{idx}{1,3}(3,3));
PeakEraDiff(16, idx) = (cell2mat(AllStats{idx}{1,3}(3,3)) - cell2mat(AllStats{idx}{1,3}(2,3)));
PeakEraDiff(17, idx) = (cell2mat(AllStats{idx}{1,3}(2,3)) - cell2mat(AllStats{idx}{1,3}(1,3)));
PeakEraDiff(18, idx) = (cell2mat(AllStats{idx}{1,3}(3,3)) - cell2mat(AllStats{idx}{1,3}(1,3)));

TTPEra(16, idx) = cell2mat(AllStats{idx}{1,4}(1,3));
TTPEra(17, idx) = cell2mat(AllStats{idx}{1,4}(2,3));
TTPEra(18, idx) = cell2mat(AllStats{idx}{1,4}(3,3));
TTPEraDiff(16, idx) = (cell2mat(AllStats{idx}{1,4}(3,3)) - cell2mat(AllStats{idx}{1,4}(2,3)));
TTPEraDiff(17, idx) = (cell2mat(AllStats{idx}{1,4}(2,3)) - cell2mat(AllStats{idx}{1,4}(1,3)));
TTPEraDiff(18, idx) = (cell2mat(AllStats{idx}{1,4}(3,3)) - cell2mat(AllStats{idx}{1,4}(1,3)));

PeakDispersion(16, idx) = cell2mat(AllStats{idx}{1,5}(1,3));
PeakDispersion(17, idx) = cell2mat(AllStats{idx}{1,5}(2,3));
PeakDispersion(18, idx) = cell2mat(AllStats{idx}{1,5}(3,3));
PeakDispDiff(16, idx) = (cell2mat(AllStats{idx}{1,5}(3,3)) - cell2mat(AllStats{idx}{1,5}(2,3)));
PeakDispDiff(17, idx) = (cell2mat(AllStats{idx}{1,5}(2,3)) - cell2mat(AllStats{idx}{1,5}(1,3)));
PeakDispDiff(18, idx) = (cell2mat(AllStats{idx}{1,5}(3,3)) - cell2mat(AllStats{idx}{1,5}(1,3)));

PeakOnset(16, idx) = cell2mat(AllStats{idx}{1,6}(1,3));
PeakOnset(17, idx) = cell2mat(AllStats{idx}{1,6}(2,3));
PeakOnset(18, idx) = cell2mat(AllStats{idx}{1,6}(3,3));
PeakOnsDiff(16, idx) = (cell2mat(AllStats{idx}{1,6}(3,3)) - cell2mat(AllStats{idx}{1,6}(2,3)));
PeakOnsDiff(17, idx) = (cell2mat(AllStats{idx}{1,6}(2,3)) - cell2mat(AllStats{idx}{1,6}(1,3)));
PeakOnsDiff(18, idx) = (cell2mat(AllStats{idx}{1,6}(3,3)) - cell2mat(AllStats{idx}{1,6}(1,3)));

end
%%
for subjs = 1:size(PeakDelay,2)
    for conds = 1:9
        CorrectedPeakAmplitude(conds,subjs) = [PeakAmplitude(conds,subjs) - nanmean(PeakAmplitude(1:9,subjs)) + nanmean(PeakAmplitude(1:9,:), 'all')];
        CorrectedPeakDelay(conds,subjs) = [PeakDelay(conds,subjs) - nanmean(PeakDelay(1:9,subjs)) + nanmean(PeakDelay(1:9,:), 'all')];
        CorrectedPeakEra(conds,subjs) = [PeakEra(conds,subjs) - nanmean(PeakEra(1:9,subjs)) + nanmean(PeakEra(1:9,:), 'all')];
        CorrectedTTPEra(conds,subjs) = [TTPEra(conds,subjs) - nanmean(TTPEra(1:9,subjs)) + nanmean(TTPEra(1:9,:), 'all')];
        CorrectedPeakDispersion(conds,subjs) = [PeakDispersion(conds,subjs) - nanmean(PeakDispersion(1:9,subjs)) + nanmean(PeakDispersion(1:9,:), 'all')];
        CorrectedPeakOnset(conds,subjs) = [PeakOnset(conds,subjs) - nanmean(PeakOnset(1:9,subjs)) + nanmean(PeakOnset(1:9,:), 'all')];
        
        CorrectedPeakAmpDiff(conds,subjs) = [PeakAmpDiff(conds,subjs) - nanmean(PeakAmpDiff(1:9,subjs)) + nanmean(PeakAmpDiff(1:9,:), 'all')];
        CorrectedPeakDelDiff(conds,subjs) = [PeakDelDiff(conds,subjs) - nanmean(PeakDelDiff(1:9,subjs)) + nanmean(PeakDelDiff(1:9,:), 'all')];
        CorrectedPeakEraDiff(conds,subjs) = [PeakEraDiff(conds,subjs) - nanmean(PeakEraDiff(1:9,subjs)) + nanmean(PeakEraDiff(1:9,:), 'all')];
        CorrectedTTPEraDiff(conds,subjs) = [TTPEraDiff(conds,subjs) - nanmean(TTPEraDiff(1:9,subjs)) + nanmean(TTPEraDiff(1:9,:), 'all')];
        CorrectedPeakDispDiff(conds,subjs) = [PeakDispDiff(conds,subjs) - nanmean(PeakDispDiff(1:9,subjs)) + nanmean(PeakDispDiff(1:9,:), 'all')];
        CorrectedPeakOnsDiff(conds,subjs) = [PeakOnsDiff(conds,subjs) - nanmean(PeakOnsDiff(1:9,subjs)) + nanmean(PeakOnsDiff(1:9,:), 'all')];
    end
    for conds = 10:18
        CorrectedPeakAmplitude(conds,subjs) = [PeakAmplitude(conds,subjs) - nanmean(PeakAmplitude(10:18,subjs)) + nanmean(PeakAmplitude(10:18,:), 'all')];
        CorrectedPeakDelay(conds,subjs) = [PeakDelay(conds,subjs) - nanmean(PeakDelay(10:18,subjs)) + nanmean(PeakDelay(10:18,:), 'all')];
        CorrectedPeakEra(conds,subjs) = [PeakEra(conds,subjs) - nanmean(PeakEra(10:18,subjs)) + nanmean(PeakEra(10:18,:), 'all')];
        CorrectedTTPEra(conds,subjs) = [TTPEra(conds,subjs) - nanmean(TTPEra(10:18,subjs)) + nanmean(TTPEra(10:18,:), 'all')];
        CorrectedPeakDispersion(conds,subjs) = [PeakDispersion(conds,subjs) - nanmean(PeakDispersion(10:18,subjs)) + nanmean(PeakDispersion(10:18,:), 'all')];
        CorrectedPeakOnset(conds,subjs) = [PeakOnset(conds,subjs) - nanmean(PeakOnset(10:18,subjs)) + nanmean(PeakOnset(10:18,:), 'all')];
        
        CorrectedPeakAmpDiff(conds,subjs) = [PeakAmpDiff(conds,subjs) - nanmean(PeakAmpDiff(10:18,subjs)) + nanmean(PeakAmpDiff(10:18,:), 'all')];
        CorrectedPeakDelDiff(conds,subjs) = [PeakDelDiff(conds,subjs) - nanmean(PeakDelDiff(10:18,subjs)) + nanmean(PeakDelDiff(10:18,:), 'all')];
        CorrectedPeakEraDiff(conds,subjs) = [PeakEraDiff(conds,subjs) - nanmean(PeakEraDiff(10:18,subjs)) + nanmean(PeakEraDiff(10:18,:), 'all')];
        CorrectedTTPEraDiff(conds,subjs) = [TTPEraDiff(conds,subjs) - nanmean(TTPEraDiff(10:18,subjs)) + nanmean(TTPEraDiff(10:18,:), 'all')];
        CorrectedPeakDispDiff(conds,subjs) = [PeakDispDiff(conds,subjs) - nanmean(PeakDispDiff(10:18,subjs)) + nanmean(PeakDispDiff(10:18,:), 'all')];
        CorrectedPeakOnsDiff(conds,subjs) = [PeakOnsDiff(conds,subjs) - nanmean(PeakOnsDiff(10:18,subjs)) + nanmean(PeakOnsDiff(10:18,:), 'all')];
    end
end
%%
for condix = 1:size(PeakAmplitude, 1)
    PAV = PeakAmplitude(condix, :)';
    PDV = PeakDelay(condix, :)';
    PADV = PeakAmpDiff(condix, :)';
    PDDV = PeakDelDiff(condix, :)';
    PERAV = PeakEra(condix, :)';
    TTPV = TTPEra(condix, :)';
    PERADV = PeakEraDiff(condix, :)';
    TTPDV = TTPEraDiff(condix, :)';
    PDPV = PeakDispersion(condix, :)';
    PDPDV = PeakDispDiff(condix, :)';
    PONV = PeakOnset(condix, :)';
    PONDV = PeakOnsDiff(condix, :)';
    
    ampmean(condix) = nanmean(PeakAmplitude(condix, :),2);
    delmean(condix) = nanmean(PeakDelay(condix, :),2);
    ampdiffmean(condix) = nanmean(PeakAmpDiff(condix, :),2);
    deldiffmean(condix) = nanmean(PeakDelDiff(condix, :),2);
    ampstd(condix) = nanstd(PeakAmplitude(condix, :));
    delstd(condix) = nanstd(PeakDelay(condix, :));
    ampdiffstd(condix) = nanstd(PeakAmpDiff(condix, :));
    deldiffstd(condix) = nanstd(PeakDelDiff(condix, :));
    
    amppd(condix) = fitdist(PAV, 'Normal');
    ampci{condix} = paramci(amppd(condix));
    ampci{condix} = ampci{condix}(:,1);
    delpd(condix) = fitdist(PDV, 'Normal');
    delci{condix} = paramci(delpd(condix));
    delci{condix} = delci{condix}(:,1);
    
    campmean(condix) = nanmean(CorrectedPeakAmplitude(condix, :));
    campstd(condix) = nanstd(CorrectedPeakAmplitude(condix, :)) * 9/(9-1);
    campci{condix} = [campmean(condix) - (1.96 * campstd(condix) / sqrt(15)), campmean(condix) + (1.96 * campstd(condix) / sqrt(15))];
    cdelmean(condix) = nanmean(CorrectedPeakDelay(condix, :));
    cdelstd(condix) = nanstd(CorrectedPeakDelay(condix, :)) * 9/(9-1);
    cdelci{condix} = [cdelmean(condix) - (1.96 * cdelstd(condix) / sqrt(15)), cdelmean(condix) + (1.96 * cdelstd(condix) / sqrt(15))];
    cperamean(condix) = nanmean(CorrectedPeakEra(condix, :));
    cperastd(condix) = nanstd(CorrectedPeakEra(condix, :)) * 9/(9-1);
    cperaci{condix} = [cperamean(condix) - (1.96 * cperastd(condix) / sqrt(15)), cperamean(condix) + (1.96 * cperastd(condix) / sqrt(15))];
    cttpmean(condix) = nanmean(CorrectedTTPEra(condix, :));
    cttpstd(condix) = nanstd(CorrectedTTPEra(condix, :)) * 9/(9-1);
    cttpci{condix} = [cttpmean(condix) - (1.96 * cttpstd(condix) / sqrt(15)), cttpmean(condix) + (1.96 * cttpstd(condix) / sqrt(15))];
    cpdpmean(condix) = nanmean(CorrectedPeakDispersion(condix, :));
    cpdpstd(condix) = nanstd(CorrectedPeakDispersion(condix, :)) * 9/(9-1);
    cpdpci{condix} = [cpdpmean(condix) - (1.96 * cpdpstd(condix) / sqrt(15)), cpdpmean(condix) + (1.96 * cpdpstd(condix) / sqrt(15))];
    cponmean(condix) = nanmean(CorrectedPeakOnset(condix, :));
    cponstd(condix) = nanstd(CorrectedPeakOnset(condix, :)) * 9/(9-1);
    cponci{condix} = [cponmean(condix) - (1.96 * cponstd(condix) / sqrt(15)), cponmean(condix) + (1.96 * cponstd(condix) / sqrt(15))];
    
    campdiffmean(condix) = nanmean(CorrectedPeakAmpDiff(condix, :));
    campdiffstd(condix) = nanstd(CorrectedPeakAmpDiff(condix, :)) * 9/(9-1);
    campdiffci{condix} = [campdiffmean(condix) - (1.96 * campdiffstd(condix) / sqrt(15)), campdiffmean(condix) + (1.96 * campdiffstd(condix) / sqrt(15))];
    cdeldiffmean(condix) = nanmean(CorrectedPeakDelDiff(condix, :));
    cdeldiffstd(condix) = nanstd(CorrectedPeakDelDiff(condix, :)) * 9/(9-1);
    cdeldiffci{condix} = [cdeldiffmean(condix) - (1.96 * cdeldiffstd(condix) / sqrt(15)), cdeldiffmean(condix) + (1.96 * cdeldiffstd(condix) / sqrt(15))];
    cperadiffmean(condix) = nanmean(CorrectedPeakEraDiff(condix, :));
    cperadiffstd(condix) = nanstd(CorrectedPeakEraDiff(condix, :)) * 9/(9-1);
    cperadiffci{condix} = [cperadiffmean(condix) - (1.96 * cperadiffstd(condix) / sqrt(15)), cperadiffmean(condix) + (1.96 * cperadiffstd(condix) / sqrt(15))];
    cttpdiffmean(condix) = nanmean(CorrectedTTPEraDiff(condix, :));
    cttpdiffstd(condix) = nanstd(CorrectedTTPEraDiff(condix, :)) * 9/(9-1);
    cttpdiffci{condix} = [cttpdiffmean(condix) - (1.96 * cttpdiffstd(condix) / sqrt(15)), cttpdiffmean(condix) + (1.96 * cttpdiffstd(condix) / sqrt(15))];
    cpdpdiffmean(condix) = nanmean(CorrectedPeakDispDiff(condix, :));
    cpdpdiffstd(condix) = nanstd(CorrectedPeakDispDiff(condix, :)) * 9/(9-1);
    cpdpdiffci{condix} = [cpdpdiffmean(condix) - (1.96 * cpdpdiffstd(condix) / sqrt(15)), cpdpdiffmean(condix) + (1.96 * cpdpdiffstd(condix) / sqrt(15))];
    cpondiffmean(condix) = nanmean(CorrectedPeakOnsDiff(condix, :));
    cpondiffstd(condix) = nanstd(CorrectedPeakOnsDiff(condix, :)) * 9/(9-1);
    cpondiffci{condix} = [cpondiffmean(condix) - (1.96 * cpondiffstd(condix) / sqrt(15)), cpondiffmean(condix) + (1.96 * cpondiffstd(condix) / sqrt(15))];
    
    ampdiffpd(condix) = fitdist(PADV, 'Normal');
    ampdiffci{condix} = paramci(ampdiffpd(condix));
    ampdiffci{condix} = ampdiffci{condix}(:,1);
    deldiffpd(condix) = fitdist(PDDV, 'Normal');
    deldiffci{condix} = paramci(deldiffpd(condix));
    deldiffci{condix} = deldiffci{condix}(:,1);
    
    peramean(condix) = nanmean(PeakEra(condix,:),2);
    perastd(condix) = nanstd(PeakEra(condix,:));
    perapd(condix) = fitdist(PERAV, 'Normal');
    peraci{condix} = paramci(perapd(condix));
    peraci{condix} = peraci{condix}(:,1);
    peradiffmean(condix) = nanmean(PeakEraDiff(condix,:),2);
    peradiffstd(condix) = nanstd(PeakEraDiff(condix,:));
    peradiffpd(condix) = fitdist(PERADV, 'Normal');
    peradiffci{condix} = paramci(peradiffpd(condix));
    peradiffci{condix} = peradiffci{condix}(:,1);
    
    ttpmean(condix) = nanmean(TTPEra(condix,:),2);
    ttpstd(condix) = nanstd(TTPEra(condix,:));
    ttppd(condix) = fitdist(TTPV, 'Normal');
    ttpci{condix} = paramci(ttppd(condix));
    ttpci{condix} = ttpci{condix}(:,1);
    ttpdiffmean(condix) = nanmean(TTPEraDiff(condix,:),2);
    ttpdiffstd(condix) = nanstd(TTPEraDiff(condix,:));
    ttpdiffpd(condix) = fitdist(TTPDV, 'Normal');
    ttpdiffci{condix} = paramci(ttpdiffpd(condix));
    ttpdiffci{condix} = ttpdiffci{condix}(:,1);
    
    pdpmean(condix) = nanmean(PeakDispersion(condix,:),2);
    pdpstd(condix) = nanstd(PeakDispersion(condix,:));
    pdppd(condix) = fitdist(PDPV, 'Normal');
    pdpci{condix} = paramci(pdppd(condix));
    pdpci{condix} = pdpci{condix}(:,1);
    pdpdiffmean(condix) = nanmean(PeakDispDiff(condix,:),2);
    pdpdiffstd(condix) = nanstd(PeakDispDiff(condix,:));
    pdpdiffpd(condix) = fitdist(PDPDV, 'Normal');
    pdpdiffci{condix} = paramci(pdpdiffpd(condix));
    pdpdiffci{condix} = pdpdiffci{condix}(:,1);
    
    ponmean(condix) = nanmean(PeakOnset(condix,:),2);
    ponstd(condix) = nanstd(PeakOnset(condix,:));
    ponpd(condix) = fitdist(PONV, 'Normal');
    ponci{condix} = paramci(ponpd(condix));
    ponci{condix} = ponci{condix}(:,1);
    pondiffmean(condix) = nanmean(PeakOnsDiff(condix,:),2);
    pondiffstd(condix) = nanstd(PeakOnsDiff(condix,:));
    pondiffpd(condix) = fitdist(PONDV, 'Normal');
    pondiffci{condix} = paramci(pondiffpd(condix));
    pondiffci{condix} = pondiffci{condix}(:,1);
    
    ampeffect(condix) = ampdiffmean(condix)/ampdiffstd(condix);
    deleffect(condix) = deldiffmean(condix)/deldiffstd(condix);
    peraeffect(condix) = peradiffmean(condix)/peradiffstd(condix);
    ttpeffect(condix) = ttpdiffmean(condix)/ttpdiffstd(condix);
    pdpeffect(condix) = pdpdiffmean(condix)/pdpdiffstd(condix);
    poneffect(condix) = pondiffmean(condix)/pondiffstd(condix);
    
    campeffect(condix) = campdiffmean(condix)/campdiffstd(condix);
    cdeleffect(condix) = cdeldiffmean(condix)/cdeldiffstd(condix);
    cperaeffect(condix) = cperadiffmean(condix)/cperadiffstd(condix);
    cttpeffect(condix) = cttpdiffmean(condix)/cttpdiffstd(condix);
    cpdpeffect(condix) = cpdpdiffmean(condix)/cpdpdiffstd(condix);
    cponeffect(condix) = cpondiffmean(condix)/cpondiffstd(condix);
end
%%

varnames = [{'PeakAmplitude'}, {'PeakAmplitudeSD'}, {'PeakAmplitudeCI'}, {'PeakDelay'}, {'PeakDelaySD'}, {'PeakDelayCI'}, {'EvPeak'}, {'EvPeakSD'}, {'EvPeakCI'}, {'TimetoEvPeak'}, {'TimetoEvPeakSD'}, {'TimetoEvPeakCI'}, {'PeakDispersion'}, {'PeakDispersionSD'}, {'PeakDispersionCI'}, {'PeakOnset'}, {'PeakOnsetSD'}, {'PeakOnsetCI'}];
rownames = [{'L1I1'}, {'L1i2'}, {'L1i3'}, {'L2I1'}, {'L2I2'}, {'L2i3'}, {'L3I1'}, {'L3I2'}, {'L3I3'},  {'L1i1'}, {'L2i1'}, {'L3i1'}, {'L1I2'}, {'L2i2'}, {'L3i2'}, {'L1I3'}, {'L2I3'}, {'L3i3'}];
summaryVals = table(ampmean', ampstd', ampci', delmean', delstd', delci', peramean', perastd', peraci', ttpmean', ttpstd', ttpci', pdpmean', pdpstd', pdpci', ponmean', ponstd', ponci', 'VariableNames', varnames, 'RowNames', rownames);
writetable(summaryVals, 'PSSummaryVals.csv');

varnames = [{'PeakAmplitude'}, {'PeakAmplitudeSD'}, {'PeakAmplitudeCI'}, {'PeakDelay'}, {'PeakDelaySD'}, {'PeakDelayCI'}, {'EvPeak'}, {'EvPeakSD'}, {'EvPeakCI'}, {'TimetoEvPeak'}, {'TimetoEvPeakSD'}, {'TimetoEvPeakCI'}, {'PeakDispersion'}, {'PeakDispersionSD'}, {'PeakDispersionCI'}, {'PeakOnset'}, {'PeakOnsetSD'}, {'PeakOnsetCI'}];
rownames = [{'L1I1'}, {'L1i2'}, {'L1i3'}, {'L2I1'}, {'L2I2'}, {'L2i3'}, {'L3I1'}, {'L3I2'}, {'L3I3'},  {'L1i1'}, {'L2i1'}, {'L3i1'}, {'L1I2'}, {'L2i2'}, {'L3i2'}, {'L1I3'}, {'L2I3'}, {'L3i3'}];
summaryVals = table(campmean', campstd', campci', cdelmean', cdelstd', cdelci', cperamean', cperastd', cperaci', cttpmean', cttpstd', cttpci', cpdpmean', cpdpstd', cpdpci', cponmean', cponstd', cponci', 'VariableNames', varnames, 'RowNames', rownames);
writetable(summaryVals, 'CorrectedPSSummaryVals.csv');

varnamesd = [{'PeakAmpDiff'}, {'PeakAmpDiffSD'}, {'PeakAmpDiffCI'}, {'PeakAmpEffectSz'}, {'PeakDelDiff'}, {'PeakDelDiffSD'}, {'PeakDelDiffCI'}, {'PeakDelEffectSz'}, {'EvPeakDiff'}, {'EvPeakDiffSD'}, {'EvPeakDiffCI'}, {'EvPeakEffectSz'}, {'TimetoEvPeakDiff'}, {'TimetoEvPeakDiffSD'}, {'TimetoEvPeakDiffCI'}, {'TimetoEvPeakEffectSz'}, {'PeakDispDiff'}, {'PeakDispDiffSD'}, {'PeakDispDiffCI'}, {'PeakDispEffectSz'}, {'PeakOnsDiff'}, {'PeakOnsDiffSD'}, {'PeakOnsDiffCI'}, {'PeakOnsEffectSz'}];
rownamesd = [{'L1I3 - L1I2'}, {'L1I2 - L1I1'}, {'L1I3 - L1I1'}, {'L2I3 - L2I2'}, {'L2I2 - L2I1'}, {'L2I3 - L2I1'}, {'L3I3 - L3I2'}, {'L3I2 - L3I1'}, {'L3I3 - L3I1'}, {'L3I1 - L2I1'}, {'L2I1 - L1I1'}, {'L3I1 - L1I1'}, {'L3I2 - L2I2'}, {'L2I2 - L1I2'}, {'L3I2 - L1I2'},  {'L3I3 - L2I3'}, {'L2I3 - L1I3'}, {'L3I3 - L1I3'}];
summaryDiffs = table(ampdiffmean', ampdiffstd', ampdiffci', ampeffect', deldiffmean', deldiffstd', deldiffci', deleffect', peradiffmean', peradiffstd', peradiffci', peraeffect', ttpdiffmean', ttpdiffstd', ttpdiffci', ttpeffect', pdpdiffmean', pdpdiffstd', pdpdiffci', pdpeffect', pondiffmean', pondiffstd', pondiffci', poneffect', 'VariableNames', varnamesd, 'RowNames', rownamesd);
writetable(summaryDiffs, 'PSSummaryDiffs.csv');

varnamesd = [{'PeakAmpDiff'}, {'PeakAmpDiffSD'}, {'PeakAmpDiffCI'}, {'PeakAmpEffectSz'}, {'PeakDelDiff'}, {'PeakDelDiffSD'}, {'PeakDelDiffCI'}, {'PeakDelEffectSz'}, {'EvPeakDiff'}, {'EvPeakDiffSD'}, {'EvPeakDiffCI'}, {'EvPeakEffectSz'}, {'TimetoEvPeakDiff'}, {'TimetoEvPeakDiffSD'}, {'TimetoEvPeakDiffCI'}, {'TimetoEvPeakEffectSz'}, {'PeakDispDiff'}, {'PeakDispDiffSD'}, {'PeakDispDiffCI'}, {'PeakDispEffectSz'}, {'PeakOnsDiff'}, {'PeakOnsDiffSD'}, {'PeakOnsDiffCI'}, {'PeakOnsEffectSz'}];
rownamesd = [{'L1I3 - L1I2'}, {'L1I2 - L1I1'}, {'L1I3 - L1I1'}, {'L2I3 - L2I2'}, {'L2I2 - L2I1'}, {'L2I3 - L2I1'}, {'L3I3 - L3I2'}, {'L3I2 - L3I1'}, {'L3I3 - L3I1'}, {'L3I1 - L2I1'}, {'L2I1 - L1I1'}, {'L3I1 - L1I1'}, {'L3I2 - L2I2'}, {'L2I2 - L1I2'}, {'L3I2 - L1I2'},  {'L3I3 - L2I3'}, {'L2I3 - L1I3'}, {'L3I3 - L1I3'}];
summaryDiffs = table(campdiffmean', campdiffstd', campdiffci', campeffect', cdeldiffmean', cdeldiffstd', cdeldiffci', cdeleffect', cperadiffmean', cperadiffstd', cperadiffci', cperaeffect', cttpdiffmean', cttpdiffstd', cttpdiffci', cttpeffect', cpdpdiffmean', cpdpdiffstd', cpdpdiffci', cpdpeffect', cpondiffmean', cpondiffstd', cpondiffci', cponeffect', 'VariableNames', varnamesd, 'RowNames', rownamesd);
writetable(summaryDiffs, 'CorrectedPSSummaryDiffs.csv');