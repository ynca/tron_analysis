clearvars();
cd('/Volumes/Passport/TRoN/Analysis/PSubj2/Data');
eventtype = "A";
filedir = dir('A1*.mat');

for fileidx = 1:numel(filedir)
    clearvars('-except', 'eventtype', 'filedir', 'fileidx', 'SubjAvgTC', 'PerSubjBootStpInput');
    filename = sprintf('%s', [filedir(fileidx).folder, '/', filedir(fileidx).name]);
    load(filename);
    subjname = [filedir(fileidx).name(4:10)];

    if eventtype == 'V'
        eventlengths = [0.1 0.3 0.9];
        eventintensities = [0.01 0.1 1];
        roiname = 'V1';
        
        for datacnt = 1:numel(CurrentV)
        VOnset(:, datacnt) = CurrentV(datacnt).VisualEvs(1:end);
        VLength(:,datacnt) = CurrentV(datacnt).VisualLengths;
        VInt(:,datacnt) = CurrentV(datacnt).VisualInts;
        end
        
        for datacnt = 1:numel(CurrentV)
        for evcnt = 1:size(VOnset(:,datacnt),1)
            VTC(:,evcnt,datacnt) = CurrentV(datacnt).RawData((VOnset(evcnt,datacnt)-10):(VOnset(evcnt,datacnt)+30));
        end
        end

        for datacnt = 1:size(VLength,2)
        for evcnt = 1:size(VLength(:,datacnt),1)
            for Lcon = 1:numel(eventlengths)
                Lsearch{Lcon}(evcnt, datacnt) = eq(VLength(evcnt, datacnt),eventlengths(Lcon));
            end
        end
        for evcnt = 1:size(VInt(:,datacnt),1)
            for Icon = 1:numel(eventintensities)
                Isearch{Icon}(evcnt, datacnt) = eq(VInt(evcnt, datacnt),eventintensities(Icon));
            end
        end
        end

        ProtoMatrix{3, 3} = [];

        for Lcon = 1:numel(eventlengths)
        for Icon = 1:numel(eventintensities)
            ConditionMap{Lcon, Icon} = [eventlengths(Lcon) eventintensities(Icon)];
            for datacnt = 1:size(Lsearch{Lcon},2)
                matcnt = 1;
                for evcnt = 1:size(Lsearch{Lcon}(:,datacnt),1)
                    if Lsearch{Lcon}(evcnt, datacnt) == 1 && Isearch{Icon}(evcnt, datacnt) == 1
                        ProtoMatrix{Lcon, Icon}(1:25, matcnt, datacnt) = VTC(11:35, evcnt,datacnt);
                        matcnt = matcnt+1;
                    end
                end
            end
        end
        end

    elseif eventtype == 'A' % A1 timecourse processing to build protomatrix
        eventlengths = [0.1 0.3 0.9];
        eventintensities = [0.1 0.3 1];
        roiname = 'A1';
        
        for datacnt = 1:numel(CurrentA)
        AOnset(:, datacnt) = CurrentA(datacnt).AuditoryEvs(1:end);
        ALength(:,datacnt) = CurrentA(datacnt).AuditoryLengths;
        AInt(:,datacnt) = CurrentA(datacnt).AuditoryInts;
        end
        
        for datacnt = 1:numel(CurrentA)
        for evcnt = 1:size(AOnset(:,datacnt),1)
            ATC(:,evcnt,datacnt) = CurrentA(datacnt).RawData((AOnset(evcnt,datacnt)-10):(AOnset(evcnt,datacnt)+30));
        end
        end

        for datacnt = 1:size(ALength,2)
        for evcnt = 1:size(ALength(:,datacnt),1)
            for Lcon = 1:numel(eventlengths)
                Lsearch{Lcon}(evcnt, datacnt) = eq(ALength(evcnt, datacnt),eventlengths(Lcon));
            end
        end
        for evcnt = 1:size(AInt(:,datacnt),1)
            for Icon = 1:numel(eventintensities)
                Isearch{Icon}(evcnt, datacnt) = eq(AInt(evcnt, datacnt),eventintensities(Icon));
            end
        end
        end

        ProtoMatrix{3, 3} = [];

        for Lcon = 1:numel(eventlengths)
        for Icon = 1:numel(eventintensities)
            ConditionMap{Lcon, Icon} = [eventlengths(Lcon) eventintensities(Icon)];
            for datacnt = 1:size(Lsearch{Lcon},2)
                matcnt = 1;
                for evcnt = 1:size(Lsearch{Lcon}(:,datacnt),1)
                    if Lsearch{Lcon}(evcnt, datacnt) == 1 && Isearch{Icon}(evcnt, datacnt) == 1
                        ProtoMatrix{Lcon, Icon}(1:25, matcnt, datacnt) = ATC(11:35, evcnt,datacnt);
                        matcnt = matcnt+1;
                    end
                end
            end
        end
        end
        
    end
    PerSubjBootStpInput{fileidx} = ProtoMatrix;
    
    for lengths = 1:size(ProtoMatrix,1)
        for ints = 1:size(ProtoMatrix,2)
            AvgTC{lengths, ints} = nanmean(ProtoMatrix{lengths, ints}, 2);
            AvgTC{lengths, ints} = reshape(AvgTC{lengths, ints}, [size(AvgTC{lengths, ints},1) size(AvgTC{lengths, ints},3)]);
            AvgTC{lengths, ints} = nanmean(AvgTC{lengths, ints},2);
            BaselineVector{lengths, ints} = AvgTC{lengths, ints}(5);
            for i = 1:size(AvgTC{lengths, ints}, 2)
                AvgTC{lengths, ints}(:, i) = ((AvgTC{lengths, ints}(:, i) - BaselineVector{lengths, ints}(1,i)) / BaselineVector{lengths, ints}(1,i)) * 100;
            end
            SubjAvgTC{lengths, ints}(fileidx, 1:25) = AvgTC{lengths, ints};
            
       
        end
    end
end

cd('/Volumes/Passport/TRoN/Analysis/Boot10k3');

[output1, bootsam] = BootStrap2(SubjAvgTC{1,1}, 0.625, 15);
save(sprintf('%s', ['BootstrapStats', '_', 'L1', '_', 'I1']), 'output1');
savefig(sprintf('%s', ['BootstrapFig', '_', 'L1', '_', 'I1']));
hold off;

[output2, bootsam] = BootStrap2(SubjAvgTC{1,2}, 0.625, 15);
save(sprintf('%s', ['BootstrapStats', '_', 'L1', '_', 'I2']), 'output2');
savefig(sprintf('%s', ['BootstrapFig', '_', 'L1', '_', 'I2']));
hold off;

[output3, bootsam] = BootStrap2(SubjAvgTC{1,3}, 0.625, 15);
save(sprintf('%s', ['BootstrapStats', '_', 'L1', '_', 'I3']), 'output3');
savefig(sprintf('%s', ['BootstrapFig', '_', 'L1', '_', 'I3']));
hold off;

[output4, bootsam] = BootStrap2(SubjAvgTC{2,1}, 0.625, 15);
save(sprintf('%s', ['BootstrapStats', '_', 'L2', '_', 'I1']), 'output4');
savefig(sprintf('%s', ['BootstrapFig', '_', 'L2', '_', 'I1']));
hold off;

[output5, bootsam] = BootStrap2(SubjAvgTC{2,2}, 0.625, 15);
save(sprintf('%s', ['BootstrapStats', '_', 'L2', '_', 'I2']), 'output5');
savefig(sprintf('%s', ['BootstrapFig', '_', 'L2', '_', 'I2']));
hold off;

[output6, bootsam] = BootStrap2(SubjAvgTC{2,3}, 0.625, 15);
save(sprintf('%s', ['BootstrapStats', '_', 'L2', '_', 'I3']), 'output6');
savefig(sprintf('%s', ['BootstrapFig', '_', 'L2', '_', 'I3']));
hold off;

[output7, bootsam] = BootStrap2(SubjAvgTC{3,1}, 0.625, 15);
save(sprintf('%s', ['BootstrapStats', '_', 'L3', '_', 'I1']), 'output7');
savefig(sprintf('%s', ['BootstrapFig', '_', 'L3', '_', 'I1']));
hold off;

[output8, bootsam] = BootStrap2(SubjAvgTC{3,2}, 0.625, 15);
save(sprintf('%s', ['BootstrapStats', '_', 'L3', '_', 'I2']), 'output8');
savefig(sprintf('%s', ['BootstrapFig', '_', 'L3', '_', 'I2']));
hold off;

[output9, bootsam] = BootStrap2(SubjAvgTC{3,3}, 0.625, 15);
save(sprintf('%s', ['BootstrapStats', '_', 'L3', '_', 'I3']), 'output9');
savefig(sprintf('%s', ['BootstrapFig', '_', 'L3', '_', 'I3']));
hold off;

%%
for idx = 1:numel(output1)
    
    % Analysis for I1 to I3 at L1
    PeakAmplitude(1, idx) = output1(idx).MaxValue;
    PeakAmplitude(2, idx) = output2(idx).MaxValue;
    PeakAmplitude(3, idx) = output3(idx).MaxValue;
    PeakAmpDiff(1, idx) = (output3(idx).MaxValue - output2(idx).MaxValue);
    PeakAmpDiff(2, idx) = (output2(idx).MaxValue - output1(idx).MaxValue);
    PeakAmpDiff(3, idx) = (output3(idx).MaxValue - output1(idx).MaxValue);
    
    PeakDelay(1, idx) = output1(idx).MaxIndexInSeconds;
    PeakDelay(2, idx) = output2(idx).MaxIndexInSeconds;
    PeakDelay(3, idx) = output3(idx).MaxIndexInSeconds;
    PeakDelDiff(1, idx) = (output3(idx).MaxIndexInSeconds - output2(idx).MaxIndexInSeconds);
    PeakDelDiff(2, idx) = (output2(idx).MaxIndexInSeconds - output1(idx).MaxIndexInSeconds);
    PeakDelDiff(3, idx) = (output3(idx).MaxIndexInSeconds - output1(idx).MaxIndexInSeconds);
    
    PeakEvAvg(1, idx) = output1(idx).PeakEventAvg;
    PeakEvAvg(2, idx) = output2(idx).PeakEventAvg;
    PeakEvAvg(3, idx) = output3(idx).PeakEventAvg;
    PeakEvAvgDiff(1, idx) = (output3(idx).PeakEventAvg - output2(idx).PeakEventAvg);
    PeakEvAvgDiff(2, idx) = (output2(idx).PeakEventAvg - output1(idx).PeakEventAvg);
    PeakEvAvgDiff(3, idx) = (output3(idx).PeakEventAvg - output1(idx).PeakEventAvg);
    
    TTPeakEvAvg(1, idx) = output1(idx).TimeToPeakEvAvg(1);
    TTPeakEvAvg(2, idx) = output2(idx).TimeToPeakEvAvg(1);
    TTPeakEvAvg(3, idx) = output3(idx).TimeToPeakEvAvg(1);
    TTPeakEvAvgDiff(1, idx) = (output3(idx).TimeToPeakEvAvg(1) - output2(idx).TimeToPeakEvAvg(1));
    TTPeakEvAvgDiff(2, idx) = (output2(idx).TimeToPeakEvAvg(1) - output1(idx).TimeToPeakEvAvg(1));
    TTPeakEvAvgDiff(3, idx) = (output3(idx).TimeToPeakEvAvg(1) - output1(idx).TimeToPeakEvAvg(1));
    
    PeakDispersion(1, idx) = output1(idx).FWHMInSeconds;
    PeakDispersion(2, idx) = output2(idx).FWHMInSeconds;
    PeakDispersion(3, idx) = output3(idx).FWHMInSeconds;
    PeakDispDiff(1, idx) = (output3(idx).FWHMInSeconds - output2(idx).FWHMInSeconds);
    PeakDispDiff(2, idx) = (output2(idx).FWHMInSeconds - output1(idx).FWHMInSeconds);
    PeakDispDiff(3, idx) = (output3(idx).FWHMInSeconds - output1(idx).FWHMInSeconds);
    
    PeakOnset(1, idx) = output1(idx).OnsetTimeInSeconds;
    PeakOnset(2, idx) = output2(idx).OnsetTimeInSeconds;
    PeakOnset(3, idx) = output3(idx).OnsetTimeInSeconds;
    PeakOnsDiff(1, idx) = (output3(idx).OnsetTimeInSeconds - output2(idx).OnsetTimeInSeconds);
    PeakOnsDiff(2, idx) = (output2(idx).OnsetTimeInSeconds - output1(idx).OnsetTimeInSeconds);
    PeakOnsDiff(3, idx) = (output3(idx).OnsetTimeInSeconds - output1(idx).OnsetTimeInSeconds);
    
    % Analysis for I1 to I3 at L2
    PeakAmplitude(4, idx) = output4(idx).MaxValue;
    PeakAmplitude(5, idx) = output5(idx).MaxValue;
    PeakAmplitude(6, idx) = output6(idx).MaxValue;
    PeakAmpDiff(4, idx) = (output6(idx).MaxValue - output5(idx).MaxValue);
    PeakAmpDiff(5, idx) = (output5(idx).MaxValue - output4(idx).MaxValue);
    PeakAmpDiff(6, idx) = (output6(idx).MaxValue - output4(idx).MaxValue);
    
    PeakDelay(4, idx) = output4(idx).MaxIndexInSeconds;
    PeakDelay(5, idx) = output5(idx).MaxIndexInSeconds;
    PeakDelay(6, idx) = output6(idx).MaxIndexInSeconds;
    PeakDelDiff(4, idx) = (output6(idx).MaxIndexInSeconds - output5(idx).MaxIndexInSeconds);
    PeakDelDiff(5, idx) = (output5(idx).MaxIndexInSeconds - output4(idx).MaxIndexInSeconds);
    PeakDelDiff(6, idx) = (output6(idx).MaxIndexInSeconds - output4(idx).MaxIndexInSeconds);
    
    PeakEvAvg(4, idx) = output4(idx).PeakEventAvg;
    PeakEvAvg(5, idx) = output5(idx).PeakEventAvg;
    PeakEvAvg(6, idx) = output6(idx).PeakEventAvg;
    PeakEvAvgDiff(4, idx) = (output6(idx).PeakEventAvg - output5(idx).PeakEventAvg);
    PeakEvAvgDiff(5, idx) = (output5(idx).PeakEventAvg - output4(idx).PeakEventAvg);
    PeakEvAvgDiff(6, idx) = (output6(idx).PeakEventAvg - output4(idx).PeakEventAvg);
    
    TTPeakEvAvg(4, idx) = output4(idx).TimeToPeakEvAvg(1);
    TTPeakEvAvg(5, idx) = output5(idx).TimeToPeakEvAvg(1);
    TTPeakEvAvg(6, idx) = output6(idx).TimeToPeakEvAvg(1);
    TTPeakEvAvgDiff(4, idx) = (output6(idx).TimeToPeakEvAvg(1) - output5(idx).TimeToPeakEvAvg(1));
    TTPeakEvAvgDiff(5, idx) = (output5(idx).TimeToPeakEvAvg(1) - output4(idx).TimeToPeakEvAvg(1));
    TTPeakEvAvgDiff(6, idx) = (output6(idx).TimeToPeakEvAvg(1) - output4(idx).TimeToPeakEvAvg(1));
    
    PeakDispersion(4, idx) = output4(idx).FWHMInSeconds;
    PeakDispersion(5, idx) = output5(idx).FWHMInSeconds;
    PeakDispersion(6, idx) = output6(idx).FWHMInSeconds;
    PeakDispDiff(4, idx) = (output6(idx).FWHMInSeconds - output5(idx).FWHMInSeconds);
    PeakDispDiff(5, idx) = (output5(idx).FWHMInSeconds - output4(idx).FWHMInSeconds);
    PeakDispDiff(6, idx) = (output6(idx).FWHMInSeconds - output4(idx).FWHMInSeconds);
    
    PeakOnset(4, idx) = output4(idx).OnsetTimeInSeconds;
    PeakOnset(5, idx) = output5(idx).OnsetTimeInSeconds;
    PeakOnset(6, idx) = output6(idx).OnsetTimeInSeconds;
    PeakOnsDiff(4, idx) = (output6(idx).OnsetTimeInSeconds - output5(idx).OnsetTimeInSeconds);
    PeakOnsDiff(5, idx) = (output5(idx).OnsetTimeInSeconds - output4(idx).OnsetTimeInSeconds);
    PeakOnsDiff(6, idx) = (output6(idx).OnsetTimeInSeconds - output4(idx).OnsetTimeInSeconds);
    
    % Analysis for I1 to I3 at L3
    PeakAmplitude(7, idx) = output7(idx).MaxValue;
    PeakAmplitude(8, idx) = output8(idx).MaxValue;
    PeakAmplitude(9, idx) = output9(idx).MaxValue;
    PeakAmpDiff(7, idx) = (output9(idx).MaxValue - output8(idx).MaxValue);
    PeakAmpDiff(8, idx) = (output8(idx).MaxValue - output7(idx).MaxValue);
    PeakAmpDiff(9, idx) = (output9(idx).MaxValue - output7(idx).MaxValue);
    
    PeakDelay(7, idx) = output7(idx).MaxIndexInSeconds;
    PeakDelay(8, idx) = output8(idx).MaxIndexInSeconds;
    PeakDelay(9, idx) = output9(idx).MaxIndexInSeconds;
    PeakDelDiff(7, idx) = (output9(idx).MaxIndexInSeconds - output8(idx).MaxIndexInSeconds);
    PeakDelDiff(8, idx) = (output8(idx).MaxIndexInSeconds - output7(idx).MaxIndexInSeconds);
    PeakDelDiff(9, idx) = (output9(idx).MaxIndexInSeconds - output7(idx).MaxIndexInSeconds);
    
    PeakEvAvg(7, idx) = output7(idx).PeakEventAvg;
    PeakEvAvg(8, idx) = output8(idx).PeakEventAvg;
    PeakEvAvg(9, idx) = output9(idx).PeakEventAvg;
    PeakEvAvgDiff(7, idx) = (output9(idx).PeakEventAvg - output8(idx).PeakEventAvg);
    PeakEvAvgDiff(8, idx) = (output8(idx).PeakEventAvg - output7(idx).PeakEventAvg);
    PeakEvAvgDiff(9, idx) = (output9(idx).PeakEventAvg - output7(idx).PeakEventAvg);
    
    TTPeakEvAvg(7, idx) = output7(idx).TimeToPeakEvAvg(1);
    TTPeakEvAvg(8, idx) = output8(idx).TimeToPeakEvAvg(1);
    TTPeakEvAvg(9, idx) = output9(idx).TimeToPeakEvAvg(1);
    TTPeakEvAvgDiff(7, idx) = (output9(idx).TimeToPeakEvAvg(1) - output8(idx).TimeToPeakEvAvg(1));
    TTPeakEvAvgDiff(8, idx) = (output8(idx).TimeToPeakEvAvg(1) - output7(idx).TimeToPeakEvAvg(1));
    TTPeakEvAvgDiff(9, idx) = (output9(idx).TimeToPeakEvAvg(1) - output7(idx).TimeToPeakEvAvg(1));
    
    PeakDispersion(7, idx) = output7(idx).FWHMInSeconds;
    PeakDispersion(8, idx) = output8(idx).FWHMInSeconds;
    PeakDispersion(9, idx) = output9(idx).FWHMInSeconds;
    PeakDispDiff(7, idx) = (output9(idx).FWHMInSeconds - output8(idx).FWHMInSeconds);
    PeakDispDiff(8, idx) = (output8(idx).FWHMInSeconds - output7(idx).FWHMInSeconds);
    PeakDispDiff(9, idx) = (output9(idx).FWHMInSeconds - output7(idx).FWHMInSeconds);
    
    PeakOnset(7, idx) = output7(idx).OnsetTimeInSeconds;
    PeakOnset(8, idx) = output8(idx).OnsetTimeInSeconds;
    PeakOnset(9, idx) = output9(idx).OnsetTimeInSeconds;
    PeakOnsDiff(7, idx) = (output9(idx).OnsetTimeInSeconds - output8(idx).OnsetTimeInSeconds);
    PeakOnsDiff(8, idx) = (output8(idx).OnsetTimeInSeconds - output7(idx).OnsetTimeInSeconds);
    PeakOnsDiff(9, idx) = (output9(idx).OnsetTimeInSeconds - output7(idx).OnsetTimeInSeconds);
    
    % Analysis for L1 to L3 at I1
    PeakAmplitude(10, idx) = output1(idx).MaxValue;
    PeakAmplitude(11, idx) = output4(idx).MaxValue;
    PeakAmplitude(12, idx) = output7(idx).MaxValue;
    PeakAmpDiff(10, idx) = (output7(idx).MaxValue - output4(idx).MaxValue);
    PeakAmpDiff(11, idx) = (output4(idx).MaxValue - output1(idx).MaxValue);
    PeakAmpDiff(12, idx) = (output7(idx).MaxValue - output1(idx).MaxValue);
    
    PeakDelay(10, idx) = output1(idx).MaxIndexInSeconds;
    PeakDelay(11, idx) = output4(idx).MaxIndexInSeconds;
    PeakDelay(12, idx) = output7(idx).MaxIndexInSeconds;
    PeakDelDiff(10, idx) = (output7(idx).MaxIndexInSeconds - output4(idx).MaxIndexInSeconds);
    PeakDelDiff(11, idx) = (output4(idx).MaxIndexInSeconds - output1(idx).MaxIndexInSeconds);
    PeakDelDiff(12, idx) = (output7(idx).MaxIndexInSeconds - output1(idx).MaxIndexInSeconds);
    
    PeakEvAvg(10, idx) = output1(idx).PeakEventAvg;
    PeakEvAvg(11, idx) = output4(idx).PeakEventAvg;
    PeakEvAvg(12, idx) = output7(idx).PeakEventAvg;
    PeakEvAvgDiff(10, idx) = (output7(idx).PeakEventAvg - output4(idx).PeakEventAvg);
    PeakEvAvgDiff(11, idx) = (output4(idx).PeakEventAvg - output1(idx).PeakEventAvg);
    PeakEvAvgDiff(12, idx) = (output7(idx).PeakEventAvg - output1(idx).PeakEventAvg);
    
    TTPeakEvAvg(10, idx) = output1(idx).TimeToPeakEvAvg(1);
    TTPeakEvAvg(11, idx) = output4(idx).TimeToPeakEvAvg(1);
    TTPeakEvAvg(12, idx) = output7(idx).TimeToPeakEvAvg(1);
    TTPeakEvAvgDiff(10, idx) = (output7(idx).TimeToPeakEvAvg(1) - output4(idx).TimeToPeakEvAvg(1));
    TTPeakEvAvgDiff(11, idx) = (output4(idx).TimeToPeakEvAvg(1) - output1(idx).TimeToPeakEvAvg(1));
    TTPeakEvAvgDiff(12, idx) = (output7(idx).TimeToPeakEvAvg(1) - output1(idx).TimeToPeakEvAvg(1));
    
    PeakDispersion(10, idx) = output1(idx).FWHMInSeconds;
    PeakDispersion(11, idx) = output4(idx).FWHMInSeconds;
    PeakDispersion(12, idx) = output7(idx).FWHMInSeconds;
    PeakDispDiff(10, idx) = (output7(idx).FWHMInSeconds - output4(idx).FWHMInSeconds);
    PeakDispDiff(11, idx) = (output4(idx).FWHMInSeconds - output1(idx).FWHMInSeconds);
    PeakDispDiff(12, idx) = (output7(idx).FWHMInSeconds - output1(idx).FWHMInSeconds);
    
    PeakOnset(10, idx) = output1(idx).OnsetTimeInSeconds;
    PeakOnset(11, idx) = output4(idx).OnsetTimeInSeconds;
    PeakOnset(12, idx) = output7(idx).OnsetTimeInSeconds;
    PeakOnsDiff(10, idx) = (output7(idx).OnsetTimeInSeconds - output4(idx).OnsetTimeInSeconds);
    PeakOnsDiff(11, idx) = (output4(idx).OnsetTimeInSeconds - output1(idx).OnsetTimeInSeconds);
    PeakOnsDiff(12, idx) = (output7(idx).OnsetTimeInSeconds - output1(idx).OnsetTimeInSeconds);
    
    % Analysis for L1 to L3 at I2
    PeakAmplitude(13, idx) = output2(idx).MaxValue;
    PeakAmplitude(14, idx) = output5(idx).MaxValue;
    PeakAmplitude(15, idx) = output8(idx).MaxValue;
    PeakAmpDiff(13, idx) = (output8(idx).MaxValue - output5(idx).MaxValue);
    PeakAmpDiff(14, idx) = (output5(idx).MaxValue - output2(idx).MaxValue);
    PeakAmpDiff(15, idx) = (output8(idx).MaxValue - output2(idx).MaxValue);
    
    PeakDelay(13, idx) = output2(idx).MaxIndexInSeconds;
    PeakDelay(14, idx) = output5(idx).MaxIndexInSeconds;
    PeakDelay(15, idx) = output8(idx).MaxIndexInSeconds;
    PeakDelDiff(13, idx) = (output8(idx).MaxIndexInSeconds - output5(idx).MaxIndexInSeconds);
    PeakDelDiff(14, idx) = (output5(idx).MaxIndexInSeconds - output2(idx).MaxIndexInSeconds);
    PeakDelDiff(15, idx) = (output8(idx).MaxIndexInSeconds - output2(idx).MaxIndexInSeconds);
    
    PeakEvAvg(13, idx) = output2(idx).PeakEventAvg;
    PeakEvAvg(14, idx) = output5(idx).PeakEventAvg;
    PeakEvAvg(15, idx) = output8(idx).PeakEventAvg;
    PeakEvAvgDiff(13, idx) = (output8(idx).PeakEventAvg - output5(idx).PeakEventAvg);
    PeakEvAvgDiff(14, idx) = (output5(idx).PeakEventAvg - output2(idx).PeakEventAvg);
    PeakEvAvgDiff(15, idx) = (output8(idx).PeakEventAvg - output2(idx).PeakEventAvg);
    
    TTPeakEvAvg(13, idx) = output2(idx).TimeToPeakEvAvg(1);
    TTPeakEvAvg(14, idx) = output5(idx).TimeToPeakEvAvg(1);
    TTPeakEvAvg(15, idx) = output8(idx).TimeToPeakEvAvg(1);
    TTPeakEvAvgDiff(13, idx) = (output8(idx).TimeToPeakEvAvg(1) - output5(idx).TimeToPeakEvAvg(1));
    TTPeakEvAvgDiff(14, idx) = (output5(idx).TimeToPeakEvAvg(1) - output2(idx).TimeToPeakEvAvg(1));
    TTPeakEvAvgDiff(15, idx) = (output8(idx).TimeToPeakEvAvg(1) - output2(idx).TimeToPeakEvAvg(1));
    
    PeakDispersion(13, idx) = output2(idx).FWHMInSeconds;
    PeakDispersion(14, idx) = output5(idx).FWHMInSeconds;
    PeakDispersion(15, idx) = output8(idx).FWHMInSeconds;
    PeakDispDiff(13, idx) = (output8(idx).FWHMInSeconds - output5(idx).FWHMInSeconds);
    PeakDispDiff(14, idx) = (output5(idx).FWHMInSeconds - output2(idx).FWHMInSeconds);
    PeakDispDiff(15, idx) = (output8(idx).FWHMInSeconds - output2(idx).FWHMInSeconds);
    
    PeakOnset(13, idx) = output2(idx).OnsetTimeInSeconds;
    PeakOnset(14, idx) = output5(idx).OnsetTimeInSeconds;
    PeakOnset(15, idx) = output8(idx).OnsetTimeInSeconds;
    PeakOnsDiff(13, idx) = (output8(idx).OnsetTimeInSeconds - output5(idx).OnsetTimeInSeconds);
    PeakOnsDiff(14, idx) = (output5(idx).OnsetTimeInSeconds - output2(idx).OnsetTimeInSeconds);
    PeakOnsDiff(15, idx) = (output8(idx).OnsetTimeInSeconds - output2(idx).OnsetTimeInSeconds);
    
    % Analysis for L1 to L3 at I3
    PeakAmplitude(16, idx) = output3(idx).MaxValue;
    PeakAmplitude(17, idx) = output6(idx).MaxValue;
    PeakAmplitude(18, idx) = output9(idx).MaxValue;
    PeakAmpDiff(16, idx) = (output9(idx).MaxValue - output6(idx).MaxValue);
    PeakAmpDiff(17, idx) = (output6(idx).MaxValue - output3(idx).MaxValue);
    PeakAmpDiff(18, idx) = (output9(idx).MaxValue - output3(idx).MaxValue);
    
    PeakDelay(16, idx) = output3(idx).MaxIndexInSeconds;
    PeakDelay(17, idx) = output6(idx).MaxIndexInSeconds;
    PeakDelay(18, idx) = output9(idx).MaxIndexInSeconds;
    PeakDelDiff(16, idx) = (output9(idx).MaxIndexInSeconds - output6(idx).MaxIndexInSeconds);
    PeakDelDiff(17, idx) = (output6(idx).MaxIndexInSeconds - output3(idx).MaxIndexInSeconds);
    PeakDelDiff(18, idx) = (output9(idx).MaxIndexInSeconds - output3(idx).MaxIndexInSeconds);
    
    PeakEvAvg(16, idx) = output3(idx).PeakEventAvg;
    PeakEvAvg(17, idx) = output6(idx).PeakEventAvg;
    PeakEvAvg(18, idx) = output9(idx).PeakEventAvg;
    PeakEvAvgDiff(16, idx) = (output9(idx).PeakEventAvg - output6(idx).PeakEventAvg);
    PeakEvAvgDiff(17, idx) = (output6(idx).PeakEventAvg - output3(idx).PeakEventAvg);
    PeakEvAvgDiff(18, idx) = (output9(idx).PeakEventAvg - output3(idx).PeakEventAvg);
    
    TTPeakEvAvg(16, idx) = output3(idx).TimeToPeakEvAvg(1);
    TTPeakEvAvg(17, idx) = output6(idx).TimeToPeakEvAvg(1);
    TTPeakEvAvg(18, idx) = output9(idx).TimeToPeakEvAvg(1);
    TTPeakEvAvgDiff(16, idx) = (output9(idx).TimeToPeakEvAvg(1) - output6(idx).TimeToPeakEvAvg(1));
    TTPeakEvAvgDiff(17, idx) = (output6(idx).TimeToPeakEvAvg(1) - output3(idx).TimeToPeakEvAvg(1));
    TTPeakEvAvgDiff(18, idx) = (output9(idx).TimeToPeakEvAvg(1) - output3(idx).TimeToPeakEvAvg(1));
    
    PeakDispersion(16, idx) = output3(idx).FWHMInSeconds;
    PeakDispersion(17, idx) = output6(idx).FWHMInSeconds;
    PeakDispersion(18, idx) = output9(idx).FWHMInSeconds;
    PeakDispDiff(16, idx) = (output9(idx).FWHMInSeconds - output6(idx).FWHMInSeconds);
    PeakDispDiff(17, idx) = (output6(idx).FWHMInSeconds - output3(idx).FWHMInSeconds);
    PeakDispDiff(18, idx) = (output9(idx).FWHMInSeconds - output3(idx).FWHMInSeconds);
    
    PeakOnset(16, idx) = output3(idx).OnsetTimeInSeconds;
    PeakOnset(17, idx) = output6(idx).OnsetTimeInSeconds;
    PeakOnset(18, idx) = output9(idx).OnsetTimeInSeconds;
    PeakOnsDiff(16, idx) = (output9(idx).OnsetTimeInSeconds - output6(idx).OnsetTimeInSeconds);
    PeakOnsDiff(17, idx) = (output6(idx).OnsetTimeInSeconds - output3(idx).OnsetTimeInSeconds);
    PeakOnsDiff(18, idx) = (output9(idx).OnsetTimeInSeconds - output3(idx).OnsetTimeInSeconds);

end
% 
for subjs = 1:size(PeakDelay,2)
    for conds = 1:9
        CorrectedPeakAmplitude(conds,subjs) = [PeakAmplitude(conds,subjs) - nanmean(PeakAmplitude(1:9,subjs)) + nanmean(PeakAmplitude(1:9,:), 'all')];
        CorrectedPeakDelay(conds,subjs) = [PeakDelay(conds,subjs) - nanmean(PeakDelay(1:9,subjs)) + nanmean(PeakDelay(1:9,:), 'all')];
        CorrectedPeakEvAvg(conds,subjs) = [PeakEvAvg(conds,subjs) - nanmean(PeakEvAvg(1:9,subjs)) + nanmean(PeakEvAvg(1:9,:), 'all')];
        CorrectedTTPeakEvAvg(conds,subjs) = [TTPeakEvAvg(conds,subjs) - nanmean(TTPeakEvAvg(1:9,subjs)) + nanmean(TTPeakEvAvg(1:9,:), 'all')];
        CorrectedPeakDispersion(conds,subjs) = [PeakDispersion(conds,subjs) - nanmean(PeakDispersion(1:9,subjs)) + nanmean(PeakDispersion(1:9,:), 'all')];
        CorrectedPeakOnset(conds,subjs) = [PeakOnset(conds,subjs) - nanmean(PeakOnset(1:9,subjs)) + nanmean(PeakOnset(1:9,:), 'all')];
        
        CorrectedPeakAmpDiff(conds,subjs) = [PeakAmpDiff(conds,subjs) - nanmean(PeakAmpDiff(1:9,subjs)) + nanmean(PeakAmpDiff(1:9,:), 'all')];
        CorrectedPeakDelDiff(conds,subjs) = [PeakDelDiff(conds,subjs) - nanmean(PeakDelDiff(1:9,subjs)) + nanmean(PeakDelDiff(1:9,:), 'all')];
        CorrectedPeakEvAvgDiff(conds,subjs) = [PeakEvAvgDiff(conds,subjs) - nanmean(PeakEvAvgDiff(1:9,subjs)) + nanmean(PeakEvAvgDiff(1:9,:), 'all')];
        CorrectedTTPeakEvAvgDiff(conds,subjs) = [TTPeakEvAvgDiff(conds,subjs) - nanmean(TTPeakEvAvgDiff(1:9,subjs)) + nanmean(TTPeakEvAvgDiff(1:9,:), 'all')];
        CorrectedPeakDispDiff(conds,subjs) = [PeakDispDiff(conds,subjs) - nanmean(PeakDispDiff(1:9,subjs)) + nanmean(PeakDispDiff(1:9,:), 'all')];
        CorrectedPeakOnsDiff(conds,subjs) = [PeakOnsDiff(conds,subjs) - nanmean(PeakOnsDiff(1:9,subjs)) + nanmean(PeakOnsDiff(1:9,:), 'all')];
    end
    for conds = 10:18
        CorrectedPeakAmplitude(conds,subjs) = [PeakAmplitude(conds,subjs) - nanmean(PeakAmplitude(10:18,subjs)) + nanmean(PeakAmplitude(10:18,:), 'all')];
        CorrectedPeakDelay(conds,subjs) = [PeakDelay(conds,subjs) - nanmean(PeakDelay(10:18,subjs)) + nanmean(PeakDelay(10:18,:), 'all')];
        CorrectedPeakEvAvg(conds,subjs) = [PeakEvAvg(conds,subjs) - nanmean(PeakEvAvg(10:18,subjs)) + nanmean(PeakEvAvg(10:18,:), 'all')];
        CorrectedTTPeakEvAvg(conds,subjs) = [TTPeakEvAvg(conds,subjs) - nanmean(TTPeakEvAvg(10:18,subjs)) + nanmean(TTPeakEvAvg(10:18,:), 'all')];
        CorrectedPeakDispersion(conds,subjs) = [PeakDispersion(conds,subjs) - nanmean(PeakDispersion(10:18,subjs)) + nanmean(PeakDispersion(10:18,:), 'all')];
        CorrectedPeakOnset(conds,subjs) = [PeakOnset(conds,subjs) - nanmean(PeakOnset(10:18,subjs)) + nanmean(PeakOnset(10:18,:), 'all')];
        
        CorrectedPeakAmpDiff(conds,subjs) = [PeakAmpDiff(conds,subjs) - nanmean(PeakAmpDiff(10:18,subjs)) + nanmean(PeakAmpDiff(10:18,:), 'all')];
        CorrectedPeakDelDiff(conds,subjs) = [PeakDelDiff(conds,subjs) - nanmean(PeakDelDiff(10:18,subjs)) + nanmean(PeakDelDiff(10:18,:), 'all')];
        CorrectedPeakEvAvgDiff(conds,subjs) = [PeakEvAvgDiff(conds,subjs) - nanmean(PeakEvAvgDiff(10:18,subjs)) + nanmean(PeakEvAvgDiff(10:18,:), 'all')];
        CorrectedTTPeakEvAvgDiff(conds,subjs) = [TTPeakEvAvgDiff(conds,subjs) - nanmean(TTPeakEvAvgDiff(10:18,subjs)) + nanmean(TTPeakEvAvgDiff(10:18,:), 'all')];
        CorrectedPeakDispDiff(conds,subjs) = [PeakDispDiff(conds,subjs) - nanmean(PeakDispDiff(10:18,subjs)) + nanmean(PeakDispDiff(10:18,:), 'all')];
        CorrectedPeakOnsDiff(conds,subjs) = [PeakOnsDiff(conds,subjs) - nanmean(PeakOnsDiff(10:18,subjs)) + nanmean(PeakOnsDiff(10:18,:), 'all')];
    end
end

for condix = 1:size(PeakAmplitude, 1)
    hibootci(condix).amp = prctile(PeakAmplitude(condix, :), 97.5, 2);
    lobootci(condix).amp = prctile(PeakAmplitude(condix, :), 2.5, 2);
    hibootci(condix).ampdiff = prctile(PeakAmpDiff(condix, :), 97.5, 2);
    lobootci(condix).ampdiff = prctile(PeakAmpDiff(condix, :), 2.5, 2);
    hibootci(condix).del = prctile(PeakDelay(condix, :), 97.5, 2);
    lobootci(condix).del = prctile(PeakDelay(condix, :), 2.5, 2);
    hibootci(condix).deldiff = prctile(PeakDelDiff(condix, :), 97.5, 2);
    lobootci(condix).deldiff = prctile(PeakDelDiff(condix, :), 2.5, 2);
    hibootci(condix).pera = prctile(PeakEvAvg(condix, :), 97.5, 2);
    lobootci(condix).pera = prctile(PeakEvAvg(condix, :), 2.5, 2);
    hibootci(condix).ttp = prctile(TTPeakEvAvg(condix, :), 97.5, 2);
    lobootci(condix).ttp = prctile(TTPeakEvAvg(condix, :), 2.5, 2);
    hibootci(condix).peradiff = prctile(PeakEvAvgDiff(condix, :), 97.5, 2);
    lobootci(condix).peradiff = prctile(PeakEvAvgDiff(condix, :), 2.5, 2);
    hibootci(condix).ttpdiff = prctile(TTPeakEvAvgDiff(condix, :), 97.5, 2);
    lobootci(condix).ttpdiff = prctile(TTPeakEvAvgDiff(condix, :), 2.5, 2);
    hibootci(condix).pdp = prctile(PeakDispersion(condix, :), 97.5, 2);
    lobootci(condix).pdp = prctile(PeakDispersion(condix, :), 2.5, 2);
    hibootci(condix).pon = prctile(PeakOnset(condix, :), 97.5, 2);
    lobootci(condix).pon = prctile(PeakOnset(condix, :), 2.5, 2);
    hibootci(condix).pdpdiff = prctile(PeakDispDiff(condix, :), 97.5, 2);
    lobootci(condix).pdpdiff = prctile(PeakDispDiff(condix, :), 2.5, 2);
    hibootci(condix).pondiff = prctile(PeakOnsDiff(condix, :), 97.5, 2);
    lobootci(condix).pondiff = prctile(PeakOnsDiff(condix, :), 2.5, 2);
    
    hibootci(condix).camp = prctile(CorrectedPeakAmplitude(condix, :), 97.5, 2);
    lobootci(condix).camp = prctile(CorrectedPeakAmplitude(condix, :), 2.5, 2);
    hibootci(condix).cdel = prctile(CorrectedPeakDelay(condix, :), 97.5, 2);
    lobootci(condix).cdel = prctile(CorrectedPeakDelay(condix, :), 2.5, 2);
    hibootci(condix).cpera = prctile(CorrectedPeakEvAvg(condix, :), 97.5, 2);
    lobootci(condix).cpera = prctile(CorrectedPeakEvAvg(condix, :), 2.5, 2);
    hibootci(condix).cttp = prctile(CorrectedTTPeakEvAvg(condix, :), 97.5, 2);
    lobootci(condix).cttp = prctile(CorrectedTTPeakEvAvg(condix, :), 2.5, 2);
    hibootci(condix).cpdp = prctile(CorrectedPeakDispersion(condix, :), 97.5, 2);
    lobootci(condix).cpdp = prctile(CorrectedPeakDispersion(condix, :), 2.5, 2);
    hibootci(condix).cpon = prctile(CorrectedPeakOnset(condix, :), 97.5, 2);
    lobootci(condix).cpon = prctile(CorrectedPeakOnset(condix, :), 2.5, 2);
    
    hibootci(condix).campdiff = prctile(CorrectedPeakAmpDiff(condix, :), 97.5, 2);
    lobootci(condix).campdiff = prctile(CorrectedPeakAmpDiff(condix, :), 2.5, 2);
    hibootci(condix).cdeldiff = prctile(CorrectedPeakDelDiff(condix, :), 97.5, 2);
    lobootci(condix).cdeldiff = prctile(CorrectedPeakDelDiff(condix, :), 2.5, 2);
    hibootci(condix).cperadiff = prctile(CorrectedPeakEvAvgDiff(condix, :), 97.5, 2);
    lobootci(condix).cperadiff = prctile(CorrectedPeakEvAvgDiff(condix, :), 2.5, 2);
    hibootci(condix).cttpdiff = prctile(CorrectedTTPeakEvAvgDiff(condix, :), 97.5, 2);
    lobootci(condix).cttpdiff = prctile(CorrectedTTPeakEvAvgDiff(condix, :), 2.5, 2);
    hibootci(condix).cpdpdiff = prctile(CorrectedPeakDispDiff(condix, :), 97.5, 2);
    lobootci(condix).cpdpdiff = prctile(CorrectedPeakDispDiff(condix, :), 2.5, 2);
    hibootci(condix).cpondiff = prctile(CorrectedPeakOnsDiff(condix, :), 97.5, 2);
    lobootci(condix).cpondiff = prctile(CorrectedPeakOnsDiff(condix, :), 2.5, 2);
    
    ampmean(condix) = nanmean(PeakAmplitude(condix, :),2);
    ampstd(condix) = nanstd(PeakAmplitude(condix, :));
    campmean(condix) = nanmean(CorrectedPeakAmplitude(condix, :));
    campstd(condix) = nanstd(CorrectedPeakAmplitude(condix, :)) * 9/(9-1);
    campci{condix} = [campmean(condix) - (1.96 * campstd(condix) / sqrt(size(PeakAmplitude, 2))), campmean(condix) + (1.96 * campstd(condix) / sqrt(size(PeakAmplitude, 2)))];
    ampdiffmean(condix) = nanmean(PeakAmpDiff(condix, :),2);
    ampdiffstd(condix) = nanstd(PeakAmpDiff(condix, :));
    
    
    delmean(condix) = nanmean(PeakDelay(condix, :),2);
    delstd(condix) = nanstd(PeakDelay(condix, :));
    cdelmean(condix) = nanmean(CorrectedPeakDelay(condix, :));
    cdelstd(condix) = nanstd(CorrectedPeakDelay(condix, :)) * 9/(9-1);
    cdelci{condix} = [cdelmean(condix) - (1.96 * cdelstd(condix) / sqrt(size(PeakAmplitude, 2))), cdelmean(condix) + (1.96 * cdelstd(condix) / sqrt(size(PeakAmplitude, 2)))];
    deldiffmean(condix) = nanmean(PeakDelDiff(condix, :),2);
    deldiffstd(condix) = nanstd(PeakDelDiff(condix, :));
    
    peramean(condix) = nanmean(PeakEvAvg(condix,:),2);
    perastd(condix) = nanstd(PeakEvAvg(condix,:));
    cperamean(condix) = nanmean(CorrectedPeakEvAvg(condix, :));
    cperastd(condix) = nanstd(CorrectedPeakEvAvg(condix, :)) * 9/(9-1);
    cperaci{condix} = [cperamean(condix) - (1.96 * cperastd(condix) / sqrt(size(PeakAmplitude, 2))), cperamean(condix) + (1.96 * cperastd(condix) / sqrt(size(PeakAmplitude, 2)))];
    peradiffmean(condix) = nanmean(PeakEvAvgDiff(condix, :),2);
    peradiffstd(condix) = nanstd(PeakEvAvgDiff(condix, :));
    
    ttpmean(condix) = nanmean(TTPeakEvAvg(condix,:),2);
    ttpstd(condix) = nanstd(TTPeakEvAvg(condix,:));
    cttpmean(condix) = nanmean(CorrectedTTPeakEvAvg(condix, :));
    cttpstd(condix) = nanstd(CorrectedTTPeakEvAvg(condix, :)) * 9/(9-1);
    cttpci{condix} = [cttpmean(condix) - (1.96 * cttpstd(condix) / sqrt(size(PeakAmplitude, 2))), cttpmean(condix) + (1.96 * cttpstd(condix) / sqrt(size(PeakAmplitude, 2)))];
    ttpdiffmean(condix) = nanmean(TTPeakEvAvgDiff(condix, :),2);
    ttpdiffstd(condix) = nanstd(TTPeakEvAvgDiff(condix, :));

    pdpmean(condix) = nanmean(PeakDispersion(condix,:),2);
    pdpstd(condix) = nanstd(PeakDispersion(condix,:));
    cpdpmean(condix) = nanmean(CorrectedPeakDispersion(condix, :));
    cpdpstd(condix) = nanstd(CorrectedPeakDispersion(condix, :)) * 9/(9-1);
    cpdpci{condix} = [cpdpmean(condix) - (1.96 * cpdpstd(condix) / sqrt(size(PeakAmplitude, 2))), cpdpmean(condix) + (1.96 * cpdpstd(condix) / sqrt(size(PeakAmplitude, 2)))];
    pdpdiffmean(condix) = nanmean(PeakDispDiff(condix,:),2);
    pdpdiffstd(condix) = nanstd(PeakDispDiff(condix,:));
    
    ponmean(condix) = nanmean(PeakOnset(condix,:),2);
    ponstd(condix) = nanstd(PeakOnset(condix,:));
    cponmean(condix) = nanmean(CorrectedPeakOnset(condix, :));
    cponstd(condix) = nanstd(CorrectedPeakOnset(condix, :)) * 9/(9-1);
    cponci{condix} = [cponmean(condix) - (1.96 * cponstd(condix) / sqrt(size(PeakAmplitude, 2))), cponmean(condix) + (1.96 * cponstd(condix) / sqrt(size(PeakAmplitude, 2)))];
    pondiffmean(condix) = nanmean(PeakOnsDiff(condix,:),2);
    pondiffstd(condix) = nanstd(PeakOnsDiff(condix,:));
    
    ampeffect(condix) = ampdiffmean(condix)/ampdiffstd(condix);
    deleffect(condix) = deldiffmean(condix)/deldiffstd(condix);
    peraeffect(condix) = peradiffmean(condix)/peradiffstd(condix);
    ttpeffect(condix) = ttpdiffmean(condix)/ttpdiffstd(condix);
    pdpeffect(condix) = pdpdiffmean(condix)/pdpdiffstd(condix);
    poneffect(condix) = pondiffmean(condix)/pondiffstd(condix);
    
    campdiffmean(condix) = nanmean(CorrectedPeakAmpDiff(condix, :));
    campdiffstd(condix) = nanstd(CorrectedPeakAmpDiff(condix, :)) * 9/(9-1);
    campdiffci{condix} = [campdiffmean(condix) - (1.96 * campdiffstd(condix) / sqrt(size(PeakAmplitude, 2))), campdiffmean(condix) + (1.96 * campdiffstd(condix) / sqrt(size(PeakAmplitude, 2)))];
    cdeldiffmean(condix) = nanmean(CorrectedPeakDelDiff(condix, :));
    cdeldiffstd(condix) = nanstd(CorrectedPeakDelDiff(condix, :)) * 9/(9-1);
    cdeldiffci{condix} = [cdeldiffmean(condix) - (1.96 * cdeldiffstd(condix) / sqrt(size(PeakAmplitude, 2))), cdeldiffmean(condix) + (1.96 * cdeldiffstd(condix) / sqrt(size(PeakAmplitude, 2)))];
    cperadiffmean(condix) = nanmean(CorrectedPeakEvAvgDiff(condix, :));
    cperadiffstd(condix) = nanstd(CorrectedPeakEvAvgDiff(condix, :)) * 9/(9-1);
    cperadiffci{condix} = [cperadiffmean(condix) - (1.96 * cperadiffstd(condix) / sqrt(size(PeakAmplitude, 2))), cperadiffmean(condix) + (1.96 * cperadiffstd(condix) / sqrt(size(PeakAmplitude, 2)))];
    cttpdiffmean(condix) = nanmean(CorrectedTTPeakEvAvgDiff(condix, :));
    cttpdiffstd(condix) = nanstd(CorrectedTTPeakEvAvgDiff(condix, :)) * 9/(9-1);
    cttpdiffci{condix} = [cttpdiffmean(condix) - (1.96 * cttpdiffstd(condix) / sqrt(size(PeakAmplitude, 2))), cttpdiffmean(condix) + (1.96 * cttpdiffstd(condix) / sqrt(size(PeakAmplitude, 2)))];
    cpdpdiffmean(condix) = nanmean(CorrectedPeakDispDiff(condix, :));
    cpdpdiffstd(condix) = nanstd(CorrectedPeakDispDiff(condix, :)) * 9/(9-1);
    cpdpdiffci{condix} = [cpdpdiffmean(condix) - (1.96 * cpdpdiffstd(condix) / sqrt(size(PeakAmplitude, 2))), cpdpdiffmean(condix) + (1.96 * cpdpdiffstd(condix) / sqrt(size(PeakAmplitude, 2)))];
    cpondiffmean(condix) = nanmean(CorrectedPeakOnsDiff(condix, :));
    cpondiffstd(condix) = nanstd(CorrectedPeakOnsDiff(condix, :)) * 9/(9-1);
    cpondiffci{condix} = [cpondiffmean(condix) - (1.96 * cpondiffstd(condix) / sqrt(size(PeakAmplitude, 2))), cpondiffmean(condix) + (1.96 * cpondiffstd(condix) / sqrt(size(PeakAmplitude, 2)))];
    
    campeffect(condix) = campdiffmean(condix)/campdiffstd(condix);
    cdeleffect(condix) = cdeldiffmean(condix)/cdeldiffstd(condix);
    cperaeffect(condix) = cperadiffmean(condix)/cperadiffstd(condix);
    cttpeffect(condix) = cttpdiffmean(condix)/cttpdiffstd(condix);
    cpdpeffect(condix) = cpdpdiffmean(condix)/cpdpdiffstd(condix);
    cponeffect(condix) = cpondiffmean(condix)/cpondiffstd(condix);
    
end
% 
for cidx = 1:18
    ampci(cidx,:) = [lobootci(cidx).amp, hibootci(cidx).amp ];
    ampdiffci(cidx,:) = [lobootci(cidx).ampdiff, hibootci(cidx).ampdiff ];
    delci(cidx,:) = [lobootci(cidx).del, hibootci(cidx).del];
    deldiffci(cidx,:) = [lobootci(cidx).deldiff, hibootci(cidx).deldiff];
    peraci(cidx,:) = [lobootci(cidx).pera, hibootci(cidx).pera ];
    ttpci(cidx,:) = [lobootci(cidx).ttp, hibootci(cidx).ttp ];
    peradiffci(cidx,:) = [lobootci(cidx).peradiff, hibootci(cidx).peradiff ];
    ttpdiffci(cidx,:) = [lobootci(cidx).peradiff, hibootci(cidx).peradiff ];
    pdpci(cidx,:) = [lobootci(cidx).pdp, hibootci(cidx).pdp ];
    ponci(cidx,:) = [lobootci(cidx).pon, hibootci(cidx).pon ];
    pdpdiffci(cidx,:) = [lobootci(cidx).pdpdiff, hibootci(cidx).pdpdiff ];
    pondiffci(cidx,:) = [lobootci(cidx).pondiff, hibootci(cidx).pondiff ];
    
    acampci(cidx,:) = [lobootci(cidx).camp, hibootci(cidx).camp ];
    acdelci(cidx,:) = [lobootci(cidx).cdel, hibootci(cidx).cdel];
    acperaci(cidx,:) = [lobootci(cidx).cpera, hibootci(cidx).cpera ];
    acttpci(cidx,:) = [lobootci(cidx).cttp, hibootci(cidx).cttp ];
    acpdpci(cidx,:) = [lobootci(cidx).cpdp, hibootci(cidx).cpdp ];
    acponci(cidx,:) = [lobootci(cidx).cpon, hibootci(cidx).cpon ];
    
    acampdiffci(cidx,:) = [lobootci(cidx).campdiff, hibootci(cidx).campdiff ];
    acdeldiffci(cidx,:) = [lobootci(cidx).cdeldiff, hibootci(cidx).cdeldiff];
    acperadiffci(cidx,:) = [lobootci(cidx).cperadiff, hibootci(cidx).cperadiff ];
    acttpdiffci(cidx,:) = [lobootci(cidx).cperadiff, hibootci(cidx).cperadiff ];
    acpdpdiffci(cidx,:) = [lobootci(cidx).cpdpdiff, hibootci(cidx).cpdpdiff ];
    acpondiffci(cidx,:) = [lobootci(cidx).cpondiff, hibootci(cidx).cpondiff ];
    
end

varnames = [{'PeakAmplitude'}, {'PeakAmplitudeSD'}, {'PeakAmplitudeCI'}, {'PeakDelay'}, {'PeakDelaySD'}, {'PeakDelayCI'}, {'EvPeak'}, {'EvPeakSD'}, {'EvPeakCI'}, {'TimetoEvPeak'}, {'TimetoEvPeakSD'}, {'TimetoEvPeakCI'}, {'PeakDispersion'}, {'PeakDispersionSD'}, {'PeakDispersionCI'}, {'PeakOnset'}, {'PeakOnsetSD'}, {'PeakOnsetCI'}];
rownames = [{'L1I1'}, {'L1i2'}, {'L1i3'}, {'L2I1'}, {'L2I2'}, {'L2i3'}, {'L3I1'}, {'L3I2'}, {'L3I3'}, {'L1i1'}, {'L2i1'}, {'L3i1'}, {'L1I2'}, {'L2i2'}, {'L3i2'}, {'L1I3'}, {'L2I3'}, {'L3i3'}];
summaryVals = table(ampmean', ampstd', ampci, delmean', delstd', delci, peramean', perastd', peraci, ttpmean', ttpstd', ttpci, pdpmean', pdpstd', pdpci, ponmean', ponstd', ponci, 'VariableNames', varnames, 'RowNames', rownames);
writetable(summaryVals, 'BootSSummaryVals.csv');

varnames = [{'PeakAmplitude'}, {'PeakAmplitudeSD'}, {'PeakAmplitudeCI'}, {'PeakDelay'}, {'PeakDelaySD'}, {'PeakDelayCI'}, {'EvPeak'}, {'EvPeakSD'}, {'EvPeakCI'}, {'TimetoEvPeak'}, {'TimetoEvPeakSD'}, {'TimetoEvPeakCI'}, {'PeakDispersion'}, {'PeakDispersionSD'}, {'PeakDispersionCI'}, {'PeakOnset'}, {'PeakOnsetSD'}, {'PeakOnsetCI'}];
rownames = [{'L1I1'}, {'L1i2'}, {'L1i3'}, {'L2I1'}, {'L2I2'}, {'L2i3'}, {'L3I1'}, {'L3I2'}, {'L3I3'}, {'L1i1'}, {'L2i1'}, {'L3i1'}, {'L1I2'}, {'L2i2'}, {'L3i2'}, {'L1I3'}, {'L2I3'}, {'L3i3'}];
summaryVals = table(campmean', campstd', campci', cdelmean', cdelstd', cdelci', cperamean', cperastd', cperaci', cttpmean', cttpstd', cttpci', cpdpmean', cpdpstd', cpdpci', cponmean', cponstd', cponci', 'VariableNames', varnames, 'RowNames', rownames);
writetable(summaryVals, 'CorrectedBootSSummaryVals.csv');

varnames = [{'PeakAmplitude'}, {'PeakAmplitudeSD'}, {'PeakAmplitudeCI'}, {'PeakDelay'}, {'PeakDelaySD'}, {'PeakDelayCI'}, {'EvPeak'}, {'EvPeakSD'}, {'EvPeakCI'}, {'TimetoEvPeak'}, {'TimetoEvPeakSD'}, {'TimetoEvPeakCI'}, {'PeakDispersion'}, {'PeakDispersionSD'}, {'PeakDispersionCI'}, {'PeakOnset'}, {'PeakOnsetSD'}, {'PeakOnsetCI'}];
rownames = [{'L1I1'}, {'L1i2'}, {'L1i3'}, {'L2I1'}, {'L2I2'}, {'L2i3'}, {'L3I1'}, {'L3I2'}, {'L3I3'}, {'L1i1'}, {'L2i1'}, {'L3i1'}, {'L1I2'}, {'L2i2'}, {'L3i2'}, {'L1I3'}, {'L2I3'}, {'L3i3'}];
summaryVals = table(campmean', campstd', acampci, cdelmean', cdelstd', acdelci, cperamean', cperastd', acperaci, cttpmean', cttpstd', acttpci, cpdpmean', cpdpstd', acpdpci, cponmean', cponstd', acponci, 'VariableNames', varnames, 'RowNames', rownames);
writetable(summaryVals, 'CorrectedBootSSummaryValsAlt.csv');

varnamesd = [{'PeakAmpDiff'}, {'PeakAmpDiffSD'}, {'PeakAmpDiffCI'}, {'PeakAmpEffectSz'}, {'PeakDelDiff'}, {'PeakDelDiffSD'}, {'PeakDelDiffCI'}, {'PeakDelEffectSz'}, {'EvPeakDiff'}, {'EvPeakDiffSD'}, {'EvPeakDiffCI'}, {'EvPeakEffectSz'}, {'TimetoEvPeakDiff'}, {'TimetoEvPeakDiffSD'}, {'TimetoEvPeakDiffCI'}, {'TimetoEvPeakEffectSz'}, {'PeakDispDiff'}, {'PeakDispDiffSD'}, {'PeakDispDiffCI'}, {'PeakDispEffectSz'}, {'PeakOnsDiff'}, {'PeakOnsDiffSD'}, {'PeakOnsDiffCI'}, {'PeakOnsEffectSz'}];
rownamesd = [{'L1I3 - L1I2'}, {'L1I2 - L1I1'}, {'L1I3 - L1I1'}, {'L2I3 - L2I2'}, {'L2I2 - L2I1'}, {'L2I3 - L2I1'}, {'L3I3 - L3I2'}, {'L3I2 - L3I1'}, {'L3I3 - L3I1'}, {'L3I1 - L2I1'}, {'L2I1 - L1I1'}, {'L3I1 - L1I1'}, {'L3I2 - L2I2'}, {'L2I2 - L1I2'}, {'L3I2 - L1I2'}, {'L3I3 - L2I3'}, {'L2I3 - L1I3'}, {'L3I3 - L1I3'}];
summaryDiffs = table(ampdiffmean', ampdiffstd', ampdiffci, ampeffect', deldiffmean', deldiffstd', deldiffci, deleffect', peradiffmean', peradiffstd', peradiffci, peraeffect', ttpdiffmean', ttpdiffstd', ttpdiffci, ttpeffect', pdpdiffmean', pdpdiffstd', pdpdiffci, pdpeffect', pondiffmean', pondiffstd', pondiffci, poneffect', 'VariableNames', varnamesd, 'RowNames', rownamesd);
writetable(summaryDiffs, 'BootSSummaryDiffs.csv');

varnamesd = [{'PeakAmpDiff'}, {'PeakAmpDiffSD'}, {'PeakAmpDiffCI'}, {'PeakAmpEffectSz'}, {'PeakDelDiff'}, {'PeakDelDiffSD'}, {'PeakDelDiffCI'}, {'PeakDelEffectSz'}, {'EvPeakDiff'}, {'EvPeakDiffSD'}, {'EvPeakDiffCI'}, {'EvPeakEffectSz'}, {'TimetoEvPeakDiff'}, {'TimetoEvPeakDiffSD'}, {'TimetoEvPeakDiffCI'}, {'TimetoEvPeakEffectSz'}, {'PeakDispDiff'}, {'PeakDispDiffSD'}, {'PeakDispDiffCI'}, {'PeakDispEffectSz'}, {'PeakOnsDiff'}, {'PeakOnsDiffSD'}, {'PeakOnsDiffCI'}, {'PeakOnsEffectSz'}];
rownamesd = [{'L1I3 - L1I2'}, {'L1I2 - L1I1'}, {'L1I3 - L1I1'}, {'L2I3 - L2I2'}, {'L2I2 - L2I1'}, {'L2I3 - L2I1'}, {'L3I3 - L3I2'}, {'L3I2 - L3I1'}, {'L3I3 - L3I1'}, {'L3I1 - L2I1'}, {'L2I1 - L1I1'}, {'L3I1 - L1I1'}, {'L3I2 - L2I2'}, {'L2I2 - L1I2'}, {'L3I2 - L1I2'}, {'L3I3 - L2I3'}, {'L2I3 - L1I3'}, {'L3I3 - L1I3'}];
summaryDiffs = table(campdiffmean', campdiffstd', campdiffci', campeffect', cdeldiffmean', cdeldiffstd', cdeldiffci', cdeleffect', cperadiffmean', cperadiffstd', cperadiffci', cperaeffect', cttpdiffmean', cttpdiffstd', cttpdiffci', cttpeffect', cpdpdiffmean', cpdpdiffstd', cpdpdiffci', cpdpeffect', cpondiffmean', cpondiffstd', cpondiffci', cponeffect', 'VariableNames', varnamesd, 'RowNames', rownamesd);
writetable(summaryDiffs, 'CorrectedBootSSummaryDiffs.csv');

varnamesd = [{'PeakAmpDiff'}, {'PeakAmpDiffSD'}, {'PeakAmpDiffCI'}, {'PeakAmpEffectSz'}, {'PeakDelDiff'}, {'PeakDelDiffSD'}, {'PeakDelDiffCI'}, {'PeakDelEffectSz'}, {'EvPeakDiff'}, {'EvPeakDiffSD'}, {'EvPeakDiffCI'}, {'EvPeakEffectSz'}, {'TimetoEvPeakDiff'}, {'TimetoEvPeakDiffSD'}, {'TimetoEvPeakDiffCI'}, {'TimetoEvPeakEffectSz'}, {'PeakDispDiff'}, {'PeakDispDiffSD'}, {'PeakDispDiffCI'}, {'PeakDispEffectSz'}, {'PeakOnsDiff'}, {'PeakOnsDiffSD'}, {'PeakOnsDiffCI'}, {'PeakOnsEffectSz'}];
rownamesd = [{'L1I3 - L1I2'}, {'L1I2 - L1I1'}, {'L1I3 - L1I1'}, {'L2I3 - L2I2'}, {'L2I2 - L2I1'}, {'L2I3 - L2I1'}, {'L3I3 - L3I2'}, {'L3I2 - L3I1'}, {'L3I3 - L3I1'}, {'L3I1 - L2I1'}, {'L2I1 - L1I1'}, {'L3I1 - L1I1'}, {'L3I2 - L2I2'}, {'L2I2 - L1I2'}, {'L3I2 - L1I2'}, {'L3I3 - L2I3'}, {'L2I3 - L1I3'}, {'L3I3 - L1I3'}];
summaryDiffs = table(campdiffmean', campdiffstd', acampdiffci, campeffect', cdeldiffmean', cdeldiffstd', acdeldiffci, cdeleffect', cperadiffmean', cperadiffstd', acperadiffci, cperaeffect', cttpdiffmean', cttpdiffstd', acttpdiffci, cttpeffect', cpdpdiffmean', cpdpdiffstd', acpdpdiffci, cpdpeffect', cpondiffmean', cpondiffstd', acpondiffci, cponeffect', 'VariableNames', varnamesd, 'RowNames', rownamesd);
writetable(summaryDiffs, 'CorrectedBootSSummaryDiffsAlt.csv');