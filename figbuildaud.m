% Figure builder
clearvars();
 cd('/Volumes/Passport/TRoN/Analysis/PSubj2/out/aud');
erafiledir = dir('*ERAvgs*.mat');
fitfiledir = dir('*FittingMatrix.mat');
%pwd = cd('/Volumes/Passport/TRoN/Analysis/PSubj/balloon_vis');
%balfiledir = dir('*BalloonResult_l3_i3.mat');
cd(pwd)
lengths = [100 300 900];
intensities = [1 10 100];
        
for erafileidx = 1:numel(erafiledir)
    erafilename = sprintf('%s', [erafiledir(erafileidx).folder, '/', erafiledir(erafileidx).name]);
    eradata{erafileidx,1} = load(erafilename);
    fitfilename = sprintf('%s', [fitfiledir(erafileidx).folder, '/', fitfiledir(erafileidx).name]);
    fitdata{erafileidx,1} = load(fitfilename);
    
    %balfilename = sprintf('%s', [balfiledir(erafileidx).folder, '/', balfiledir(erafileidx).name]);
    %baldata{erafileidx,1} = load(balfilename);
end

grouphrfdata = load('LoadThis.mat');
grouperadata = load('LoadThis2.mat');

%% hrfs, non-boot
for l = 1:numel(lengths)
    for i = 1:numel(intensities)
        for s = 1:numel(erafiledir)
        %eravecs{l,i}(s,:) = eradata{s}.EventAvg{l,i};
        
       
        nbpmagdist{l,i}(s,:) = fitdata{s}.FittingMatrix{l,i}.MaxValue;
        nbpdeldist{l,i}(s,:) = fitdata{s}.FittingMatrix{l,i}.MaxIndexInSeconds;
        end
        
        hrfvecs{l,i}= grouphrfdata.FittingMatrix{l,i}.hrf;
        hrfvec2{l,i} = grouphrfdata.FittingMatrix{l,i}.fullhrf;
        eravecs{l,i} = grouperadata.EventAvg{l,i};
        smoothhrfvec2{l,i} = hrfvec2{l,i};
        samplingRateIncrease = 1000;
        newSamplePoints = linspace(0,15,15000);
        smoothhrfvec2{l,i} = spline(0:0.625:15, smoothhrfvec2{l,i}, newSamplePoints);
    end
end
%% hrfs, non-boot

ybounds = [-0.2 0.8];
subplot(2,3,1)
for l = 1
    for i = 1:numel(intensities)
        cols = ['r' 'g' 'b'];
        plot(0:0.625:15, eravecs{l,i}, cols(i))
        hold on
        TR = 0.625/1000;
        eventlength = 15*1000;
        t = 0:TR:(eventlength-1)*TR;
        t2 = 0:0.001:15-0.001;
        plot(t2, smoothhrfvec2{l,i}, cols(i),'LineWidth', 2)
        %plot(t, hrfvecs{l,i}, cols(i), 'LineWidth', 2)
        ylim(ybounds);
        yticks(ybounds(1):0.2:ybounds(2));
        xticks(0:2:16);
        ylabel('% signal change', 'fontsize', 13);
        title(sprintf('%s', [num2str(lengths(l)), 'ms']),'fontsize',15);
    end
end

subplot(2,3,2)
for l = 2
    for i = 1:numel(intensities)
        cols = ['r' 'g' 'b'];
        plot(0:0.625:15, eravecs{l,i}, cols(i))
        hold on
        TR = 0.625/1000;
        eventlength = 15*1000;
        t = 0:TR:(eventlength-1)*TR;
        t2 = 0:0.001:15-0.001;
        plot(t2, smoothhrfvec2{l,i}, cols(i),'LineWidth', 2)
        %plot(t,hrfvecs{l,i}, cols(i), 'LineWidth', 2)
        ylim(ybounds);
        yticks(ybounds(1):0.2:ybounds(2));
        xticks(0:2:16);
        title(sprintf('%s', [num2str(lengths(l)), 'ms']),'fontsize',15);
    end
end

subplot(2,3,3)
for l = 3
    for i = 1:numel(intensities)
        cols = ['r' 'g' 'b'];
        plot(0:0.625:15, eravecs{l,i}, cols(i),'HandleVisibility','off')
        hold on
        TR = 0.625/1000;
        eventlength = 15*1000;
        t = 0:TR:(eventlength-1)*TR;
        t2 = 0:0.001:15-0.001;
        plot(t2, smoothhrfvec2{l,i}, cols(i),'LineWidth', 2)
        %plot(t,hrfvecs{l,i}, cols(i), 'LineWidth', 2)
        ylim(ybounds);
        yticks(ybounds(1):0.2:ybounds(2));
        xticks(0:2:16);
        title(sprintf('%s', [num2str(lengths(l)), 'ms']),'fontsize',15);
        legend('1%', '10%', '100%','fontsize', 13);
    end
end

subplot(2,3,4)
for l = 1:numel(lengths)
    for i = 1
        cols = {[0.9 0.3 0.6],[0.4 0.9 0.9],[0.5 0.4 1]};
        plot(0:0.625:15, eravecs{l,i}, 'Color', cols{l})
        hold on
        TR = 0.625/1000;
        eventlength = 15*1000;
        t = 0:TR:(eventlength-1)*TR;
        t2 = 0:0.001:15-0.001;
        plot(t2, smoothhrfvec2{l,i}, 'Color', cols{l},'LineWidth', 2)
        %plot(t, hrfvecs{l,i}, 'Color', cols{l}, 'LineWidth', 2)
        ylim(ybounds);
        ylabel('% signal change','fontsize', 13);
        yticks(ybounds(1):0.2:ybounds(2));
        xticks(0:2:16);
        title(sprintf('%s', [num2str(intensities(i)), '% contrast']),'fontsize',15);
    end
end

subplot(2,3,5)
for l = 1:numel(lengths)
    for i = 2
        cols = {[0.9 0.3 0.6],[0.4 0.9 0.9],[0.5 0.4 1]};
        plot(0:0.625:15, eravecs{l,i}, 'Color', cols{l})
        hold on
        TR = 0.625/1000;
        eventlength = 15*1000;
        t = 0:TR:(eventlength-1)*TR;
        t2 = 0:0.001:15-0.001;
        plot(t2, smoothhrfvec2{l,i}, 'Color', cols{l},'LineWidth', 2)
        %plot(t,hrfvecs{l,i}, 'Color', cols{l}, 'LineWidth', 2)
        ylim(ybounds);
        yticks(ybounds(1):0.2:ybounds(2));
        xticks(0:2:16);
        xlabel('Time in seconds', 'fontsize', 13);
        title(sprintf('%s', [num2str(intensities(i)), '% contrast']),'fontsize',15);
    end
end

subplot(2,3,6)
for l = 1:numel(lengths)
    for i = 3
        cols = {[0.9 0.3 0.6],[0.4 0.9 0.9],[0.5 0.4 1]};
        plot(0:0.625:15, eravecs{l,i}, 'Color', cols{l}, 'HandleVisibility','off')
        hold on
        TR = 0.625/1000;
        eventlength = 15*1000;
        t = 0:TR:(eventlength-1)*TR;
        t2 = 0:0.001:15-0.001;
        plot(t2, smoothhrfvec2{l,i}, 'Color', cols{l},'LineWidth', 2)
        %plot(t,hrfvecs{l,i},'Color', cols{l}, 'LineWidth', 2)
        ylim(ybounds);
        yticks(ybounds(1):0.2:ybounds(2));
        xticks(0:2:16);
        title(sprintf('%s', [num2str(intensities(i)), '% contrast']),'fontsize',15);
        legend('100ms', '300ms', '900ms', 'fontsize', 13);
    end
end

% hrfs, boot

%% mean peak magnitude, boot vs non-boot
nbstats = csvread('CorrectedPSSummaryVals.csv',1,0);
pwd = cd('/Volumes/Passport/TRoN/Analysis/Boot10k2/aud');
bstats = csvread('CorrectedBootSSummaryValsAlt.csv',1,0);

%%
figure;
subplot(1,3,1)

xaxes = [100 300 900];
means = plot(xaxes, nbstats(10:12,1), 'Color', [0.3 0.3 0.3], 'MarkerFaceColor', [0.3 0.3 0.3], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [nbstats(9+i,3) nbstats(9+i,4)],'LineWidth', 5, 'Color', [0.7 0.7 0.7]);
end
%ci1 = plot(xaxes, nbstats(10:12,3), 'kx');
%ci2 = plot(xaxes, nbstats(10:12,4), 'kx');

means = plot(xaxes, bstats(10:12,1), '-o', 'Color', [0.9 0.3 0.6], 'LineWidth', 2, 'MarkerFaceColor', [0.9 0.3 0.6], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)],  [bstats(9+i,3) bstats(9+i,4)],':', 'LineWidth', 3, 'Color', [0.9 0.3 0.6]);
end
%ci1 = plot(xaxes, bstats(10:12,3), 'o', 'Color', [0.9 0.3 0.6], 'MarkerFaceColor', [0.9 0.3 0.6]);
%ci2 = plot(xaxes, bstats(10:12,4), 'o', 'Color', [0.9 0.3 0.6], 'MarkerFaceColor', [0.9 0.3 0.6]);

xlim([0 1000]);
xticks([0:200:1000]);
yticks([0:0.1:0.8]);
ylim([0 0.8]);
ylabel('Peak magnitude', 'fontsize', 13);

title(sprintf('%s', [num2str(1), '% contrast']),'fontsize',15);

subplot(1,3,2)
means = plot(xaxes, nbstats(13:15,1), 'Color', [0.3 0.3 0.3], 'MarkerFaceColor', [0.3 0.3 0.3], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [nbstats(12+i,3) nbstats(12+i,4)],'LineWidth', 5, 'Color', [0.7 0.7 0.7]);
end
%ci1 = plot(xaxes, nbstats(13:15,3), 'k.');
%ci2 = plot(xaxes, nbstats(13:15,4), 'k.');

means = plot(xaxes, bstats(13:15,1), '-o', 'Color', [0.4 0.9 0.9], 'LineWidth', 2, 'MarkerFaceColor', [0.4 0.9 0.9],  'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [bstats(12+i,3) bstats(12+i,4)], ':', 'LineWidth', 3, 'Color', [0.4 0.9 0.9]);
end
%ci1 = plot(xaxes, bstats(13:15,3), 'o', 'Color', [0.4 0.9 0.9],'MarkerFaceColor', [0.4 0.9 0.9]);
%ci2 = plot(xaxes, bstats(13:15,4), 'o', 'Color', [0.4 0.9 0.9],'MarkerFaceColor', [0.4 0.9 0.9]);
xlabel('Stimulus duration (ms)', 'fontsize', 13);
xlim([0 1000]);
xticks([0:200:1000]);

yticks([0:0.1:0.8]);
ylim([0 0.8]);
title(sprintf('%s', [num2str(10), '% contrast']),'fontsize',15);

subplot(1,3,3)
means = plot(xaxes, nbstats(16:18,1), 'Color', [0.3 0.3 0.3], 'MarkerFaceColor', [0.3 0.3 0.3], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [nbstats(15+i,3) nbstats(15+i,4)],'LineWidth', 5, 'Color', [0.7 0.7 0.7]);
end
%ci1 = plot(xaxes, nbstats(16:18,3), 'k.');
%ci2 = plot(xaxes, nbstats(16:18,4), 'k.');

means = plot(xaxes, bstats(16:18,1), '-o', 'Color', [0.5 0.4 1], 'LineWidth', 2, 'MarkerFaceColor', [0.5 0.4 1], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [bstats(15+i,3) bstats(15+i,4)], ':', 'LineWidth', 3, 'Color', [0.5 0.4 1]);
end
%ci1 = plot(xaxes, bstats(16:18,3), 'o', 'Color', [0.5 0.4 1], 'MarkerFaceColor', [0.5 0.4 1]);
%ci2 = plot(xaxes, bstats(16:18,4), 'o', 'Color', [0.5 0.4 1], 'MarkerFaceColor', [0.5 0.4 1]);
xticks([0:200:1000]);
xlim([0 1000]);

yticks([0:0.1:0.8]);
ylim([0 0.8]);
title(sprintf('%s', [num2str(100), '% contrast']),'fontsize',15);

hold off

%% mean peak latency vs mean resp latency, boot vs non-boot

figure;
subplot(2,3,1)

xaxes = [100 300 900];
means = plot(xaxes, nbstats(10:12,5), 'Color', [0.3 0.3 0.3], 'MarkerFaceColor', [0.3 0.3 0.3], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [nbstats(9+i,7) nbstats(9+i,8)],'LineWidth', 5, 'Color', [0.7 0.7 0.7]);
end
%ci1 = plot(xaxes, nbstats(10:12,7), 'k.');
%ci2 = plot(xaxes, nbstats(10:12,8), 'k.');

means = plot(xaxes, bstats(10:12,5), '-o', 'Color', [0.9 0.3 0.6], 'LineWidth', 2, 'MarkerFaceColor', [0.9 0.3 0.6], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [bstats(9+i,7) bstats(9+i,8)], ':', 'LineWidth', 3, 'Color', [0.9 0.3 0.6]);
end
%ci1 = plot(xaxes, bstats(10:12,7), 'o', 'Color', [0.9 0.3 0.6], 'MarkerFaceColor', [0.9 0.3 0.6]);
%ci2 = plot(xaxes, bstats(10:12,8), 'o', 'Color', [0.9 0.3 0.6], 'MarkerFaceColor', [0.9 0.3 0.6]);

xlim([0 1000]);
xticks([0:200:1000]);

yticks([2.8:0.4:5.6]);
ylim([2.8 5.6]);
ylabel('Peak latency (s)', 'fontsize', 13);
title(sprintf('%s', [num2str(1), '% contrast']),'fontsize',15);


subplot(2,3,2)

xaxes = [100 300 900];
means = plot(xaxes, nbstats(13:15,5), 'Color', [0.3 0.3 0.3], 'MarkerFaceColor', [0.3 0.3 0.3], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [nbstats(12+i,7) nbstats(12+i,8)],'LineWidth', 5, 'Color', [0.7 0.7 0.7]);
end
%ci1 = plot(xaxes, nbstats(13:15,7), 'k.');
%ci2 = plot(xaxes, nbstats(13:15,8), 'k.');

means = plot(xaxes, bstats(13:15,5), '-o', 'Color', [0.4 0.9 0.9], 'LineWidth', 2, 'MarkerFaceColor', [0.4 0.9 0.9], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [bstats(12+i,7) bstats(12+i,8)],  ':','LineWidth', 3, 'Color', [0.4 0.9 0.9]);
end
%ci1 = plot(xaxes, bstats(13:15,7), 'o', 'Color', [0.4 0.9 0.9], 'MarkerFaceColor', [0.4 0.9 0.9]);
%ci2 = plot(xaxes, bstats(13:15,8), 'o', 'Color', [0.4 0.9 0.9], 'MarkerFaceColor', [0.4 0.9 0.9]);

xlim([0 1000]);
xticks([0:200:1000]);

yticks([2.8:0.4:5.6]);
ylim([2.8 5.6]);
title(sprintf('%s', [num2str(10), '% contrast']),'fontsize',15);

subplot(2,3,3)

means = plot(xaxes, nbstats(16:18,5), 'Color', [0.3 0.3 0.3], 'MarkerFaceColor', [0.3 0.3 0.3], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [nbstats(15+i,7) nbstats(15+i,8)],'LineWidth', 5, 'Color', [0.7 0.7 0.7]);
end
%ci1 = plot(xaxes, nbstats(16:18,7), 'k.');
%ci2 = plot(xaxes, nbstats(16:18,8), 'k.');

means = plot(xaxes, bstats(16:18,5), '-o', 'Color', [0.5 0.4 1], 'LineWidth', 2, 'MarkerFaceColor', [0.5 0.4 1], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [bstats(15+i,7) bstats(15+i,8)],  ':','LineWidth', 3, 'Color', [0.5 0.4 1]);
end
%ci1 = plot(xaxes, bstats(16:18,7), 'o', 'Color', [0.5 0.4 1],'MarkerFaceColor', [0.5 0.4 1]);
%ci2 = plot(xaxes, bstats(16:18,8), 'o', 'Color', [0.5 0.4 1], 'MarkerFaceColor', [0.5 0.4 1]);
xticks([0:200:1000]);
xlim([0 1000]);

yticks([2.8:0.4:5.6]);
ylim([2.8 5.6]);
title(sprintf('%s', [num2str(100), '% contrast']),'fontsize',15);

subplot(2,3,4)

xaxes = [100 300 900];
means = plot(xaxes, nbstats(10:12,21), 'Color', [0.3 0.3 0.3], 'MarkerFaceColor', [0.3 0.3 0.3], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [nbstats(9+i,23) nbstats(9+i,24)],'LineWidth', 5, 'Color', [0.7 0.7 0.7]);
end
%ci1 = plot(xaxes, nbstats(10:12,23), 'k.');
%ci2 = plot(xaxes, nbstats(10:12,24), 'k.');

means = plot(xaxes, bstats(10:12,21), '-o', 'Color', [0.9 0.3 0.6], 'LineWidth', 2 , 'MarkerFaceColor', [0.9 0.3 0.6], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [bstats(9+i,23) bstats(9+i,24)], ':', 'LineWidth', 3, 'Color', [0.9 0.3 0.6]);
end
%ci1 = plot(xaxes, bstats(10:12,23), 'o',  'Color', [0.9 0.3 0.6], 'MarkerFaceColor', [0.9 0.3 0.6]);
%ci2 = plot(xaxes, bstats(10:12,24), 'o',  'Color', [0.9 0.3 0.6], 'MarkerFaceColor', [0.9 0.3 0.6]);

xlim([0 1000]);
xticks([0:200:1000]);

yticks([1.5:0.5:3.5]);
ylim([1 3.5]);
ylabel('Response latency (s)', 'fontsize', 13);

subplot(2,3,5)

xaxes = [100 300 900];
means = plot(xaxes, nbstats(13:15,21), 'Color', [0.3 0.3 0.3], 'MarkerFaceColor', [0.3 0.3 0.3], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [nbstats(12+i,23) nbstats(12+i,24)],'LineWidth', 5, 'Color', [0.7 0.7 0.7]);
end
%ci1 = plot(xaxes, nbstats(13:15,23), 'k.');
%ci2 = plot(xaxes, nbstats(13:15,24), 'k.');

means = plot(xaxes, bstats(13:15,21), '-o', 'Color', [0.4 0.9 0.9], 'LineWidth', 2, 'MarkerFaceColor', [0.4 0.9 0.9], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [bstats(12+i,23) bstats(12+i,24)], ':', 'LineWidth', 3, 'Color', [0.4 0.9 0.9]);
end
%ci1 = plot(xaxes, bstats(13:15,23), 'o', 'Color', [0.4 0.9 0.9], 'MarkerFaceColor', [0.4 0.9 0.9]);
%ci2 = plot(xaxes, bstats(13:15,24), 'o', 'Color', [0.4 0.9 0.9], 'MarkerFaceColor', [0.4 0.9 0.9]);

xlim([0 1000]);
xlabel('Stimulus duration (ms)', 'fontsize', 13);
xticks([0:200:1000]);

yticks([1.5:0.5:3.5]);
ylim([1 3.5]);


subplot(2,3,6)

means = plot(xaxes, nbstats(16:18,21), 'Color', [0.3 0.3 0.3], 'MarkerFaceColor', [0.3 0.3 0.3], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [nbstats(15+i,23) nbstats(15+i,24)],'LineWidth', 5, 'Color', [0.7 0.7 0.7]);
end
%ci1 = plot(xaxes, nbstats(16:18,23), 'k.');
%ci2 = plot(xaxes, nbstats(16:18,24), 'k.');

means = plot(xaxes, bstats(16:18,21), '-o', 'Color', [0.5 0.4 1], 'LineWidth', 2,'MarkerFaceColor', [0.5 0.4 1], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [bstats(15+i,23) bstats(15+i,24)], ':', 'LineWidth', 3, 'Color', [0.5 0.4 1]);
end
%ci1 = plot(xaxes, bstats(16:18,23), 'o',  'Color', [0.5 0.4 1],'MarkerFaceColor', [0.5 0.4 1]);
%ci2 = plot(xaxes, bstats(16:18,24), 'o',  'Color', [0.5 0.4 1],'MarkerFaceColor', [0.5 0.4 1]);
xticks([0:200:1000]);
xlim([0 1000]);

yticks([1.5:0.5:3.5]);
ylim([1 3.5]);

%% dispersion vs peak magnitude, boot and non-boot
figure;
subplot(3,1,1)
xaxes = [100 300 900];
plot(xaxes, nbstats(10:12,17), 'Color', [0.5 0.5 0.5], 'MarkerFaceColor', [0.5 0.5 0.5]);
hold on
for i = 1:numel(xaxes)
plot([xaxes(i) xaxes(i)], [nbstats(9+i,19) nbstats(9+i,20)], 'LineWidth', 5, 'Color', [0.7 0.7 0.7]);
end
%plot(xaxes, nbstats(10:12,19), 'k.');
%plot(xaxes, nbstats(10:12,20), 'k.');

plot(xaxes, bstats(10:12,17), '-o', 'Color', [0.9 0.3 0.6], 'LineWidth', 2, 'MarkerFaceColor', [0.9 0.3 0.6], 'MarkerSize', 7);
for i = 1:numel(xaxes)
plot([xaxes(i) xaxes(i)], [bstats(9+i,19) bstats(9+i,20)], ':', 'LineWidth', 3, 'Color', [0.9 0.3 0.6]);
end
%plot(xaxes, bstats(10:12,19), 'o', 'Color', [0.9 0.3 0.6], 'MarkerFaceColor', [0.9 0.3 0.6]);
%plot(xaxes, bstats(10:12,20), 'o', 'Color', [0.9 0.3 0.6], 'MarkerFaceColor', [0.9 0.3 0.6]);

xlim([0 1000]);
xticks([0:200:1000]);

yticks([1:0.5:5.5]);
ylim([1 5.5]);
ylabel('Peak dispersion (s)');

yyaxis right
plot(xaxes, bstats(10:12,1), ':', 'Color', [0.1 0.1 0.1], 'LineWidth', 2);
ylabel('Peak magnitude (%)', 'Color', 'k');
yax = gca;
yax.YColor = 'k';
ylim([0.1 0.5])
yticks([0.1:0.1:0.5])
title(sprintf('%s', [num2str(1), '% contrast']),'fontsize',12);

subplot(3,1,2)
xaxes = [100 300 900];
plot(xaxes, nbstats(13:15,17), 'Color', [0.5 0.5 0.5], 'MarkerFaceColor', [0.5 0.5 0.5], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
plot([xaxes(i) xaxes(i)], [nbstats(12+i,19) nbstats(12+i,20)], 'LineWidth', 5, 'Color', [0.7 0.7 0.7]);
end
%plot(xaxes, nbstats(13:15,19), 'k.');
%plot(xaxes, nbstats(13:15,20), 'k.');

plot(xaxes, bstats(13:15,17), '-o', 'Color', [0.4 0.9 0.9], 'LineWidth', 2, 'MarkerFaceColor', [0.4 0.9 0.9], 'MarkerSize', 7 );
for i = 1:numel(xaxes)
plot([xaxes(i) xaxes(i)], [bstats(12+i,19) bstats(12+i,20)], ':', 'LineWidth', 3, 'Color', [0.4 0.9 0.9]);
end
%plot(xaxes, bstats(13:15,19), 'o', 'Color', [0.4 0.9 0.9],  'MarkerFaceColor', [0.4 0.9 0.9]);
%plot(xaxes, bstats(13:15,20), 'o', 'Color', [0.4 0.9 0.9],  'MarkerFaceColor', [0.4 0.9 0.9]);

xlim([0 1000]);
xticks([0:200:1000]);

yticks([1:0.5:5.5]);
ylim([1 5.5]);
ylabel('Peak dispersion (s)');

yyaxis right
plot(xaxes, bstats(13:15,1), ':', 'Color', [0.1 0.1 0.1], 'LineWidth', 2);
ylabel('Peak magnitude (%)', 'Color', 'k');
yax = gca;
yax.YColor = 'k';

ylim([0.3 0.6])
yticks([0.3:0.1:0.6])
title(sprintf('%s', [num2str(10), '% contrast']),'fontsize',12);

subplot(3,1,3)
xaxes = [100 300 900];
plot(xaxes, nbstats(16:18,17), 'Color', [0.5 0.5 0.5], 'MarkerFaceColor', [0.5 0.5 0.5], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
plot([xaxes(i) xaxes(i)], [nbstats(15+i,19) nbstats(15+i,20)], 'LineWidth', 5, 'Color', [0.7 0.7 0.7]);
end
%plot(xaxes, nbstats(16:18,19), 'k.');
%plot(xaxes, nbstats(16:18,20), 'k.');

plot(xaxes, bstats(16:18,17), '-o', 'Color', [0.5 0.4 1], 'LineWidth', 2,'MarkerFaceColor', [0.5 0.4 1], 'MarkerSize', 7);
for i = 1:numel(xaxes)
plot([xaxes(i) xaxes(i)], [bstats(15+i,19) bstats(15+i,20)], ':', 'LineWidth', 3, 'Color', [0.5 0.4 1]);
end
%plot(xaxes, bstats(16:18,19), 'o', 'Color', [0.5 0.4 1], 'MarkerFaceColor', [0.5 0.4 1]);
%plot(xaxes, bstats(16:18,20), 'o', 'Color', [0.5 0.4 1], 'MarkerFaceColor', [0.5 0.4 1]);

xlim([0 1000]);
xticks([0:200:1000]);

yticks([1.5:0.5:5]);
ylim([1.5 5]);
ylabel('Peak dispersion (s)');

yyaxis right
plot(xaxes, bstats(16:18,1), ':', 'Color', [0.1 0.1 0.1], 'LineWidth', 2);
ylabel('Peak magnitude (%)', 'Color', 'k');
yax = gca;
yax.YColor = 'k';

ylim([0.4 0.7])
yticks([0.4:0.1:0.7])
title(sprintf('%s', [num2str(100), '% contrast']),'fontsize',12);

%%

% %% balloon hrfs
% figure;
% ybounds = [-0.4 1.2];
% %ybounds = [-0.4 0.8];
% 
% subplot(2,3,1)
% for l = 1
%     for i = 1:numel(intensities)
%         cols = ['r' 'g' 'b'];
%         plot(0:0.625:15, nanmean(eravecs{l,i}), cols(i))
%         hold on
%         TR = 0.625/1000;
%         eventlength = 15*1000;
%         t = 0:TR:(eventlength-1)*TR;
%         t2 = 0:0.001:15-0.001;
%         t3 = 0:0.01:15-0.01;
%         plot(t3, nanmean(balcurve{l,i}), cols(i),'LineWidth', 2)
%         %plot(t, nanmean(hrfvecs{l,i}), cols(i), 'LineWidth', 2)
%         ylim(ybounds);
%         yticks(ybounds(1):0.2:ybounds(2));
%         xticks(0:2:16);
%         ylabel('% signal change');
%         title(sprintf('%s', [num2str(lengths(l)), 'ms']),'fontsize',12);
%     end
% end
% 
% subplot(2,3,2)
% for l = 2
%     for i = 1:numel(intensities)
%         cols = ['r' 'g' 'b'];
%         plot(0:0.625:15, nanmean(eravecs{l,i}), cols(i))
%         hold on
%         TR = 0.625/1000;
%         eventlength = 15*1000;
%         t = 0:TR:(eventlength-1)*TR;
%         t2 = 0:0.001:15-0.001;
%         t3 = 0:0.01:15-0.01;
%         plot(t3, nanmean(balcurve{l,i}), cols(i),'LineWidth', 2)
%         %plot(t,nanmean(hrfvecs{l,i}), cols(i), 'LineWidth', 2)
%         ylim(ybounds);
%         yticks(ybounds(1):0.2:ybounds(2));
%         xticks(0:2:16);
%         title(sprintf('%s', [num2str(lengths(l)), 'ms']),'fontsize',12);
%     end
% end
% 
% subplot(2,3,3)
% for l = 3
%     for i = 1:numel(intensities)
%         cols = ['r' 'g' 'b'];
%         plot(0:0.625:15, nanmean(eravecs{l,i}), cols(i),'HandleVisibility','off')
%         hold on
%         TR = 0.625/1000;
%         eventlength = 15*1000;
%         t = 0:TR:(eventlength-1)*TR;
%         t2 = 0:0.001:15-0.001;
%         t3 = 0:0.01:15-0.01;
%         plot(t3, nanmean(balcurve{l,i}), cols(i),'LineWidth', 2)
%         %plot(t,nanmean(hrfvecs{l,i}), cols(i), 'LineWidth', 2)
%         ylim(ybounds);
%         yticks(ybounds(1):0.2:ybounds(2));
%         xticks(0:2:16);
%         title(sprintf('%s', [num2str(lengths(l)), 'ms']),'fontsize',12);
%         legend('1% contrast', '10% contrast', '100% contrast');
%     end
% end
% 
% subplot(2,3,4)
% for l = 1:numel(lengths)
%     for i = 1
%         cols = {[0.9 0.3 0.6],[0.4 0.9 0.9],[0.5 0.4 1]};
%         plot(0:0.625:15, nanmean(eravecs{l,i}), 'Color', cols{l})
%         hold on
%         TR = 0.625/1000;
%         eventlength = 15*1000;
%         t = 0:TR:(eventlength-1)*TR;
%         t2 = 0:0.001:15-0.001;
%         t3 = 0:0.01:15-0.01;
%         plot(t3, nanmean(balcurve{l,i}), 'Color', cols{l},'LineWidth', 2)
%         %plot(t, nanmean(hrfvecs{l,i}), 'Color', cols{l}, 'LineWidth', 2)
%         ylim(ybounds);
%         ylabel('% signal change');
%         yticks(ybounds(1):0.2:ybounds(2));
%         xticks(0:2:16);
%         title(sprintf('%s', [num2str(intensities(i)), '% contrast']),'fontsize',12);
%     end
% end
% 
% subplot(2,3,5)
% for l = 1:numel(lengths)
%     for i = 2
%         cols = {[0.9 0.3 0.6],[0.4 0.9 0.9],[0.5 0.4 1]};
%         plot(0:0.625:15, nanmean(eravecs{l,i}), 'Color', cols{l})
%         hold on
%         TR = 0.625/1000;
%         eventlength = 15*1000;
%         t = 0:TR:(eventlength-1)*TR;
%         t2 = 0:0.001:15-0.001;
%         t3 = 0:0.01:15-0.01;
%         plot(t3, nanmean(balcurve{l,i}), 'Color', cols{l},'LineWidth', 2)
%         %plot(t,nanmean(hrfvecs{l,i}), 'Color', cols{l}, 'LineWidth', 2)
%         ylim(ybounds);
%         yticks(ybounds(1):0.2:ybounds(2));
%         xticks(0:2:16);
%         xlabel('Time in seconds');
%         title(sprintf('%s', [num2str(intensities(i)), '% contrast']),'fontsize',12);
%     end
% end
% 
% subplot(2,3,6)
% for l = 1:numel(lengths)
%     for i = 3
%         cols = {[0.9 0.3 0.6],[0.4 0.9 0.9],[0.5 0.4 1]};
%         plot(0:0.625:15, nanmean(eravecs{l,i}), 'Color', cols{l}, 'HandleVisibility','off')
%         hold on
%         TR = 0.625/1000;
%         eventlength = 15*1000;
%         t = 0:TR:(eventlength-1)*TR;
%         t2 = 0:0.001:15-0.001;
%         t3 = 0:0.01:15-0.01;
%         plot(t3, nanmean(balcurve{l,i}), 'Color', cols{l},'LineWidth', 2)
%         %plot(t,nanmean(hrfvecs{l,i}),'Color', cols{l}, 'LineWidth', 2)
%         ylim(ybounds);
%         yticks(ybounds(1):0.2:ybounds(2));
%         xticks(0:2:16);
%         title(sprintf('%s', [num2str(intensities(i)), '% contrast']),'fontsize',12);
%         legend('100ms', '300ms', '900ms');
%     end
% end
%% boxplots
% figure;
% subplot(1,2,1)
% for a = 1:numel(nbpmagdist)
%     mags(:,a) = nbpmagdist{a};
% end
% boxplot(mags);
% hold on
% plot(1:9, nbstats(1:9,1), 'ro')
% 
% subplot(1,2,2)
% for b = 1:numel(output1)
% bmags(b,1) = output1(b).MaxValue;
% bmags(b,2) = output2(b).MaxValue;
% bmags(b,3) = output3(b).MaxValue;
% bmags(b,4) = output4(b).MaxValue;
% bmags(b,5) = output5(b).MaxValue;
% bmags(b,6) = output6(b).MaxValue;
% bmags(b,7) = output7(b).MaxValue;
% bmags(b,8) = output8(b).MaxValue;
% bmags(b,9) = output9(b).MaxValue;
% end
% boxplot(bmags);
% hold on
% plot(1:9, bstats(1:9,1), 'ro')
cd(pwd);