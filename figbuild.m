% Figure builder
clearvars();
 cd('/Volumes/Passport/TRoN/Analysis/PSubj2/out/vis');
erafiledir = dir('*ERAvgs*.mat');
fitfiledir = dir('*FittingMatrix.mat');
%pwd = cd('/Volumes/Passport/TRoN/Analysis/PSubj/balloon_vis');
%balfiledir = dir('*BalloonResult_l3_i3.mat');
cd(pwd)
lengths = [100 300 900];
intensities = [1 10 100];
        
for erafileidx = 1:numel(erafiledir)
    erafilename = sprintf('%s', [erafiledir(erafileidx).folder, '/', erafiledir(erafileidx).name]);
    eradata{erafileidx,1} = load(erafilename);
    fitfilename = sprintf('%s', [fitfiledir(erafileidx).folder, '/', fitfiledir(erafileidx).name]);
    fitdata{erafileidx,1} = load(fitfilename);
    
    %balfilename = sprintf('%s', [balfiledir(erafileidx).folder, '/', balfiledir(erafileidx).name]);
    %baldata{erafileidx,1} = load(balfilename);
end

grouphrfdata = load('LoadThis.mat');
grouperadata = load('LoadThis2.mat');

%% hrfs, non-boot
for l = 1:numel(lengths)
    for i = 1:numel(intensities)
        for s = 1:numel(erafiledir)
        %eravecs{l,i}(s,:) = eradata{s}.EventAvg{l,i};
        
       
        nbpmagdist{l,i}(s,:) = fitdata{s}.FittingMatrix{l,i}.MaxValue;
        nbpdeldist{l,i}(s,:) = fitdata{s}.FittingMatrix{l,i}.MaxIndexInSeconds;
        end
        
        hrfvecs{l,i}= grouphrfdata.FittingMatrix{l,i}.hrf;
        hrfvec2{l,i} = grouphrfdata.FittingMatrix{l,i}.fullhrf;
        eravecs{l,i} = grouperadata.EventAvg{l,i};
        smoothhrfvec2{l,i} = hrfvec2{l,i};
        samplingRateIncrease = 1000;
        newSamplePoints = linspace(0,15,15000);
        smoothhrfvec2{l,i} = spline(0:0.625:15, smoothhrfvec2{l,i}, newSamplePoints);
    end
end
%% hrfs, non-boot
ybounds = [-0.2 1.2];
%ybounds = [-0.2 0.8];
subplot(2,3,1)
for l = 1
    for i = 1:numel(intensities)
        cols = ['r' 'g' 'b'];
        plot(0:0.625:15, eravecs{l,i}, cols(i))
        hold on
        TR = 0.625/1000;
        eventlength = 15*1000;
        t = 0:TR:(eventlength-1)*TR;
        t2 = 0:0.001:15-0.001;
        plot(t2, smoothhrfvec2{l,i}, cols(i),'LineWidth', 2)
        %plot(t, hrfvecs{l,i}, cols(i), 'LineWidth', 2)
        ylim(ybounds);
        yticks(ybounds(1):0.2:ybounds(2));
        xticks(0:2:16);
        ylabel('% signal change', 'fontsize', 13);
        title(sprintf('%s', [num2str(lengths(l)), 'ms']),'fontsize',15);
    end
end

subplot(2,3,2)
for l = 2
    for i = 1:numel(intensities)
        cols = ['r' 'g' 'b'];
        plot(0:0.625:15, eravecs{l,i}, cols(i))
        hold on
        TR = 0.625/1000;
        eventlength = 15*1000;
        t = 0:TR:(eventlength-1)*TR;
        t2 = 0:0.001:15-0.001;
        plot(t2, smoothhrfvec2{l,i}, cols(i),'LineWidth', 2)
        %plot(t,hrfvecs{l,i}, cols(i), 'LineWidth', 2)
        ylim(ybounds);
        yticks(ybounds(1):0.2:ybounds(2));
        xticks(0:2:16);
        title(sprintf('%s', [num2str(lengths(l)), 'ms']),'fontsize',15);
    end
end

subplot(2,3,3)
for l = 3
    for i = 1:numel(intensities)
        cols = ['r' 'g' 'b'];
        plot(0:0.625:15, eravecs{l,i}, cols(i),'HandleVisibility','off')
        hold on
        TR = 0.625/1000;
        eventlength = 15*1000;
        t = 0:TR:(eventlength-1)*TR;
        t2 = 0:0.001:15-0.001;
        plot(t2, smoothhrfvec2{l,i}, cols(i),'LineWidth', 2)
        %plot(t,hrfvecs{l,i}, cols(i), 'LineWidth', 2)
        ylim(ybounds);
        yticks(ybounds(1):0.2:ybounds(2));
        xticks(0:2:16);
        title(sprintf('%s', [num2str(lengths(l)), 'ms']),'fontsize',15);
        legend('1%', '10%', '100%','fontsize', 13);
    end
end

subplot(2,3,4)
for l = 1:numel(lengths)
    for i = 1
        cols = {[0.9 0.3 0.6],[0.4 0.9 0.9],[0.5 0.4 1]};
        plot(0:0.625:15, eravecs{l,i}, 'Color', cols{l})
        hold on
        TR = 0.625/1000;
        eventlength = 15*1000;
        t = 0:TR:(eventlength-1)*TR;
        t2 = 0:0.001:15-0.001;
        plot(t2, smoothhrfvec2{l,i}, 'Color', cols{l},'LineWidth', 2)
        %plot(t, hrfvecs{l,i}, 'Color', cols{l}, 'LineWidth', 2)
        ylim(ybounds);
        ylabel('% signal change','fontsize', 13);
        yticks(ybounds(1):0.2:ybounds(2));
        xticks(0:2:16);
        title(sprintf('%s', [num2str(intensities(i)), '% contrast']),'fontsize',15);
    end
end

subplot(2,3,5)
for l = 1:numel(lengths)
    for i = 2
        cols = {[0.9 0.3 0.6],[0.4 0.9 0.9],[0.5 0.4 1]};
        plot(0:0.625:15, eravecs{l,i}, 'Color', cols{l})
        hold on
        TR = 0.625/1000;
        eventlength = 15*1000;
        t = 0:TR:(eventlength-1)*TR;
        t2 = 0:0.001:15-0.001;
        plot(t2, smoothhrfvec2{l,i}, 'Color', cols{l},'LineWidth', 2)
        %plot(t,hrfvecs{l,i}, 'Color', cols{l}, 'LineWidth', 2)
        ylim(ybounds);
        yticks(ybounds(1):0.2:ybounds(2));
        xticks(0:2:16);
        xlabel('Time in seconds', 'fontsize', 13);
        title(sprintf('%s', [num2str(intensities(i)), '% contrast']),'fontsize',15);
    end
end

subplot(2,3,6)
for l = 1:numel(lengths)
    for i = 3
        cols = {[0.9 0.3 0.6],[0.4 0.9 0.9],[0.5 0.4 1]};
        plot(0:0.625:15, eravecs{l,i}, 'Color', cols{l}, 'HandleVisibility','off')
        hold on
        TR = 0.625/1000;
        eventlength = 15*1000;
        t = 0:TR:(eventlength-1)*TR;
        t2 = 0:0.001:15-0.001;
        plot(t2, smoothhrfvec2{l,i}, 'Color', cols{l},'LineWidth', 2)
        %plot(t,hrfvecs{l,i},'Color', cols{l}, 'LineWidth', 2)
        ylim(ybounds);
        yticks(ybounds(1):0.2:ybounds(2));
        xticks(0:2:16);
        title(sprintf('%s', [num2str(intensities(i)), '% contrast']),'fontsize',15);
        legend('100ms', '300ms', '900ms', 'fontsize', 13);
    end
end

% hrfs, boot

%% mean peak magnitude, boot vs non-boot
nbstats = csvread('CorrectedPSSummaryVals.csv',1,0);
pwd = cd('/Volumes/Passport/TRoN/Analysis/Boot10k2/vis');
bstats = csvread('CorrectedBootSSummaryValsAlt.csv',1,0);

%%
figure;
subplot(1,3,1)

xaxes = [100 300 900];
means = plot(xaxes, nbstats(10:12,1), 'Color', [0.3 0.3 0.3], 'MarkerFaceColor', [0.3 0.3 0.3], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [nbstats(9+i,3) nbstats(9+i,4)],'LineWidth', 5, 'Color', [0.7 0.7 0.7]);
end
%ci1 = plot(xaxes, nbstats(10:12,3), 'kx');
%ci2 = plot(xaxes, nbstats(10:12,4), 'kx');

means = plot(xaxes, bstats(10:12,1), '-o', 'Color', [0.9 0.3 0.6], 'LineWidth', 2, 'MarkerFaceColor', [0.9 0.3 0.6], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)],  [bstats(9+i,3) bstats(9+i,4)],':', 'LineWidth', 3, 'Color', [0.9 0.3 0.6]);
end
%ci1 = plot(xaxes, bstats(10:12,3), 'o', 'Color', [0.9 0.3 0.6], 'MarkerFaceColor', [0.9 0.3 0.6]);
%ci2 = plot(xaxes, bstats(10:12,4), 'o', 'Color', [0.9 0.3 0.6], 'MarkerFaceColor', [0.9 0.3 0.6]);

xlim([0 1000]);
xticks([0:200:1000]);
yticks([0:0.2:1.5]);
ylim([0 1.5]);
% yticks([0:0.1:0.8]);
% ylim([0 0.8]);
ylabel('Peak magnitude', 'fontsize', 13);

title(sprintf('%s', [num2str(1), '% contrast']),'fontsize',15);

subplot(1,3,2)
means = plot(xaxes, nbstats(13:15,1), 'Color', [0.3 0.3 0.3], 'MarkerFaceColor', [0.3 0.3 0.3], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [nbstats(12+i,3) nbstats(12+i,4)],'LineWidth', 5, 'Color', [0.7 0.7 0.7]);
end
%ci1 = plot(xaxes, nbstats(13:15,3), 'k.');
%ci2 = plot(xaxes, nbstats(13:15,4), 'k.');

means = plot(xaxes, bstats(13:15,1), '-o', 'Color', [0.4 0.9 0.9], 'LineWidth', 2, 'MarkerFaceColor', [0.4 0.9 0.9],  'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [bstats(12+i,3) bstats(12+i,4)], ':', 'LineWidth', 3, 'Color', [0.4 0.9 0.9]);
end
%ci1 = plot(xaxes, bstats(13:15,3), 'o', 'Color', [0.4 0.9 0.9],'MarkerFaceColor', [0.4 0.9 0.9]);
%ci2 = plot(xaxes, bstats(13:15,4), 'o', 'Color', [0.4 0.9 0.9],'MarkerFaceColor', [0.4 0.9 0.9]);
xlabel('Stimulus duration (ms)', 'fontsize', 13);
xlim([0 1000]);
xticks([0:200:1000]);
ylim([0 1.5]);
yticks([0:0.2:1.5]);
% yticks([0:0.1:0.8]);
% ylim([0 0.8]);
title(sprintf('%s', [num2str(10), '% contrast']),'fontsize',15);

subplot(1,3,3)
means = plot(xaxes, nbstats(16:18,1), 'Color', [0.3 0.3 0.3], 'MarkerFaceColor', [0.3 0.3 0.3], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [nbstats(15+i,3) nbstats(15+i,4)],'LineWidth', 5, 'Color', [0.7 0.7 0.7]);
end
%ci1 = plot(xaxes, nbstats(16:18,3), 'k.');
%ci2 = plot(xaxes, nbstats(16:18,4), 'k.');

means = plot(xaxes, bstats(16:18,1), '-o', 'Color', [0.5 0.4 1], 'LineWidth', 2, 'MarkerFaceColor', [0.5 0.4 1], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [bstats(15+i,3) bstats(15+i,4)], ':', 'LineWidth', 3, 'Color', [0.5 0.4 1]);
end
%ci1 = plot(xaxes, bstats(16:18,3), 'o', 'Color', [0.5 0.4 1], 'MarkerFaceColor', [0.5 0.4 1]);
%ci2 = plot(xaxes, bstats(16:18,4), 'o', 'Color', [0.5 0.4 1], 'MarkerFaceColor', [0.5 0.4 1]);
xticks([0:200:1000]);
xlim([0 1000]);
yticks([0:0.2:1.5]);
ylim([0 1.5]);
% yticks([0:0.1:0.8]);
% ylim([0 0.8]);
title(sprintf('%s', [num2str(100), '% contrast']),'fontsize',15);

hold off

%% mean peak latency vs mean resp latency, boot vs non-boot

figure;
subplot(1,2,1)

xaxes = [100 300 900];
means = plot(xaxes, nbstats(10:12,5), 'Color', [0.3 0.3 0.3], 'MarkerFaceColor', [0.3 0.3 0.3], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [nbstats(9+i,7) nbstats(9+i,8)],'LineWidth', 5, 'Color', [0.7 0.7 0.7]);
end
%ci1 = plot(xaxes, nbstats(10:12,7), 'k.');
%ci2 = plot(xaxes, nbstats(10:12,8), 'k.');

means = plot(xaxes, bstats(10:12,5), '-o', 'Color', [0.9 0.3 0.6], 'LineWidth', 2, 'MarkerFaceColor', [0.9 0.3 0.6], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [bstats(9+i,7) bstats(9+i,8)], ':', 'LineWidth', 3, 'Color', [0.9 0.3 0.6]);
end
%ci1 = plot(xaxes, bstats(10:12,7), 'o', 'Color', [0.9 0.3 0.6], 'MarkerFaceColor', [0.9 0.3 0.6]);
%ci2 = plot(xaxes, bstats(10:12,8), 'o', 'Color', [0.9 0.3 0.6], 'MarkerFaceColor', [0.9 0.3 0.6]);

xlim([0 1000]);
xticks([0:200:1000]);
yticks([4:0.5:9.5]);
ylim([4 9.5]);
% yticks([3.5:0.5:5]);
% ylim([3.5 5.1]);
ylabel('Peak latency (s)', 'fontsize', 13);

sgtitle(sprintf('%s', [num2str(1), '% contrast']),'fontsize',15);

subplot(1,2,2)

xaxes = [100 300 900];
means = plot(xaxes, nbstats(10:12,21), 'Color', [0.3 0.3 0.3], 'MarkerFaceColor', [0.3 0.3 0.3], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [nbstats(9+i,23) nbstats(9+i,24)],'LineWidth', 5, 'Color', [0.7 0.7 0.7]);
end
%ci1 = plot(xaxes, nbstats(10:12,23), 'k.');
%ci2 = plot(xaxes, nbstats(10:12,24), 'k.');

means = plot(xaxes, bstats(10:12,21), '-o', 'Color', [0.9 0.3 0.6], 'LineWidth', 2 , 'MarkerFaceColor', [0.9 0.3 0.6], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [bstats(9+i,23) bstats(9+i,24)], ':', 'LineWidth', 3, 'Color', [0.9 0.3 0.6]);
end
%ci1 = plot(xaxes, bstats(10:12,23), 'o',  'Color', [0.9 0.3 0.6], 'MarkerFaceColor', [0.9 0.3 0.6]);
%ci2 = plot(xaxes, bstats(10:12,24), 'o',  'Color', [0.9 0.3 0.6], 'MarkerFaceColor', [0.9 0.3 0.6]);

xlim([0 1000]);
xticks([0:200:1000]);
yticks([1:0.5:4]);
ylim([1 4]);
% yticks([1.5:0.5:3.5]);
% ylim([1 3.5]);
ylabel('Response latency (s)', 'fontsize', 13);

figure;
subplot(2,2,1)

xaxes = [100 300 900];
means = plot(xaxes, nbstats(13:15,5), 'Color', [0.3 0.3 0.3], 'MarkerFaceColor', [0.3 0.3 0.3], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [nbstats(12+i,7) nbstats(12+i,8)],'LineWidth', 5, 'Color', [0.7 0.7 0.7]);
end
%ci1 = plot(xaxes, nbstats(13:15,7), 'k.');
%ci2 = plot(xaxes, nbstats(13:15,8), 'k.');

means = plot(xaxes, bstats(13:15,5), '-o', 'Color', [0.4 0.9 0.9], 'LineWidth', 2, 'MarkerFaceColor', [0.4 0.9 0.9], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [bstats(12+i,7) bstats(12+i,8)],  ':','LineWidth', 3, 'Color', [0.4 0.9 0.9]);
end
%ci1 = plot(xaxes, bstats(13:15,7), 'o', 'Color', [0.4 0.9 0.9], 'MarkerFaceColor', [0.4 0.9 0.9]);
%ci2 = plot(xaxes, bstats(13:15,8), 'o', 'Color', [0.4 0.9 0.9], 'MarkerFaceColor', [0.4 0.9 0.9]);

xlim([0 1000]);
xticks([0:200:1000]);
yticks([4:0.5:7]);
ylim([4 7]);
% yticks([3.5:0.5:5]);
% ylim([3.5 5.1]);
title(sprintf('%s', [num2str(10), '% contrast']),'fontsize',15);

subplot(2,2,2)

means = plot(xaxes, nbstats(16:18,5), 'Color', [0.3 0.3 0.3], 'MarkerFaceColor', [0.3 0.3 0.3], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [nbstats(15+i,7) nbstats(15+i,8)],'LineWidth', 5, 'Color', [0.7 0.7 0.7]);
end
%ci1 = plot(xaxes, nbstats(16:18,7), 'k.');
%ci2 = plot(xaxes, nbstats(16:18,8), 'k.');

means = plot(xaxes, bstats(16:18,5), '-o', 'Color', [0.5 0.4 1], 'LineWidth', 2, 'MarkerFaceColor', [0.5 0.4 1], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [bstats(15+i,7) bstats(15+i,8)],  ':','LineWidth', 3, 'Color', [0.5 0.4 1]);
end
%ci1 = plot(xaxes, bstats(16:18,7), 'o', 'Color', [0.5 0.4 1],'MarkerFaceColor', [0.5 0.4 1]);
%ci2 = plot(xaxes, bstats(16:18,8), 'o', 'Color', [0.5 0.4 1], 'MarkerFaceColor', [0.5 0.4 1]);
xticks([0:200:1000]);
xlim([0 1000]);
yticks([4:0.5:7]);
ylim([4 7]);
% yticks([3.5:0.5:5]);
% ylim([3.5 5.1]);
title(sprintf('%s', [num2str(100), '% contrast']),'fontsize',15);

subplot(2,2,3)

xaxes = [100 300 900];
means = plot(xaxes, nbstats(13:15,21), 'Color', [0.3 0.3 0.3], 'MarkerFaceColor', [0.3 0.3 0.3], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [nbstats(12+i,23) nbstats(12+i,24)],'LineWidth', 5, 'Color', [0.7 0.7 0.7]);
end
%ci1 = plot(xaxes, nbstats(13:15,23), 'k.');
%ci2 = plot(xaxes, nbstats(13:15,24), 'k.');

means = plot(xaxes, bstats(13:15,21), '-o', 'Color', [0.4 0.9 0.9], 'LineWidth', 2, 'MarkerFaceColor', [0.4 0.9 0.9], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [bstats(12+i,23) bstats(12+i,24)], ':', 'LineWidth', 3, 'Color', [0.4 0.9 0.9]);
end
%ci1 = plot(xaxes, bstats(13:15,23), 'o', 'Color', [0.4 0.9 0.9], 'MarkerFaceColor', [0.4 0.9 0.9]);
%ci2 = plot(xaxes, bstats(13:15,24), 'o', 'Color', [0.4 0.9 0.9], 'MarkerFaceColor', [0.4 0.9 0.9]);

xlim([0 1000]);
xlabel('Stimulus duration (ms)', 'fontsize', 13);
xticks([0:200:1000]);
yticks([1:0.5:4]);
ylim([1 4]);
% yticks([1.5:0.5:3.5]);
% ylim([1 3.5]);


subplot(2,2,4)

means = plot(xaxes, nbstats(16:18,21), 'Color', [0.3 0.3 0.3], 'MarkerFaceColor', [0.3 0.3 0.3], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [nbstats(15+i,23) nbstats(15+i,24)],'LineWidth', 5, 'Color', [0.7 0.7 0.7]);
end
%ci1 = plot(xaxes, nbstats(16:18,23), 'k.');
%ci2 = plot(xaxes, nbstats(16:18,24), 'k.');

means = plot(xaxes, bstats(16:18,21), '-o', 'Color', [0.5 0.4 1], 'LineWidth', 2,'MarkerFaceColor', [0.5 0.4 1], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [bstats(15+i,23) bstats(15+i,24)], ':', 'LineWidth', 3, 'Color', [0.5 0.4 1]);
end
%ci1 = plot(xaxes, bstats(16:18,23), 'o',  'Color', [0.5 0.4 1],'MarkerFaceColor', [0.5 0.4 1]);
%ci2 = plot(xaxes, bstats(16:18,24), 'o',  'Color', [0.5 0.4 1],'MarkerFaceColor', [0.5 0.4 1]);
xticks([0:200:1000]);
xlim([0 1000]);
yticks([1:0.5:4]);
ylim([1 4]);
% yticks([1.5:0.5:3.5]);
% ylim([1 3.5]);

%% dispersion vs peak magnitude, boot and non-boot
figure;
subplot(3,1,1)
xaxes = [100 300 900];
plot(xaxes, nbstats(10:12,17), 'Color', [0.5 0.5 0.5], 'MarkerFaceColor', [0.5 0.5 0.5]);
hold on
for i = 1:numel(xaxes)
plot([xaxes(i) xaxes(i)], [nbstats(9+i,19) nbstats(9+i,20)], 'LineWidth', 5, 'Color', [0.7 0.7 0.7]);
end
%plot(xaxes, nbstats(10:12,19), 'k.');
%plot(xaxes, nbstats(10:12,20), 'k.');

plot(xaxes, bstats(10:12,17), '-o', 'Color', [0.9 0.3 0.6], 'LineWidth', 2, 'MarkerFaceColor', [0.9 0.3 0.6], 'MarkerSize', 7);
for i = 1:numel(xaxes)
plot([xaxes(i) xaxes(i)], [bstats(9+i,19) bstats(9+i,20)], ':', 'LineWidth', 3, 'Color', [0.9 0.3 0.6]);
end
%plot(xaxes, bstats(10:12,19), 'o', 'Color', [0.9 0.3 0.6], 'MarkerFaceColor', [0.9 0.3 0.6]);
%plot(xaxes, bstats(10:12,20), 'o', 'Color', [0.9 0.3 0.6], 'MarkerFaceColor', [0.9 0.3 0.6]);

xlim([0 1000]);
xticks([0:200:1000]);
yticks([2:1:7]);
ylim([2 7]);
% yticks([1:0.5:5.5]);
% ylim([1 5.5]);
ylabel('Peak dispersion (s)');

yyaxis right
plot(xaxes, bstats(10:12,1), ':', 'Color', [0.1 0.1 0.1], 'LineWidth', 2);
ylabel('Peak magnitude (%)', 'Color', 'k');
yax = gca;
yax.YColor = 'k';
ylim([0.2 0.8])
yticks([0.2:0.1:0.8])
% ylim([0.1 0.5])
% yticks([0.1:0.1:0.5])
title(sprintf('%s', [num2str(1), '% contrast']),'fontsize',12);

subplot(3,1,2)
xaxes = [100 300 900];
plot(xaxes, nbstats(13:15,17), 'Color', [0.5 0.5 0.5], 'MarkerFaceColor', [0.5 0.5 0.5], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
plot([xaxes(i) xaxes(i)], [nbstats(12+i,19) nbstats(12+i,20)], 'LineWidth', 5, 'Color', [0.7 0.7 0.7]);
end
%plot(xaxes, nbstats(13:15,19), 'k.');
%plot(xaxes, nbstats(13:15,20), 'k.');

plot(xaxes, bstats(13:15,17), '-o', 'Color', [0.4 0.9 0.9], 'LineWidth', 2, 'MarkerFaceColor', [0.4 0.9 0.9], 'MarkerSize', 7 );
for i = 1:numel(xaxes)
plot([xaxes(i) xaxes(i)], [bstats(12+i,19) bstats(12+i,20)], ':', 'LineWidth', 3, 'Color', [0.4 0.9 0.9]);
end
%plot(xaxes, bstats(13:15,19), 'o', 'Color', [0.4 0.9 0.9],  'MarkerFaceColor', [0.4 0.9 0.9]);
%plot(xaxes, bstats(13:15,20), 'o', 'Color', [0.4 0.9 0.9],  'MarkerFaceColor', [0.4 0.9 0.9]);

xlim([0 1000]);
xticks([0:200:1000]);
yticks([2:1:7]);
ylim([2 7]);
% yticks([1:0.5:5.5]);
% ylim([1 5.5]);
ylabel('Peak dispersion (s)');

yyaxis right
plot(xaxes, bstats(13:15,1), ':', 'Color', [0.1 0.1 0.1], 'LineWidth', 2);
ylabel('Peak magnitude (%)', 'Color', 'k');
yax = gca;
yax.YColor = 'k';
ylim([0.4 1])
yticks([0.4:0.1:1])
% ylim([0.3 0.6])
% yticks([0.3:0.1:0.6])
title(sprintf('%s', [num2str(10), '% contrast']),'fontsize',12);

subplot(3,1,3)
xaxes = [100 300 900];
plot(xaxes, nbstats(16:18,17), 'Color', [0.5 0.5 0.5], 'MarkerFaceColor', [0.5 0.5 0.5], 'MarkerSize', 7);
hold on
for i = 1:numel(xaxes)
plot([xaxes(i) xaxes(i)], [nbstats(15+i,19) nbstats(15+i,20)], 'LineWidth', 5, 'Color', [0.7 0.7 0.7]);
end
%plot(xaxes, nbstats(16:18,19), 'k.');
%plot(xaxes, nbstats(16:18,20), 'k.');

plot(xaxes, bstats(16:18,17), '-o', 'Color', [0.5 0.4 1], 'LineWidth', 2,'MarkerFaceColor', [0.5 0.4 1], 'MarkerSize', 7);
for i = 1:numel(xaxes)
plot([xaxes(i) xaxes(i)], [bstats(15+i,19) bstats(15+i,20)], ':', 'LineWidth', 3, 'Color', [0.5 0.4 1]);
end
%plot(xaxes, bstats(16:18,19), 'o', 'Color', [0.5 0.4 1], 'MarkerFaceColor', [0.5 0.4 1]);
%plot(xaxes, bstats(16:18,20), 'o', 'Color', [0.5 0.4 1], 'MarkerFaceColor', [0.5 0.4 1]);

xlim([0 1000]);
xticks([0:200:1000]);
yticks([2:1:7]);
ylim([2 7]);
% yticks([1.5:0.5:5]);
% ylim([1.5 5]);
ylabel('Peak dispersion (s)');

yyaxis right
plot(xaxes, bstats(16:18,1), ':', 'Color', [0.1 0.1 0.1], 'LineWidth', 2);
ylabel('Peak magnitude (%)', 'Color', 'k');
yax = gca;
yax.YColor = 'k';
ylim([0.6 1.2])
yticks([0.6:0.1:1.2])
% ylim([0.4 0.7])
% yticks([0.4:0.1:0.7])
title(sprintf('%s', [num2str(100), '% contrast']),'fontsize',12);

%% summary figure
figure;

% Create standard impulse function (Friston)
imp_func = spm_hrf(0.001, [6 16 1 1 inf 0 32]);


subplot(4,5,6)
% Boxcars are defined in milliseconds. In this example, the first
% represents a stimulus of duration 1000 ms, amplitude 1. The second
% represents a stimulus of duration 100 ms, amplitude 10.
boxcar1 = [repmat(10,1,100) repmat(0,1,14900)];
boxcar2 = [repmat(10,1,300) repmat(0,1,14700)];
boxcar3 = [repmat(10,1,900) repmat(0,1,14100)];

hemo1 = conv(imp_func, boxcar1); %Perform the convolution of boxcar and standard impulse function
hemo2 = conv(imp_func, boxcar2);
hemo3 = conv(imp_func, boxcar3);
range = 1:15000; %Specify the timepoints of interest in hemo1 and hemo2 (in ms)
hemo1 = hemo1(range); %Trim the response to 24 seconds
hemo2 = hemo2(range);
hemo3 = hemo3(range);

%Plot the two hemodynamic responses
cols = {[0.9 0.3 0.6],[0.4 0.9 0.9],[0.5 0.4 1]};
plot(range/1000, hemo1, 'LineWidth', 2, 'Color', cols{1});
hold on;
plot(range/1000, hemo2, 'LineWidth', 2, 'Color', cols{2});
plot(range/1000, hemo3, 'LineWidth', 2, 'Color', cols{3});
yticks([0:0.4:2]);
ylim([-0.2 2]);
hold off

%Make everything look nicer
set(gca, 'fontunits', 'points', 'fontsize', 11);
set(gca, 'fontunits', 'points', 'fontsize', 11);
xlabel('Time (seconds) ');
ylabel('% signal change');


%Add a legend with information about the peak amplitude and response time
legend(['100ms'], ['300ms'], ['900ms']);


subplot(4,5,1)
% Boxcars are defined in milliseconds. In this example, the first
% represents a stimulus of duration 1000 ms, amplitude 1. The second
% represents a stimulus of duration 100 ms, amplitude 10.
boxcar4 = [repmat(1,1,900) repmat(0,1,14100)];
boxcar5 = [repmat(3,1,900) repmat(0,1,14100)];
boxcar6 = [repmat(10,1,900) repmat(0,1,14100)];

hemo4 = conv(imp_func, boxcar4); %Perform the convolution of boxcar and standard impulse function
hemo5 = conv(imp_func, boxcar5);
hemo6 = conv(imp_func, boxcar6);
range = 1:15000; %Specify the timepoints of interest in hemo1 and hemo2 (in ms)
hemo4 = hemo4(range); %Trim the response to 24 seconds
hemo5 = hemo5(range);
hemo6 = hemo6(range);

%Plot the two hemodynamic responses
plot(range/1000, hemo4, 'LineWidth', 2, 'Color', [1 0 0]);
hold on;
plot(range/1000, hemo5, 'LineWidth', 2, 'Color', [0 1 0]);
plot(range/1000, hemo6, 'LineWidth', 2, 'Color', [0 0 1]);
yticks([0:0.4:2]);
ylim([-0.2 2]);

%Calculate plot area parameters to plot the boxcars nicely
a = axis;
maxAmp = 10;%max(max(boxcar1)-min(boxcar1), max(boxcar2)-min(boxcar2));

% %And now plot those boxcars
% plot((1:length(boxcar1))/1000, (boxcar1/maxAmp)*(a(4)/2.5)-a(4)/2, 'LineWidth', 1, 'Color', [1 0 0]);
% plot((1:length(boxcar2))/1000, (boxcar2/maxAmp)*(a(4)/2.5)-a(4)/3, 'LineWidth', 1, 'Color', [0 1 0]);
% plot((1:length(boxcar3))/1000, (boxcar3/maxAmp)*(a(4)/2.5)-a(4)/6, 'LineWidth', 1, 'Color', [0 0 0]);

%Make everything look nicer
set(gca, 'fontunits', 'points', 'fontsize', 11);
set(gca, 'fontunits', 'points', 'fontsize', 11);
xlabel('Time (seconds) ');
ylabel('% signal change');

%Add a legend with information about the peak amplitude and response time
legend(['1%'], ['10%'], ['100%']);


%Get the amplitude and time of the peak from each hemodynamic response
[peak1_amp, peak1_time] = max(hemo1);
[peak2_amp, peak2_time] = max(hemo2);
[peak3_amp, peak3_time] = max(hemo3);

[peak4_amp, peak4_time] = max(hemo4);
[peak5_amp, peak5_time] = max(hemo5);
[peak6_amp, peak6_time] = max(hemo6);

subplot(4,5,7)

xaxes = [100 300 900];
amps = [peak1_amp, peak2_amp, peak3_amp];
plot(xaxes, amps, '-o', 'Color', [0 0 0], 'LineWidth', 2, 'MarkerFaceColor', [0 0 0]);
xticks([0:200:1000]);
xlim([0 1000]);
% yticks([0:0.2:1.5]);
% ylim([0 1.5]);
yticks([0:0.5:2]);
ylim([0 2]);
ylabel('Peak magnitude (%)');
xlabel('Stimulus duration (ms)');

subplot(4,5,8)
xaxes = [100 300 900];
times = [peak1_time, peak2_time, peak3_time] / 1000;
plot(xaxes, times, '-o', 'Color', [0 0 0], 'LineWidth', 2, 'MarkerFaceColor', [0 0 0]);
xticks([0:200:1000]);
xlim([0 1000]);
% yticks([0:0.2:1.5]);
% ylim([0 1.5]);
yticks([4:0.4:6.4]);
ylim([4 6.4]);
ylabel('Peak latency (s)');
xlabel('Stimulus duration (ms)');
title('Predicted effects of increasing stimulus durations at constant intensity', 'FontSize', 13);

subplot(4,5,2)

xaxes = [[log(0.01)/log(3)+1, log(0.1)/log(3)+1, log(1)/log(3)+1]];

amps = [peak4_amp, peak5_amp, peak6_amp];
plot(xaxes, amps, '-o', 'Color', [0 0 0], 'LineWidth', 2, 'MarkerFaceColor', [0 0 0]);

xlim([-3.5 1.3]);
xlabv = [{'1', '10', '100'}];
xticks(xaxes);
xticklabels(xlabv);

% yticks([0:0.2:1.5]);
% ylim([0 1.5]);
yticks([0:0.5:2]);
ylim([0 2]);
ylabel('Peak magnitude (%)');
xlabel('Stimulus intensity (%)');


subplot(4,5,3)
xaxes = [[log(0.01)/log(3)+1, log(0.1)/log(3)+1, log(1)/log(3)+1]];

times = [peak4_time, peak5_time, peak6_time] / 1000;
plot(xaxes, times, '-o', 'Color', [0 0 0], 'LineWidth', 2, 'MarkerFaceColor', [0 0 0]);

xlim([-3.5 1.3]);
xlabv = [{'1', '10', '100'}];
xticks(xaxes);
xticklabels(xlabv);

% yticks([0:0.2:1.5]);
% ylim([0 1.5]);
yticks([4:0.4:6.4]);
ylim([4 6.4]);
ylabel('Peak latency (s)');
xlabel('Stimulus intensity (%)');
title('Predicted effects of increasing stimulus intensities at constant duration', 'FontSize', 13);

hrfs = [hemo1 hemo2 hemo3 hemo4 hemo5 hemo6];

for j = 1:size(hrfs,2)
    
[MaxValue, MaxIndex] = max(hrfs(:,j));
HalfMax = (MaxValue-0)/2 + 0;
OnsetMax = (MaxValue-0)/10 + 0;
[MinValue, MinIndex] = min(hrfs(:,j));

for i = MaxIndex:-1:1
    HMPoint1 = i;
    if hrfs(i,j) < HalfMax
        break
    end
end

for i = MaxIndex:15000
    HMPoint2 = i;
    if hrfs(i,j) < HalfMax
        break
    end
end

for i = MaxIndex:-1:1
    RisePoint = i;
    if hrfs(i,j) < OnsetMax
        break
    end
end

FWHM(j) = HMPoint2 - HMPoint1;
Onset(j) = RisePoint;

end

subplot(4,5,9)

xaxes = [100 300 900];
plot(xaxes, FWHM(1:3)/1000, '-o', 'Color', [0 0 0], 'LineWidth', 2, 'MarkerFaceColor', [0 0 0]);
xticks([0:200:1000]);
xlim([0 1000]);
% yticks([0:0.2:1.5]);
% ylim([0 1.5]);
yticks([2:1:8]);
ylim([2 8]);
ylabel('Peak dispersion (s)');
xlabel('Stimulus duration (ms)');

subplot(4,5,10)
xaxes = [100 300 900];
plot(xaxes, Onset(1:3)/1000, '-o', 'Color', [0 0 0], 'LineWidth', 2, 'MarkerFaceColor', [0 0 0]);
xticks([0:200:1000]);
xlim([0 1000]);
% yticks([0:0.2:1.5]);
% ylim([0 1.5]);
yticks([1.5:0.2:2.5]);
ylim([1.5 2.5]);
ylabel('Response latency (s)');
xlabel('Stimulus duration (ms)');

subplot(4,5,4)

xaxes = [[log(0.01)/log(3)+1, log(0.1)/log(3)+1, log(1)/log(3)+1]];

plot(xaxes, FWHM(4:6)/1000, '-o', 'Color', [0 0 0], 'LineWidth', 2, 'MarkerFaceColor', [0 0 0]);

xlim([-3.5 1.3]);
xlabv = [{'1', '10', '100'}];
xticks(xaxes);
xticklabels(xlabv);
% yticks([0:0.2:1.5]);
% ylim([0 1.5]);
yticks([2:1:8]);
ylim([2 8]);
ylabel('Peak dispersion (s)');
xlabel('Stimulus intensity (%)');

subplot(4,5,5)

xaxes = [[log(0.01)/log(3)+1, log(0.1)/log(3)+1, log(1)/log(3)+1]];
plot(xaxes, Onset(4:6)/1000, '-o', 'Color', [0 0 0], 'LineWidth', 2, 'MarkerFaceColor', [0 0 0]);

% yticks([0:0.2:1.5]);
% ylim([0 1.5]);
xlim([-3.5 1.3]);
xlabv = [{'1', '10', '100'}];
xticks(xaxes);
xticklabels(xlabv);

yticks([1.5:0.2:2.5]);
ylim([1.5 2.5]);
ylabel('Response latency (s)');
xlabel('Stimulus intensity (%)');

subplot(4,5,11)

for l = 3
    for i = 1:numel(intensities)
        cols = ['r' 'g' 'b'];
        plot(0:0.625:15, eravecs{l,i}, cols(i),'HandleVisibility','off')
        hold on
        TR = 0.625/1000;
        eventlength = 15*1000;
        t = 0:TR:(eventlength-1)*TR;
        t2 = 0:0.001:15-0.001;
        plot(t2, smoothhrfvec2{l,i}, cols(i),'LineWidth', 2)
        %plot(t,hrfvecs{l,i}, cols(i), 'LineWidth', 2)
        ylim([-0.2 1.6]);
        yticks(0:0.4:1.6);
        xticks(0:2:16);
       
        legend('1%', '10%', '100%','fontsize', 10);
        xlabel('Time (seconds) ');
        ylabel('% signal change');
    end
end

subplot(4,5,16)

for l = 1:numel(lengths)
    for i = 3
        cols = {[0.9 0.3 0.6],[0.4 0.9 0.9],[0.5 0.4 1]};
        plot(0:0.625:15, eravecs{l,i}, 'Color', cols{l}, 'HandleVisibility','off')
        hold on
        TR = 0.625/1000;
        eventlength = 15*1000;
        t = 0:TR:(eventlength-1)*TR;
        t2 = 0:0.001:15-0.001;
        plot(t2, smoothhrfvec2{l,i}, 'Color', cols{l},'LineWidth', 2)
        %plot(t,hrfvecs{l,i},'Color', cols{l}, 'LineWidth', 2)
        ylim([-0.2 1.6]);
        yticks(0:0.4:1.6);
        xticks(0:2:16);
       
        legend('100ms', '300ms', '900ms', 'fontsize', 10);
        xlabel('Time (seconds) ');
        ylabel('% signal change');
    end
end

subplot(4,5,12)

xaxes = [[log(0.01)/log(3)+1, log(0.1)/log(3)+1, log(1)/log(3)+1]];
% means = plot(xaxes, nbstats(7:9,1), 'Color', [0.3 0.3 0.3], 'MarkerFaceColor', [0.3 0.3 0.3], 'MarkerSize', 7);
% hold on
% for i = 1:numel(xaxes)
% bars = plot([xaxes(i) xaxes(i)], [nbstats(6+i,3) nbstats(6+i,4)],'LineWidth', 4, 'Color', [0.5 0.5 0.5]);
% end
% %ci1 = plot(xaxes, nbstats(16:18,3), 'k.');
%ci2 = plot(xaxes, nbstats(16:18,4), 'k.');
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [bstats(6+i,3) bstats(6+i,4)], 'LineWidth', 2, 'Color', [0.5 0.5 0.5]);
hold on
end
hold on
means = plot(xaxes, bstats(7:9,1), '-o', 'Color', [0 0 0], 'LineWidth', 2, 'MarkerFaceColor', [0 0 0]);

%ci1 = plot(xaxes, bstats(7:9,3), 'o', 'Color', [0 0 0], 'MarkerFaceColor', [0 0 0]);
%ci2 = plot(xaxes, bstats(7:9,4), 'o', 'Color', [0 0 0], 'MarkerFaceColor', [0 0 0]);
xlim([-3.5 1.3]);
xlabv = [{'1', '10', '100'}];
xticks(xaxes);
xticklabels(xlabv);

% yticks([0:0.2:1.5]);
% ylim([0 1.5]);
yticks([0:0.5:2]);
ylim([0 2]);
ylabel('Peak magnitude (%)');
xlabel('Stimulus intensity (%)');


hold off

subplot(4,5,13)
xaxes = [[log(0.01)/log(3)+1, log(0.1)/log(3)+1, log(1)/log(3)+1]];
% means = plot(xaxes, nbstats(7:9,5), 'Color', [0.3 0.3 0.3], 'MarkerFaceColor', [0.3 0.3 0.3], 'MarkerSize', 7);
% hold on
% for i = 1:numel(xaxes)
% bars = plot([xaxes(i) xaxes(i)], [nbstats(6+i,7) nbstats(6+i,8)],'LineWidth', 4, 'Color', [0.5 0.5 0.5]);
% end
%ci1 = plot(xaxes, nbstats(16:18,7), 'k.');
%ci2 = plot(xaxes, nbstats(16:18,8), 'k.');
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [bstats(6+i,7) bstats(6+i,8)], 'LineWidth', 2, 'Color', [0.5 0.5 0.5]);
hold on
end
hold on
means = plot(xaxes, bstats(7:9,5), '-o', 'Color', [0 0 0], 'LineWidth', 2, 'MarkerFaceColor', [0 0 0]);


%ci1 = plot(xaxes, bstats(7:9,7), 'o', 'Color', [0 0 0],'MarkerFaceColor', [0 0 0]);
%ci2 = plot(xaxes, bstats(7:9,8), 'o', 'Color', [0 0 0], 'MarkerFaceColor', [0 0 0]);
xlim([-3.5 1.3]);
xlabv = [{'1', '10', '100'}];
xticks(xaxes);
xticklabels(xlabv);

% yticks([0:0.2:1.5]);
% ylim([0 1.5]);
yticks([4:0.4:6.4]);
ylim([4 6.4]);
ylabel('Peak latency (s)');
xlabel('Stimulus intensity (%)');
title('Observed effects of increasing stimulus intensities at maximum stimulus duration', 'FontSize', 13);

subplot(4,5,14)

xaxes = [[log(0.01)/log(3)+1, log(0.1)/log(3)+1, log(1)/log(3)+1]];
% plot(xaxes, nbstats(7:9,17), 'Color', [0.5 0.5 0.5], 'MarkerFaceColor', [0.5 0.5 0.5], 'MarkerSize', 7);
% hold on
% for i = 1:numel(xaxes)
% plot([xaxes(i) xaxes(i)], [nbstats(6+i,19) nbstats(6+i,20)], 'LineWidth', 4, 'Color', [0.5 0.5 0.5]);
% end
%plot(xaxes, nbstats(16:18,19), 'k.');
%plot(xaxes, nbstats(16:18,20), 'k.');
for i = 1:numel(xaxes)
plot([xaxes(i) xaxes(i)], [bstats(6+i,19) bstats(6+i,20)], 'LineWidth', 2, 'Color', [0.5 0.5 0.5]);
hold on
end
hold on
plot(xaxes, bstats(7:9,17), '-o','Color', [0 0 0], 'LineWidth', 2, 'MarkerFaceColor', [0 0 0]);


%plot(xaxes, bstats(7:9,19), 'o', 'Color', [0 0 0], 'MarkerFaceColor', [0 0 0]);
%plot(xaxes, bstats(7:9,20), 'o', 'Color', [0 0 0], 'MarkerFaceColor', [0 0 0]);


xlim([-3.5 1.3]);
xlabv = [{'1', '10', '100'}];
xticks(xaxes);
xticklabels(xlabv);
% yticks([0:0.2:1.5]);
% ylim([0 1.5]);
yticks([1:1:7]);
ylim([1 7]);
ylabel('Peak dispersion (s)');
xlabel('Stimulus intensity (%)');

subplot(4,5,15)

xaxes = [[log(0.01)/log(3)+1, log(0.1)/log(3)+1, log(1)/log(3)+1]];
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [bstats(6+i,23) bstats(6+i,24)], 'LineWidth', 2, 'Color', [0.5 0.5 0.5]);
hold on
end
hold on
means = plot(xaxes, bstats(7:9,21), '-o','Color', [0 0 0], 'LineWidth', 2, 'MarkerFaceColor', [0 0 0]);


%ci1 = plot(xaxes, bstats(7:9,23), 'o',  'Color', [0 0 0],'MarkerFaceColor', [0 0 0]);
%ci2 = plot(xaxes, bstats(7:9,24), 'o',  'Color', [0 0 0],'MarkerFaceColor', [0 0 0]);

xlim([-3.5 1.3]);
xlabv = [{'1', '10', '100'}];
xticks(xaxes);
xticklabels(xlabv);

yticks([2.2:0.2:3.2]);
ylim([2.2 3.2]);
ylabel('Response latency (s)');
xlabel('Stimulus intensity (%)');

subplot(4,5,17)

xaxes = [100 300 900];
for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [bstats(15+i,3) bstats(15+i,4)], 'LineWidth', 2, 'Color', [0.5 0.5 0.5]);
hold on
end
hold on
means = plot(xaxes, bstats(16:18,1), '-o','Color', [0 0 0], 'LineWidth', 2, 'MarkerFaceColor', [0 0 0]);


%ci1 = plot(xaxes, bstats(16:18,3), 'o', 'Color', [0 0 0], 'MarkerFaceColor', [0 0 0]);
%ci2 = plot(xaxes, bstats(16:18,4), 'o', 'Color', [0 0 0], 'MarkerFaceColor', [0 0 0]);
xticks([0:200:1000]);
xlim([0 1000]);
yticks([0:0.5:2]);
ylim([0 2]);
ylabel('Peak magnitude (%)');
xlabel('Stimulus duration (ms)');

subplot(4,5,18)

xaxes = [100 300 900];

for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [bstats(15+i,7) bstats(15+i,8)], 'LineWidth', 2, 'Color', [0.5 0.5 0.5]);
hold on
end
hold on
means = plot(xaxes, bstats(16:18,5), '-o','Color', [0 0 0], 'LineWidth', 2, 'MarkerFaceColor', [0 0 0]);

%ci1 = plot(xaxes, bstats(16:18,7), 'o', 'Color', [0 0 0],'MarkerFaceColor', [0 0 0]);
%ci2 = plot(xaxes, bstats(16:18,8), 'o', 'Color', [0 0 0], 'MarkerFaceColor', [0 0 0]);
xticks([0:200:1000]);
xlim([0 1000]);
yticks([4:0.4:6.4]);
ylim([4 6.4]);
ylabel('Peak latency (s)');
xlabel('Stimulus duration (ms)');
title('Observed effects of increasing stimulus durations at maximum stimulus intensity', 'FontSize', 13);

subplot(4,5,19)

xaxes = [100 300 900];
for i = 1:numel(xaxes)
plot([xaxes(i) xaxes(i)], [bstats(15+i,19) bstats(15+i,20)], 'LineWidth', 2, 'Color', [0.5 0.5 0.5]);
hold on
end
hold on
plot(xaxes, bstats(16:18,17), '-o','Color', [0 0 0], 'LineWidth', 2, 'MarkerFaceColor', [0 0 0]);


%plot(xaxes, bstats(16:18,19), 'o', 'Color', [0 0 0], 'MarkerFaceColor', [0 0 0]);
%plot(xaxes, bstats(16:18,20), 'o', 'Color', [0 0 0], 'MarkerFaceColor', [0 0 0]);

xlim([0 1000]);
xticks([0:200:1000]);
% yticks([3:0.5:6.5]);
% ylim([3 6.5]);
yticks([1:1:7]);
ylim([1 7]);
ylabel('Peak dispersion (s)');
xlabel('Stimulus duration (ms)');

subplot(4,5,20)
xaxes = [100 300 900];

for i = 1:numel(xaxes)
bars = plot([xaxes(i) xaxes(i)], [bstats(15+i,23) bstats(15+i,24)], 'LineWidth', 2, 'Color', [0.5 0.5 0.5]);
hold on
end
hold on
means = plot(xaxes, bstats(16:18,21),'-o', 'Color', [0 0 0], 'LineWidth', 2, 'MarkerFaceColor', [0 0 0]);


%ci1 = plot(xaxes, bstats(16:18,23), 'o',  'Color', [0 0 0],'MarkerFaceColor', [0 0 0]);
%ci2 = plot(xaxes, bstats(16:18,24), 'o',  'Color', [0 0 0],'MarkerFaceColor', [0 0 0]);
xticks([0:200:1000]);
xlim([0 1000]);
% yticks([1:0.5:4]);
% ylim([1 4]);
yticks([2.2:0.2:3.2]);
ylim([2.2 3.2]);
ylabel('Response latency (s)');
xlabel('Stimulus duration (ms)');
hold off
%%

% %% balloon hrfs
% figure;
% ybounds = [-0.4 1.2];
% %ybounds = [-0.4 0.8];
% 
% subplot(2,3,1)
% for l = 1
%     for i = 1:numel(intensities)
%         cols = ['r' 'g' 'b'];
%         plot(0:0.625:15, nanmean(eravecs{l,i}), cols(i))
%         hold on
%         TR = 0.625/1000;
%         eventlength = 15*1000;
%         t = 0:TR:(eventlength-1)*TR;
%         t2 = 0:0.001:15-0.001;
%         t3 = 0:0.01:15-0.01;
%         plot(t3, nanmean(balcurve{l,i}), cols(i),'LineWidth', 2)
%         %plot(t, nanmean(hrfvecs{l,i}), cols(i), 'LineWidth', 2)
%         ylim(ybounds);
%         yticks(ybounds(1):0.2:ybounds(2));
%         xticks(0:2:16);
%         ylabel('% signal change');
%         title(sprintf('%s', [num2str(lengths(l)), 'ms']),'fontsize',12);
%     end
% end
% 
% subplot(2,3,2)
% for l = 2
%     for i = 1:numel(intensities)
%         cols = ['r' 'g' 'b'];
%         plot(0:0.625:15, nanmean(eravecs{l,i}), cols(i))
%         hold on
%         TR = 0.625/1000;
%         eventlength = 15*1000;
%         t = 0:TR:(eventlength-1)*TR;
%         t2 = 0:0.001:15-0.001;
%         t3 = 0:0.01:15-0.01;
%         plot(t3, nanmean(balcurve{l,i}), cols(i),'LineWidth', 2)
%         %plot(t,nanmean(hrfvecs{l,i}), cols(i), 'LineWidth', 2)
%         ylim(ybounds);
%         yticks(ybounds(1):0.2:ybounds(2));
%         xticks(0:2:16);
%         title(sprintf('%s', [num2str(lengths(l)), 'ms']),'fontsize',12);
%     end
% end
% 
% subplot(2,3,3)
% for l = 3
%     for i = 1:numel(intensities)
%         cols = ['r' 'g' 'b'];
%         plot(0:0.625:15, nanmean(eravecs{l,i}), cols(i),'HandleVisibility','off')
%         hold on
%         TR = 0.625/1000;
%         eventlength = 15*1000;
%         t = 0:TR:(eventlength-1)*TR;
%         t2 = 0:0.001:15-0.001;
%         t3 = 0:0.01:15-0.01;
%         plot(t3, nanmean(balcurve{l,i}), cols(i),'LineWidth', 2)
%         %plot(t,nanmean(hrfvecs{l,i}), cols(i), 'LineWidth', 2)
%         ylim(ybounds);
%         yticks(ybounds(1):0.2:ybounds(2));
%         xticks(0:2:16);
%         title(sprintf('%s', [num2str(lengths(l)), 'ms']),'fontsize',12);
%         legend('1% contrast', '10% contrast', '100% contrast');
%     end
% end
% 
% subplot(2,3,4)
% for l = 1:numel(lengths)
%     for i = 1
%         cols = {[0.9 0.3 0.6],[0.4 0.9 0.9],[0.5 0.4 1]};
%         plot(0:0.625:15, nanmean(eravecs{l,i}), 'Color', cols{l})
%         hold on
%         TR = 0.625/1000;
%         eventlength = 15*1000;
%         t = 0:TR:(eventlength-1)*TR;
%         t2 = 0:0.001:15-0.001;
%         t3 = 0:0.01:15-0.01;
%         plot(t3, nanmean(balcurve{l,i}), 'Color', cols{l},'LineWidth', 2)
%         %plot(t, nanmean(hrfvecs{l,i}), 'Color', cols{l}, 'LineWidth', 2)
%         ylim(ybounds);
%         ylabel('% signal change');
%         yticks(ybounds(1):0.2:ybounds(2));
%         xticks(0:2:16);
%         title(sprintf('%s', [num2str(intensities(i)), '% contrast']),'fontsize',12);
%     end
% end
% 
% subplot(2,3,5)
% for l = 1:numel(lengths)
%     for i = 2
%         cols = {[0.9 0.3 0.6],[0.4 0.9 0.9],[0.5 0.4 1]};
%         plot(0:0.625:15, nanmean(eravecs{l,i}), 'Color', cols{l})
%         hold on
%         TR = 0.625/1000;
%         eventlength = 15*1000;
%         t = 0:TR:(eventlength-1)*TR;
%         t2 = 0:0.001:15-0.001;
%         t3 = 0:0.01:15-0.01;
%         plot(t3, nanmean(balcurve{l,i}), 'Color', cols{l},'LineWidth', 2)
%         %plot(t,nanmean(hrfvecs{l,i}), 'Color', cols{l}, 'LineWidth', 2)
%         ylim(ybounds);
%         yticks(ybounds(1):0.2:ybounds(2));
%         xticks(0:2:16);
%         xlabel('Time in seconds');
%         title(sprintf('%s', [num2str(intensities(i)), '% contrast']),'fontsize',12);
%     end
% end
% 
% subplot(2,3,6)
% for l = 1:numel(lengths)
%     for i = 3
%         cols = {[0.9 0.3 0.6],[0.4 0.9 0.9],[0.5 0.4 1]};
%         plot(0:0.625:15, nanmean(eravecs{l,i}), 'Color', cols{l}, 'HandleVisibility','off')
%         hold on
%         TR = 0.625/1000;
%         eventlength = 15*1000;
%         t = 0:TR:(eventlength-1)*TR;
%         t2 = 0:0.001:15-0.001;
%         t3 = 0:0.01:15-0.01;
%         plot(t3, nanmean(balcurve{l,i}), 'Color', cols{l},'LineWidth', 2)
%         %plot(t,nanmean(hrfvecs{l,i}),'Color', cols{l}, 'LineWidth', 2)
%         ylim(ybounds);
%         yticks(ybounds(1):0.2:ybounds(2));
%         xticks(0:2:16);
%         title(sprintf('%s', [num2str(intensities(i)), '% contrast']),'fontsize',12);
%         legend('100ms', '300ms', '900ms');
%     end
% end
%% boxplots
% figure;
% subplot(1,2,1)
% for a = 1:numel(nbpmagdist)
%     mags(:,a) = nbpmagdist{a};
% end
% boxplot(mags);
% hold on
% plot(1:9, nbstats(1:9,1), 'ro')
% 
% subplot(1,2,2)
% for b = 1:numel(output1)
% bmags(b,1) = output1(b).MaxValue;
% bmags(b,2) = output2(b).MaxValue;
% bmags(b,3) = output3(b).MaxValue;
% bmags(b,4) = output4(b).MaxValue;
% bmags(b,5) = output5(b).MaxValue;
% bmags(b,6) = output6(b).MaxValue;
% bmags(b,7) = output7(b).MaxValue;
% bmags(b,8) = output8(b).MaxValue;
% bmags(b,9) = output9(b).MaxValue;
% end
% boxplot(bmags);
% hold on
% plot(1:9, bstats(1:9,1), 'ro')
cd(pwd);