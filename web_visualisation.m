clearvars();
cd('/Volumes/Passport/TRoN/Analysis/Bootstp/aud/');
summdat = readtable('BootSSummaryVals.csv');
diffdat = readtable('BootSSummaryDiffs.csv');
cd('/Volumes/Passport/TRoN/Analysis/Bootstp/save');
%%
% Uncomment below and comment above to plot real-subject data

% clearvars();
% cd('/Volumes/Passport/TRoN/Analysis/PSubj/');
% summdat = readtable('PSSummaryVals.csv');
% diffdat = readtable('PSSummaryDiffs.csv');
% cd('/Volumes/Passport/TRoN/Analysis/PSubj/save');

x0 = [log(0.01)/log(3)+1, log(0.1)/log(3)+1, log(1)/log(3)+1];
xlabv = [{'Intensity 0.01', 'Intensity 0.1', 'Intensity 1'}];
xlaba = [{'Intensity 0.1', 'Intensity 0.3', 'Intensity 0.9'}];
x1 = [100 300 900];

eventtype = 'A';

if eventtype == 'V'
    xlaba = xlabv;
end

l1 = [table2array(summdat(1:3,1)) table2array(summdat(1:3,5)) table2array(summdat(1:3,9)) table2array(summdat(1:3,13)) table2array(summdat(1:3,17)) table2array(summdat(1:3,21))];
l1ampci = [table2array(summdat(1:3,3)) table2array(summdat(1:3,4))];
l1delci = [table2array(summdat(1:3,7)) table2array(summdat(1:3,8))];
l1peraci = [table2array(summdat(1:3,11)) table2array(summdat(1:3,12))];
l1ttpci = [table2array(summdat(1:3,15)) table2array(summdat(1:3,16))];
l1dispci = [table2array(summdat(1:3,19)) table2array(summdat(1:3,20))];
l1onsci = [table2array(summdat(1:3,23)) table2array(summdat(1:3,24))];

l2 = [table2array(summdat(4:6,1)) table2array(summdat(4:6,5)) table2array(summdat(4:6,9)) table2array(summdat(4:6,13)) table2array(summdat(4:6,17)) table2array(summdat(4:6,21))];
l2ampci = [table2array(summdat(4:6,3)) table2array(summdat(4:6,4))];
l2delci = [table2array(summdat(4:6,7)) table2array(summdat(4:6,8))];
l2peraci = [table2array(summdat(4:6,11)) table2array(summdat(4:6,12))];
l2ttpci = [table2array(summdat(4:6,15)) table2array(summdat(4:6,16))];
l2dispci = [table2array(summdat(4:6,19)) table2array(summdat(4:6,20))];
l2onsci = [table2array(summdat(4:6,23)) table2array(summdat(4:6,24))];

l3 = [table2array(summdat(7:9,1)) table2array(summdat(7:9,5)) table2array(summdat(7:9,9)) table2array(summdat(7:9,13)) table2array(summdat(7:9,17)) table2array(summdat(7:9,21))];
l3ampci = [table2array(summdat(7:9,3)) table2array(summdat(7:9,4))];
l3delci = [table2array(summdat(7:9,7)) table2array(summdat(7:9,8))];
l3peraci = [table2array(summdat(7:9,11)) table2array(summdat(7:9,12))];
l3ttpci = [table2array(summdat(7:9,15)) table2array(summdat(7:9,16))];
l3dispci = [table2array(summdat(7:9,19)) table2array(summdat(7:9,20))];
l3onsci = [table2array(summdat(7:9,23)) table2array(summdat(7:9,24))];

i1 = [table2array(summdat(10:12,1)) table2array(summdat(10:12,5)) table2array(summdat(10:12,9)) table2array(summdat(10:12,13)) table2array(summdat(10:12,17)) table2array(summdat(10:12,21))];
i1ampci = [table2array(summdat(10:12,3)) table2array(summdat(10:12,4))];
i1delci = [table2array(summdat(10:12,7)) table2array(summdat(10:12,8))];
i1peraci = [table2array(summdat(10:12,11)) table2array(summdat(10:12,12))];
i1ttpci = [table2array(summdat(10:12,15)) table2array(summdat(10:12,16))];
i1dispci = [table2array(summdat(10:12,19)) table2array(summdat(10:12,20))];
i1onsci = [table2array(summdat(10:12,23)) table2array(summdat(10:12,24))];

i2 = [table2array(summdat(13:15,1)) table2array(summdat(13:15,5)) table2array(summdat(13:15,9)) table2array(summdat(13:15,13)) table2array(summdat(13:15,17)) table2array(summdat(13:15,21))];
i2ampci = [table2array(summdat(13:15,3)) table2array(summdat(13:15,4))];
i2delci = [table2array(summdat(13:15,7)) table2array(summdat(13:15,8))];
i2peraci = [table2array(summdat(13:15,11)) table2array(summdat(13:15,12))];
i2ttpci = [table2array(summdat(13:15,15)) table2array(summdat(13:15,16))];
i2dispci = [table2array(summdat(13:15,19)) table2array(summdat(13:15,20))];
i2onsci = [table2array(summdat(13:15,23)) table2array(summdat(13:15,24))];

i3 = [table2array(summdat(16:18,1)) table2array(summdat(16:18,5)) table2array(summdat(16:18,9)) table2array(summdat(16:18,13)) table2array(summdat(16:18,17)) table2array(summdat(16:18,21))];
i3ampci = [table2array(summdat(16:18,3)) table2array(summdat(16:18,4))];
i3delci = [table2array(summdat(16:18,7)) table2array(summdat(16:18,8))];
i3peraci = [table2array(summdat(16:18,11)) table2array(summdat(16:18,12))];
i3ttpci = [table2array(summdat(16:18,15)) table2array(summdat(16:18,16))];
i3dispci = [table2array(summdat(16:18,19)) table2array(summdat(16:18,20))];
i3onsci = [table2array(summdat(16:18,23)) table2array(summdat(16:18,24))];
%% Peak Amplitude when intensity constant
f1 = polyfit(x1',i1(:,1), 1);
f2 = polyfit(x1',i2(:,1), 1);
f3 = polyfit(x1',i3(:,1), 1);
ff1 = polyval(f1, x1);
ff2 = polyval(f2, x1);
ff3 = polyval(f3, x1);

figure;
ax1 = subplot(1,3,1);
axis([0 1000 0 1.5])
xticks([0:100:1000]);
yticks([0:0.1:1.5]);
hold on
plot(ax1, x1, ff1, 'k', 'LineWidth', 1);
scatter(ax1, x1, i1(:,1), 60, 'filled', 'd', 'r');
errorbar(ax1, x1, i1(:,1), i1ampci(:,1)-i1(:,1), i1ampci(:,2) - i1(:,1), 'dr','HandleVisibility','off');
xlabel('Stimulus length (m/s)', 'fontsize', 14);
ylabel('Peak signal change (%)', 'fontsize', 14);
title(sprintf('%s', ['intensity 0.1', 'd =', num2str(table2array(diffdat(12,5)))]));
hold on

ax2 = subplot(1,3,2);
axis([0 1000 0 1.5])
xticks([0:100:1000]);
yticks([0:0.1:1.5]);
hold on
plot(ax2, x1, ff2, 'k', 'LineWidth', 1);
scatter(ax2, x1, i2(:,1), 60, 'filled', 's', 'r');
errorbar(ax2, x1, i2(:,1), i2ampci(:,1)-i2(:,1), i2ampci(:,2) - i2(:,1), 'sr','HandleVisibility','off');
xlabel('Stimulus length (m/s)', 'fontsize', 14);
ylabel('Peak signal change (%)', 'fontsize', 14);
title(sprintf('%s', ['intensity 0.3', 'd =', num2str(table2array(diffdat(15,5)))]));
hold on

ax3 = subplot(1,3,3);
axis([0 1000 0 1.5])
xticks([0:100:1000]);
yticks([0:0.1:1.5]);
hold on
plot(ax3, x1, ff3, 'k', 'LineWidth', 1);
scatter(ax3, x1, i3(:,1), 60, 'filled', 'o','r');
errorbar(ax3, x1, i3(:,1), i3ampci(:,1)-i3(:,1), i3ampci(:,2) - i3(:,1), 'or','HandleVisibility','off');
xlabel('Stimulus length (m/s)', 'fontsize', 14);
ylabel('Peak signal change (%)', 'fontsize', 14);
title(sprintf('%s', ['intensity 0.9', 'd =', num2str(table2array(diffdat(18,5)))]));
hold on

sgtitle(sprintf('%s', 'Mean and CIs of peak amplitudes across stimulus length'),'fontsize',16);
%savefig('AmpforInts.fig');
hold off

%% Peak Amplitude when duration constant
f4 = polyfit(x0',l1(:,1), 1);
f5 = polyfit(x0',l2(:,1), 1);
f6 = polyfit(x0',l3(:,1), 1);
ff4 = polyval(f4, x0);
ff5 = polyval(f5, x0);
ff6 = polyval(f6, x0);

figure;
ax1 = subplot(1,3,1);
axis([-3.5 1.5 0 1.5])
xticks(x0);
xticklabels(xlaba);
yticks([0:0.1:1.5]);
xlabel('Contrast (a.u.)', 'fontsize', 14); % x-axis label
ylabel('Peak signal change (%)', 'fontsize', 14);
hold on
plot(x0, ff4, 'k', 'LineWidth', 1);
scatter(x0, l1(:,1), 60, 'filled', 'd', 'r');
errorbar(x0, l1(:,1), l1ampci(:,1)-l1(:,1), l1ampci(:,2) - l1(:,1), 'dr','HandleVisibility','off');
title(sprintf('%s', ['length 100ms ', 'd =', num2str(table2array(diffdat(3,5)))]));
hold on

ax2 = subplot(1,3,2);
axis([-3.5 1.5 0 1.5])
xticks(x0);
xticklabels(xlaba);
yticks([0:0.1:1.5]);
xlabel('Contrast (a.u.)', 'fontsize', 14); % x-axis label
ylabel('Peak signal change (%)', 'fontsize', 14);
hold on
plot( x0, ff5, 'k', 'LineWidth', 1);
scatter(x0, l2(:,1), 60, 'filled', 's', 'r');
errorbar(x0, l2(:,1), l2ampci(:,1)-l2(:,1), l2ampci(:,2)- l2(:,1), 'sr','HandleVisibility','off');
title(sprintf('%s', ['length 300ms ', 'd =', num2str(table2array(diffdat(6,5)))]));
hold on

ax3 = subplot(1,3,3);
axis([-3.5 1.5 0 1.5])
xticks(x0);
xticklabels(xlaba);
yticks([0:0.1:1.5]);
xlabel('Contrast (a.u.)', 'fontsize', 14); % x-axis label
ylabel('Peak signal change (%)', 'fontsize', 14);
hold on
plot(x0, ff6, 'k', 'LineWidth', 1);
scatter(x0, l3(:,1), 60, 'filled', 'o', 'r');
errorbar(x0, l3(:,1), l3ampci(:,1)-l3(:,1), l3ampci(:,2)- l3(:,1), 'or','HandleVisibility','off');
title(sprintf('%s', ['length 900ms ', 'd =', num2str(table2array(diffdat(9,5)))]));
hold on

sgtitle(sprintf('%s', 'Mean and CIs of peak amplitudes across stimulus intensity'),'fontsize',16);
%savefig('AmpforDurs.fig');
hold off

%% Peak Delay when intensity constant

f1 = polyfit(x1',i1(:,2), 1);
f2 = polyfit(x1',i2(:,2), 1);
f3 = polyfit(x1',i3(:,2), 1);
ff1 = polyval(f1, x1);
ff2 = polyval(f2, x1);
ff3 = polyval(f3, x1);

figure;
ax1 = subplot(1,3,1);
axis([0 1000 3.5 9.5])
xticks([0:100:1000]);
yticks([3.5:0.5:9.5]);
xlabel('Stimulus length (m/s)', 'fontsize', 14); % x-axis label
ylabel('Peak delay (s)', 'fontsize', 14);
hold on
plot(x1, ff1, 'k', 'LineWidth', 1);
scatter(x1, i1(:,2), 60, 'filled', 'd', 'r')
errorbar(x1, i1(:,2), i1delci(:,1)-i1(:,2), i1delci(:,2) - i1(:,2), 'dr','HandleVisibility','off');
title(sprintf('%s', ['intensity 0.01', 'd =', num2str(table2array(diffdat(12,10)))]));
hold on

ax2 = subplot(1,3,2);
axis([0 1000 3.5 9.5])
xticks([0:100:1000]);
yticks([3.5:0.5:9.5]);
xlabel('Stimulus length (m/s)', 'fontsize', 14); % x-axis label
ylabel('Peak delay (s)', 'fontsize', 14);
hold on
plot(x1, ff2, 'k', 'LineWidth', 1);
scatter(x1, i2(:,2), 60, 'filled', 's', 'r');
errorbar(x1, i2(:,2), i2delci(:,1)-i2(:,2), i2delci(:,2) - i2(:,2), 'sr','HandleVisibility','off');
title(sprintf('%s', ['intensity 0.1', 'd =', num2str(table2array(diffdat(15,10)))]));
hold on

ax3 = subplot(1,3,3);
axis([0 1000 3.5 9.5])
xticks([0:100:1000]);
yticks([3.5:0.5:9.5]);
xlabel('Stimulus length (m/s)', 'fontsize', 14); % x-axis label
ylabel('Peak delay (s)', 'fontsize', 14);
hold on
plot(x1, ff3, 'k', 'LineWidth', 1);
scatter(x1, i3(:,2), 60, 'filled', 'o','r');
errorbar(x1, i3(:,2), i3delci(:,1)-i3(:,2), i3delci(:,2) - i3(:,2), 'or','HandleVisibility','off');
title(sprintf('%s', ['intensity 1', 'd =', num2str(table2array(diffdat(18,10)))]));
hold on
sgtitle(sprintf('%s', 'Mean and CIs of peak delay across stimulus lengths'),'fontsize',16);

%savefig('DelforInts.fig');
hold off

%% Peak Delay when duration constant
f4 = polyfit(x0',l1(:,2), 1);
f5 = polyfit(x0',l2(:,2), 1);
f6 = polyfit(x0',l3(:,2), 1);
ff4 = polyval(f4, x0);
ff5 = polyval(f5, x0);
ff6 = polyval(f6, x0);

figure;
ax1 = subplot(1,3,1);
axis([-3.5 1.5 3.5 9.5])
xticks(x0);
xticklabels(xlaba);
yticks([3.5:0.5:9.5]);
xlabel('Contrast (a.u.)', 'fontsize', 14); % x-axis label
ylabel('Peak delay (s)', 'fontsize', 14);
hold on
plot(x0, ff4, 'k', 'LineWidth', 1)
scatter(x0, l1(:,2), 60, 'filled', 'd', 'r', 'HandleVisibility','off');
errorbar(x0, l1(:,2), l1delci(:,1)-l1(:,2), l1delci(:,2) - l1(:,2), 'dr','HandleVisibility','off');
title(sprintf('%s', ['length 100ms ', 'd =', num2str(table2array(diffdat(3,10)))]));

hold on

ax2 = subplot(1,3,2);
axis([-3.5 1.5 3.5 9.5])
xticks(x0);
xticklabels(xlaba);
yticks([3.5:0.5:9.5]);
xlabel('Contrast (a.u.)', 'fontsize', 14); % x-axis label
ylabel('Peak delay (s)', 'fontsize', 14);
hold on
plot(x0, ff5, 'k', 'LineWidth', 1);
scatter(x0, l2(:,2), 60, 'filled', 's', 'r', 'HandleVisibility','off');
errorbar(x0, l2(:,2), l2delci(:,1)-l2(:,2), l2delci(:,2)- l2(:,2), 'dr','HandleVisibility','off');
title(sprintf('%s', ['length 300ms ', 'd =', num2str(table2array(diffdat(6,10)))]));
hold on

ax3 = subplot(1,3,3);
axis([-3.5 1.5 3.5 9.5])
xticks(x0);
xticklabels(xlaba);
yticks([3.5:0.5:9.5]);
xlabel('Contrast (a.u.)', 'fontsize', 14); % x-axis label
ylabel('Peak delay (s)', 'fontsize', 14);
hold on
plot(x0, ff6, 'k', 'LineWidth', 1);
title(sprintf('%s', ['length 900ms ', 'd =', num2str(table2array(diffdat(9,10)))]));
scatter(x0, l3(:,2), 60, 'filled', 'o', 'r', 'HandleVisibility','off');
errorbar(x0, l3(:,2), l3delci(:,1)-l3(:,2), l3delci(:,1)- l3(:,2), 'or','HandleVisibility','off');
hold on

sgtitle(sprintf('%s', 'Means and CIs of peak delay across stimulus intensity'),'fontsize',16);
%savefig('DelforDurs.fig');
hold off

%% Event-average peak when intensity constant

f1 = polyfit(x1',i1(:,3), 1);
f2 = polyfit(x1',i2(:,3), 1);
f3 = polyfit(x1',i3(:,3), 1);
ff1 = polyval(f1, x1);
ff2 = polyval(f2, x1);
ff3 = polyval(f3, x1);

figure;
ax1 = subplot(1,3,1);
axis([0 1000 0 1.5])
xticks([0:100:1000]);
yticks([0:0.1:1.5]);
xlabel('Stimulus length (m/s)', 'fontsize', 14); % x-axis label
ylabel('Peak event-related signal change (%)', 'fontsize', 14);
hold on
plot(x1, ff1, 'k', 'LineWidth', 1)
scatter(x1, i1(:,3), 60, 'filled', 'd', 'r', 'HandleVisibility','off');
errorbar(x1, i1(:,3), i1peraci(:,1)-i1(:,3), i1peraci(:,2) - i1(:,3), 'dr','HandleVisibility','off');
title(sprintf('%s', ['intensity 0.01', 'd =', num2str(table2array(diffdat(12,15)))]));
hold on

ax2 = subplot(1,3,2);
axis([0 1000 0 1.5])
xticks([0:100:1000]);
yticks([0:0.1:1.5]);
xlabel('Stimulus length (m/s)', 'fontsize', 14); % x-axis label
ylabel('Peak event-related signal change (%)', 'fontsize', 14);
hold on
plot(x1, ff2, 'k', 'LineWidth', 1);
errorbar(x1, i2(:,3), i2peraci(:,1)-i2(:,3), i2peraci(:,2) - i2(:,3), 'sr','HandleVisibility','off');
scatter(x1, i2(:,3), 60, 'filled', 's', 'r', 'HandleVisibility','off');
title(sprintf('%s', ['intensity 0.1', 'd =', num2str(table2array(diffdat(15,15)))]));
hold on

ax3 = subplot(1,3,3);
axis([0 1000 0 1.5])
xticks([0:100:1000]);
yticks([0:0.1:1.5]);
xlabel('Stimulus length (m/s)', 'fontsize', 14); % x-axis label
ylabel('Peak event-related signal change (%)', 'fontsize', 14);
hold on;
plot(x1, ff3, 'k', 'LineWidth', 1);
title(sprintf('%s', ['intensity 0.9', 'd =', num2str(table2array(diffdat(18,15)))]));
scatter(x1, i3(:,3), 60, 'filled', 'r', 'HandleVisibility','off');
errorbar(x1, i3(:,3), i3peraci(:,1)-i3(:,3), i3peraci(:,2) - i3(:,3), 'or','HandleVisibility','off');
hold on

sgtitle(sprintf('%s', 'Means and CIs of peak signal change from event averaging across stimulus length'),'fontsize',16);
%savefig('PeraforInts.fig');
hold off

%% Event-average peak when duration constant
f4 = polyfit(x0',l1(:,3), 1);
f5 = polyfit(x0',l2(:,3), 1);
f6 = polyfit(x0',l3(:,3), 1);
ff4 = polyval(f4, x0);
ff5 = polyval(f5, x0);
ff6 = polyval(f6, x0);

figure;
ax1 = subplot(1,3,1);
axis([-3.5 1.5 0 1.5])
xticks(x0);
xticklabels(xlaba);
yticks([0:0.1:1.5]);
xlabel('Contrast (a.u.)', 'fontsize', 14); % x-axis label
ylabel('Peak signal change (%)', 'fontsize', 14);
hold on
plot( x0, ff4, 'k', 'LineWidth', 1);
scatter(x0, l1(:,3), 60, 'filled', 'd', 'r', 'HandleVisibility','off');
errorbar(x0, l1(:,3), l1peraci(:,1)-l1(:,3), l1peraci(:,2) - l1(:,3), 'dr','HandleVisibility','off');
title(sprintf('%s', ['length 100ms ', 'd =', num2str(table2array(diffdat(3,15)))]));
hold on

ax2 = subplot(1,3,2);
axis([-3.5 1.5 0 1.5])
xticks(x0);
xticklabels(xlaba);
yticks([0:0.1:1.5]);
xlabel('Contrast (a.u.)', 'fontsize', 14); % x-axis label
ylabel('Peak signal change (%)', 'fontsize', 14);
hold on
plot(x0, ff5, 'k', 'LineWidth', 1);
scatter(x0, l2(:,3), 60, 'filled', 's','r', 'HandleVisibility','off');
errorbar(x0, l2(:,3), l2peraci(:,1)-l2(:,3), l2peraci(:,2)- l2(:,3), 'sr','HandleVisibility','off');
title(sprintf('%s', ['length 300ms ', 'd =', num2str(table2array(diffdat(6,15)))]));
hold on

ax3 = subplot(1,3,3);
axis([-3.5 1.5 0 1.5])
xticks(x0);
xticklabels(xlaba);
yticks([0:0.1:1.5]);
xlabel('Contrast (a.u.)', 'fontsize', 14); % x-axis label
ylabel('Peak signal change (%)', 'fontsize', 14);
hold on
plot(x0, ff6, 'k', 'LineWidth', 1)
scatter(x0, l3(:,3), 60, 'filled', 'r', 'HandleVisibility','off');
errorbar(x0, l3(:,3), l3peraci(:,1)-l3(:,3), l3peraci(:,2)- l3(:,3), 'or','HandleVisibility','off');
title(sprintf('%s', ['length 900ms ', 'd =', num2str(table2array(diffdat(9,15)))]));
hold on

sgtitle(sprintf('%s', 'Means and CIs of peak signal change from event averaging across stimulus intensity'),'fontsize',16);
%savefig('PeraforDurs.fig');
hold off

%% Time to event-average peak when intensity constant

f1 = polyfit(x1',i1(:,4), 1);
f2 = polyfit(x1',i2(:,4), 1);
f3 = polyfit(x1',i3(:,4), 1);
ff1 = polyval(f1, x1);
ff2 = polyval(f2, x1);
ff3 = polyval(f3, x1);

figure;
ax1 = subplot(1,3,1);
axis([0 1000 3.5 12])
yticks([3.5:0.5:12]);
xticks([0:100:1000]);
xlabel('Stimulus length (m/s)', 'fontsize', 14); % x-axis label
ylabel('Peak delay (s)', 'fontsize', 14);
hold on
plot(x1, ff1, 'k', 'LineWidth', 1)
scatter(x1, i1(:,4), 60, 'filled', 'd','r', 'HandleVisibility','off');
errorbar(x1, i1(:,4), i1ttpci(:,1)-i1(:,4), i1ttpci(:,2) - i1(:,4), 'dr','HandleVisibility','off');
title(sprintf('%s', ['intensity 0.01', 'd =', num2str(table2array(diffdat(12,20)))]));
hold on

ax2 = subplot(1,3,2);
axis([0 1000 3.5 12])
yticks([3.5:0.5:12]);
xticks([0:100:1000]);
xlabel('Stimulus length (m/s)', 'fontsize', 14); % x-axis label
ylabel('Peak delay (s)', 'fontsize', 14);
hold on
plot(x1, ff2, 'k', 'LineWidth', 1);
title(sprintf('%s', ['intensity 0.1', 'd =', num2str(table2array(diffdat(15,20)))]));
scatter(x1, i2(:,4), 60, 'filled', 's','r', 'HandleVisibility','off');
errorbar(x1, i2(:,4), i2ttpci(:,1)-i2(:,4), i2ttpci(:,2) - i2(:,4), 'sr','HandleVisibility','off');
hold on

ax3 = subplot(1,3,3);
axis([0 1000 3.5 12])
yticks([3.5:0.5:12]);
xticks([0:100:1000]);
xlabel('Stimulus length (m/s)', 'fontsize', 14); % x-axis label
ylabel('Peak delay (s)', 'fontsize', 14);
hold on
plot(x1, ff3, 'k', 'LineWidth', 1)
title(sprintf('%s', ['intensity 0.9', 'd =', num2str(table2array(diffdat(18,20)))]));
scatter(x1, i3(:,4), 60, 'filled', 'r', 'HandleVisibility','off');
errorbar(x1, i3(:,4), i3ttpci(:,1)-i3(:,4), i3ttpci(:,2) - i3(:,4), 'or','HandleVisibility','off');
hold on

sgtitle(sprintf('%s', 'Means and CIs of event-averaged time to peak across stimulus length'),'fontsize',16);
%savefig('TTPeraforInts.fig');
hold off

%% Time to event-average peak when duration constant
f4 = polyfit(x0',l1(:,4), 1);
f5 = polyfit(x0',l2(:,4), 1);
f6 = polyfit(x0',l3(:,4), 1);
ff4 = polyval(f4, x0);
ff5 = polyval(f5, x0);
ff6 = polyval(f6, x0);

figure;
ax1 = subplot(1,3,1);
axis([-3.5 1.5 3.5 12])
yticks([3.5:0.5:12]);
xticks(x0);
xticklabels(xlaba);
xlabel('Contrast (a.u.)', 'fontsize', 14); % x-axis label
ylabel('Peak delay (s)', 'fontsize', 14);
hold on
plot(x0, ff4, 'k', 'LineWidth', 1)
scatter(x0, l1(:,4), 60, 'filled', 'd','r', 'HandleVisibility','off');
errorbar(x0, l1(:,4), l1ttpci(:,1)-l1(:,4), l1ttpci(:,2) - l1(:,4), 'dr','HandleVisibility','off');
title(sprintf('%s', ['length 100ms ', 'd =', num2str(table2array(diffdat(3,20)))]));
hold on

ax2 = subplot(1,3,2);
axis([-3.5 1.5 3.5 12])
yticks([3.5:0.5:12]);
xticks(x0);
xticklabels(xlaba);
xlabel('Contrast (a.u.)', 'fontsize', 14); % x-axis label
ylabel('Peak delay (s)', 'fontsize', 14);
hold on
plot(x0, ff5, 'k', 'LineWidth', 1);
scatter(x0, l2(:,4), 60, 'filled', 's', 'r', 'HandleVisibility','off');
errorbar(x0, l2(:,4), l2ttpci(:,1)-l2(:,4), l2ttpci(:,2)- l2(:,4), 'sr','HandleVisibility','off');
title(sprintf('%s', ['length 300ms ', 'd =', num2str(table2array(diffdat(6,20)))]));
hold on

ax3 = subplot(1,3,3);
axis([-3.5 1.5 3.5 12])
yticks([3.5:0.5:12]);
xticks(x0);
xticklabels(xlaba);
xlabel('Contrast (a.u.)', 'fontsize', 14); % x-axis label
ylabel('Peak delay (s)', 'fontsize', 14);
hold on
plot(x0, ff6, 'k', 'LineWidth', 1)
scatter(x0, l3(:,4), 60, 'filled', 'r', 'HandleVisibility','off');
errorbar(x0, l3(:,4), l3ttpci(:,1)-l3(:,4), l3ttpci(:,2)- l3(:,4), 'or','HandleVisibility','off');
title(sprintf('%s', ['length 900ms ', 'd =', num2str(table2array(diffdat(9,20)))]));
hold on

sgtitle(sprintf('%s', 'Means and CIs of event-averaged time to peak across stimulus intensity'),'fontsize',16);
%savefig('TTPeraforDurs.fig');
hold off

%% Peak dispersion(FWHMinSecs) when intensity constant

f1 = polyfit(x1',i1(:,5), 1);
f2 = polyfit(x1',i2(:,5), 1);
f3 = polyfit(x1',i3(:,5), 1);
ff1 = polyval(f1, x1);
ff2 = polyval(f2, x1);
ff3 = polyval(f3, x1);

figure;
ax1 = subplot(1,3,1);
axis([0 1000 2 6])
xticks([0:100:1000]);
yticks([2:0.5:6]);
xlabel('Stimulus length (m/s)', 'fontsize', 14); % x-axis label
ylabel('Peak dispersion (FWHM) (s)', 'fontsize', 14);
hold on
plot(x1, ff1, 'k', 'LineWidth', 1)
scatter(x1, i1(:,5), 60, 'filled', 'd','r', 'HandleVisibility','off');
errorbar(x1, i1(:,5), i1dispci(:,1)-i1(:,5), i1dispci(:,2) - i1(:,5), 'dr','HandleVisibility','off');
title(sprintf('%s', ['intensity 0.01', 'd =', num2str(table2array(diffdat(12,25)))]));
hold on

ax2 = subplot(1,3,2);
axis([0 1000 2 6])
xticks([0:100:1000]);
yticks([2:0.5:6]);
xlabel('Stimulus length (m/s)', 'fontsize', 14); % x-axis label
ylabel('Peak dispersion (FWHM) (s)', 'fontsize', 14);
hold on
plot(x1, ff2, 'k', 'LineWidth', 1) 
scatter(x1, i2(:,5), 60, 'filled', 's','r', 'HandleVisibility','off');
errorbar(x1, i2(:,5), i2dispci(:,1)-i2(:,5), i2dispci(:,2) - i2(:,5), 'sr','HandleVisibility','off');
title(sprintf('%s', ['intensity 0.1', 'd =', num2str(table2array(diffdat(15,25)))]));
hold on

ax3 = subplot(1,3,3);
axis([0 1000 2 6])
xticks([0:100:1000]);
yticks([2:0.5:6]);
xlabel('Stimulus length (m/s)', 'fontsize', 14); % x-axis label
ylabel('Peak dispersion (FWHM) (s)', 'fontsize', 14);
hold on
plot(x1, ff3, 'k', 'LineWidth', 1)
title(sprintf('%s', ['intensity 0.9', 'd =', num2str(table2array(diffdat(18,25)))]));
scatter(x1, i3(:,5), 60, 'filled', 'r', 'HandleVisibility','off');
errorbar(x1, i3(:,5), i3dispci(:,1)-i3(:,5), i3dispci(:,2) - i3(:,5), 'or','HandleVisibility','off');
hold on

sgtitle(sprintf('%s', 'Means and CIs of peak dispersion across stimulus length'),'fontsize',16);
%savefig('DispforInts.fig');
hold off

%% Peak dispersion when duration constant
f4 = polyfit(x0',l1(:,5), 1);
f5 = polyfit(x0',l2(:,5), 1);
f6 = polyfit(x0',l3(:,5), 1);
ff4 = polyval(f4, x0);
ff5 = polyval(f5, x0);
ff6 = polyval(f6, x0);

figure;
ax1 = subplot(1,3,1);
axis([-3.5 1.5 2 6])
xticks(x0);
xticklabels(xlaba);
yticks([2:0.5:6]);
xlabel('Contrast (a.u.)', 'fontsize', 14); % x-axis label
ylabel('Peak dispersion (FWHM) (s)', 'fontsize', 14);
hold on
plot(x0, ff4, 'k', 'LineWidth', 1)
scatter(x0, l1(:,5), 60, 'filled', 'd', 'r', 'HandleVisibility','off');
errorbar(x0, l1(:,5), l1dispci(:,1)-l1(:,5), l1dispci(:,2) - l1(:,5), 'dr','HandleVisibility','off');
title(sprintf('%s', ['length 100ms ', 'd =', num2str(table2array(diffdat(3,25)))]));
hold on

ax2 = subplot(1,3,2);
axis([-3.5 1.5 2 6])
xticks(x0);
xticklabels(xlaba);
yticks([2:0.5:6]);
xlabel('Contrast (a.u.)', 'fontsize', 14); % x-axis label
ylabel('Peak dispersion (FWHM) (s)', 'fontsize', 14);
hold on
plot(x0, ff5, 'k', 'LineWidth', 1)
scatter(x0, l2(:,5), 60, 'filled', 's','r', 'HandleVisibility','off');
errorbar(x0, l2(:,5), l2dispci(:,1)-l2(:,5), l2dispci(:,2)- l2(:,5), 'sr','HandleVisibility','off');
title(sprintf('%s', ['length 300ms ', 'd =', num2str(table2array(diffdat(6,25)))]));
hold on

ax3 = subplot(1,3,3);
axis([-3.5 1.5 2 6])
xticks(x0);
xticklabels(xlaba);
yticks([2:0.5:6]);
xlabel('Contrast (a.u.)', 'fontsize', 14); % x-axis label
ylabel('Peak dispersion (FWHM) (s)', 'fontsize', 14);
hold on
plot(x0, ff6, 'k', 'LineWidth', 1)
scatter(x0, l3(:,5), 60, 'filled', 'r', 'HandleVisibility','off');
errorbar(x0, l3(:,5), l3dispci(:,1)-l3(:,5), l3dispci(:,2)- l3(:,5), 'or','HandleVisibility','off');
title(sprintf('%s', ['length 900ms ', 'd =', num2str(table2array(diffdat(9,25)))]));
hold on

sgtitle(sprintf('%s', 'Means and CIs of peak dispersion across stimulus intensity'),'fontsize',16);
%savefig('DispforDurs.fig');
hold off

%% Peak onset when intensity constant

f1 = polyfit(x1',i1(:,6), 1);
f2 = polyfit(x1',i2(:,6), 1);
f3 = polyfit(x1',i3(:,6), 1);
ff1 = polyval(f1, x1);
ff2 = polyval(f2, x1);
ff3 = polyval(f3, x1);

figure;
ax1 = subplot(1,3,1);
axis([0 1000 2.5 4.5])
xticks([0:100:1000]);
yticks([2.5:0.1:5]);
xlabel('Stimulus length (m/s)', 'fontsize', 14); % x-axis label
ylabel('Peak onset (s)', 'fontsize', 14);
hold on
plot(x1, ff1, 'k', 'LineWidth', 1)
scatter(x1, i1(:,6), 60, 'filled', 'd', 'r', 'HandleVisibility','off');
errorbar(x1, i1(:,6), i1onsci(:,1)-i1(:,6), i1onsci(:,2) - i1(:,6), 'dr','HandleVisibility','off');
title(sprintf('%s', ['intensity 0.01', 'd =', num2str(table2array(diffdat(12,30)))]));
hold on

ax2 = subplot(1,3,2);
axis([0 1000 2.5 4.5])
xticks([0:100:1000]);
yticks([2.5:0.1:5]);
xlabel('Stimulus length (m/s)', 'fontsize', 14); % x-axis label
ylabel('Peak onset (s)', 'fontsize', 14);
hold on
plot(x1, ff2, 'k', 'LineWidth', 1)
scatter(x1, i2(:,6), 60, 'filled', 's','r', 'HandleVisibility','off');
errorbar(x1, i2(:,6), i2onsci(:,1)-i2(:,6), i2onsci(:,2) - i2(:,6), 'sr','HandleVisibility','off');
title(sprintf('%s', ['intensity 0.1', 'd =', num2str(table2array(diffdat(15,30)))]));
hold on

ax3 = subplot(1,3,3);
axis([0 1000 2.5 4.5])
xticks([0:100:1000]);
yticks([2.5:0.1:5]);
xlabel('Stimulus length (m/s)', 'fontsize', 14); % x-axis label
ylabel('Peak onset (s)', 'fontsize', 14);
hold on
plot(x1, ff3, 'k', 'LineWidth', 1)
scatter(x1, i3(:,6), 60, 'filled', 's', 'r', 'HandleVisibility','off');
errorbar(x1, i3(:,6), i3onsci(:,1)-i3(:,6), i3onsci(:,2) - i3(:,6), 'sr','HandleVisibility','off');
title(sprintf('%s', ['intensity 0.9', 'd =', num2str(table2array(diffdat(18,30)))]));
hold on

sgtitle(sprintf('%s', 'Means and CIs of peak onset across stimulus length'),'fontsize',16);
%savefig('OnsetforInts.fig');
hold off

%% Peak onset when duration constant
f4 = polyfit(x0',l1(:,6), 1);
f5 = polyfit(x0',l2(:,6), 1);
f6 = polyfit(x0',l3(:,6), 1);
ff4 = polyval(f4, x0);
ff5 = polyval(f5, x0);
ff6 = polyval(f6, x0);

figure;
ax1 = subplot(1,3,1);
%axis([-3.5 1.5 3 5])
axis([-3.5 1.5 2.5 4.5])
xticks(x0);
xticklabels(xlaba);
yticks([2.5:0.1:5]);
xlabel('Contrast (a.u.)', 'fontsize', 14); % x-axis label
ylabel('Peak onset (s)', 'fontsize', 14);
hold on
plot(x0, ff4, 'k', 'LineWidth', 1)
scatter(x0, l1(:,6), 60, 'filled', 'd','r', 'HandleVisibility','off');
errorbar(x0, l1(:,6), l1onsci(:,1)-l1(:,6), l1onsci(:,2) - l1(:,6), 'dr','HandleVisibility','off');
title(sprintf('%s', ['length 100ms ', 'd =', num2str(table2array(diffdat(3,30)))]));
hold on

ax2 = subplot(1,3,2);
%axis([-3.5 1.5 3 5])
axis([-3.5 1.5 2.5 4.5])
xticks(x0);
xticklabels(xlaba);
yticks([2.5:0.1:5]);
xlabel('Contrast (a.u.)', 'fontsize', 14); % x-axis label
ylabel('Peak onset (s)', 'fontsize', 14);
hold on
plot(x0, ff5, 'k', 'LineWidth', 1)
scatter(x0, l2(:,6), 60, 'filled', 's', 'r', 'HandleVisibility','off');
errorbar(x0, l2(:,6), l2onsci(:,1)-l2(:,6), l2onsci(:,2)- l2(:,6), 'sr','HandleVisibility','off');
title(sprintf('%s', ['length 300ms ', 'd =', num2str(table2array(diffdat(6,30)))]));
hold on

ax3 = subplot(1,3,3);
%axis([-3.5 1.5 3 5])
axis([-3.5 1.5 2.5 4.5])
xticks(x0);
xticklabels(xlaba);
yticks([2.5:0.1:5]);
xlabel('Contrast (a.u.)', 'fontsize', 14); % x-axis label
ylabel('Peak onset (s)', 'fontsize', 14);
hold on
plot(x0, ff6, 'k', 'LineWidth', 1) 
scatter(x0, l3(:,6), 60, 'filled', 'r', 'HandleVisibility','off');
errorbar(x0, l3(:,6), l3onsci(:,1)-l3(:,6), l3onsci(:,2)- l3(:,6), 'or','HandleVisibility','off');
title(sprintf('%s', ['length 900ms ', 'd =', num2str(table2array(diffdat(9,30)))]));
hold on

sgtitle(sprintf('%s', 'Means and CIs of peak onset across stimulus intensity'),'fontsize',16);
%savefig('OnsetforDurs.fig');
hold off