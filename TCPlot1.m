function timecoursePlot = TCPlot1(looks, tr_vector, CombEventAvg, CombErrMargin, plotTitle, conddefinition)

% Function for plotting BOLD percentage activation change over time

timecoursePlot = plot(tr_vector, CombEventAvg, 'LineWidth', looks.mainlinewidth, 'Color', looks.color, 'DisplayName', sprintf('%s', conddefinition));
hold on;
err = errorbar(tr_vector, CombEventAvg, CombErrMargin, 'o', 'Color', looks.color, 'MarkerSize',3);
hold on;

for k= 1:size(timecoursePlot, 1)
    legend(timecoursePlot(1:k));
end

hold on

hold off

xlim([min(tr_vector) max(tr_vector)]); % Default x-axis range
% xlim([-10 max(tr_vector)]); % Custom x-axis range
% ylim([-0.6 0.7]); % y-axis range
xlabel('Time from event onset (vols)', 'fontsize', 14); % x-axis label
ylabel('Percent signal change', 'fontsize', 14); % y-axis label
set(gca, 'fontsize', 14);
title([plotTitle],'fontsize',14); % plot title

hold off;

figname = sprintf('%s', [plotTitle, '_fig', '.fig']); % Automatically saves plot according to condition_roi naming scheme
savefig(figname);
end